﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <آیتم فرد>
    /// </summary>

    //-----------------------------------------------------
    #region Class "PersonItemDB"

    public class PersonItemDB
    {



        #region Methods :

        public void AddPersonItemDB(PersonItemEntity personItemEntityParam, out Guid PersonItemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@PersonItemId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Date", SqlDbType.SmallDateTime) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = personItemEntityParam.PersonId;
            parameters[2].Value = personItemEntityParam.ItemId;
            parameters[3].Value = personItemEntityParam.Date;            
            parameters[4].Value = personItemEntityParam.IsActive;            

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_PersonItemAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            PersonItemId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdatePersonItemDB(PersonItemEntity personItemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@PersonItemId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Date", SqlDbType.SmallDateTime) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
							
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = personItemEntityParam.PersonItemId;
            parameters[1].Value = personItemEntityParam.PersonId;
            parameters[2].Value = personItemEntityParam.ItemId;
            parameters[3].Value = personItemEntityParam.Date;            
            parameters[4].Value = personItemEntityParam.IsActive;


            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SmsPanel.p_PersonItemUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeletePersonItemDB(PersonItemEntity PersonItemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@PersonItemId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = PersonItemEntityParam.PersonItemId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_PersonItemDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public PersonItemEntity GetSinglePersonItemDB(PersonItemEntity PersonItemEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@PersonItemId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = PersonItemEntityParam.PersonItemId;

                reader = _intranetDB.RunProcedureReader("SmsPanel.p_PersonItemGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetPersonItemDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PersonItemEntity> GetAllPersonItemDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetPersonItemDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SmsPanel.p_PersonItemGetAll", new IDataParameter[] { }));
        }

        public List<PersonItemEntity> GetPagePersonItemDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_PersonItem";
            parameters[5].Value = "PersonItemId";
            DataSet ds = _intranetDB.RunProcedureDS("SmsPanel.p_TablesGetPage", parameters);
            return GetPersonItemDBCollectionFromDataSet(ds, out count);
        }

        public PersonItemEntity GetPersonItemDBFromDataReader(IDataReader reader)
        {
            return new PersonItemEntity(Guid.Parse(reader["PersonItemId"].ToString()),
                                    Guid.Parse(reader["PersonId"].ToString()),
                                    Guid.Parse(reader["ItemId"].ToString()),
                                    (reader["Date"].ToString()),
                                    int.Parse(reader["MonthId"].ToString()),
                                    int.Parse(reader["DayNo"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    Guid.Parse(reader["ItemCategoryId"].ToString()),
                                    reader["FullName"].ToString());
        }

        public List<PersonItemEntity> GetPersonItemDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<PersonItemEntity> lst = new List<PersonItemEntity>();
                while (reader.Read())
                    lst.Add(GetPersonItemDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PersonItemEntity> GetPersonItemDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetPersonItemDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<PersonItemEntity> GetPersonItemDBCollectionByPersonDB(PersonItemEntity personItemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PersonId", SqlDbType.Int);
            parameter.Value = personItemEntityParam.PersonId;
            return GetPersonItemDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_PersonItemGetByPerson", new[] { parameter }));
        }



        public List<PersonItemEntity> GetPersonItemDBCollectionByItemDB(PersonItemEntity personItemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ItemId", SqlDbType.Int);
            parameter.Value = personItemEntityParam.ItemId;
            return GetPersonItemDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_PersonItemGetByItem", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}