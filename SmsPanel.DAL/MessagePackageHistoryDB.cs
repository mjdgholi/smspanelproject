﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/23>
    /// update date: <1393/07/14>
    /// Description: < بسته پیامک>
    /// </summary>    

    public class MessagePackageHistoryDB
    {
        #region Methods :

        public void AddMessagePackageHistoryDB(MessagePackageHistoryEntity messagePackageHistoryEntityParam, out Guid MessagePackageHistoryId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@MessagePackageHistoryId", SqlDbType.UniqueIdentifier),
                                 //new SqlParameter("@OccasionDayId", SqlDbType.Int) ,											  									  
											  new SqlParameter("@UserAndGroupId", SqlDbType.Int) ,
											  new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageText", SqlDbType.NVarChar,300) ,
                                              new SqlParameter("@SendDate", SqlDbType.DateTime) ,
                                              new SqlParameter("@GroupIdStr",SqlDbType.NVarChar,-1), 
											  new SqlParameter("@PersonMoblieIdStr", SqlDbType.NVarChar,-1) ,
											   new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;            
            //parameters[1].Value = (messagePackageHistoryEntityParam.OccasionDayId==null?System.DBNull.Value:(object)messagePackageHistoryEntityParam.OccasionDayId);                        
            parameters[1].Value = messagePackageHistoryEntityParam.UserAndGroupId;
            parameters[2].Value = (messagePackageHistoryEntityParam.MessageDraftId==null?System.DBNull.Value:(object)messagePackageHistoryEntityParam.MessageDraftId);
            parameters[3].Value = messagePackageHistoryEntityParam.MessageText;
            parameters[4].Value = messagePackageHistoryEntityParam.SendDate;
            parameters[5].Value = messagePackageHistoryEntityParam.GroupIdStr;
            parameters[6].Value = (messagePackageHistoryEntityParam.PersonMoblieIdStr==""?System.DBNull.Value:(object)messagePackageHistoryEntityParam.PersonMoblieIdStr);
            parameters[7].Value = messagePackageHistoryEntityParam.SmsServiceProviderId;
            parameters[8].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_MessagePackageHistoryAdd", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MessagePackageHistoryId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateMessagePackageHistoryDB(MessagePackageHistoryEntity messagePackageHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MessagePackageHistoryId", SqlDbType.UniqueIdentifier),
								  new SqlParameter("@UserAndGroupId", SqlDbType.Int) ,
											  new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageText", SqlDbType.NVarChar,300) ,
                                              new SqlParameter("@SendDate", SqlDbType.DateTime) ,
                                              new SqlParameter("@GroupIdStr",SqlDbType.NVarChar,-1), 
											  new SqlParameter("@PersonMoblieIdStr", SqlDbType.NVarChar,-1) ,
											   new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = messagePackageHistoryEntityParam.MessagePackageHistoryId;
            parameters[1].Value = messagePackageHistoryEntityParam.UserAndGroupId;
            parameters[2].Value = (messagePackageHistoryEntityParam.MessageDraftId == null ? System.DBNull.Value : (object)messagePackageHistoryEntityParam.MessageDraftId);
            parameters[3].Value = messagePackageHistoryEntityParam.MessageText;
            parameters[4].Value = messagePackageHistoryEntityParam.SendDate;
            parameters[5].Value = messagePackageHistoryEntityParam.GroupIdStr;
            parameters[6].Value = (messagePackageHistoryEntityParam.PersonMoblieIdStr == "" ? System.DBNull.Value : (object)messagePackageHistoryEntityParam.PersonMoblieIdStr);
            parameters[7].Value = messagePackageHistoryEntityParam.SmsServiceProviderId;      

            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SmsPanel.p_MessagePackageHistoryUpdate", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMessagePackageHistoryDB(MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@MessagePackageHistoryId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = MessagePackageHistoryEntityParam.MessagePackageHistoryId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_MessagePackageHistoryDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MessagePackageHistoryEntity GetSingleMessagePackageHistoryDB(MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@MessagePackageHistoryId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = MessagePackageHistoryEntityParam.MessagePackageHistoryId;

                reader = _intranetDB.RunProcedureReader("SmsPanel.p_MessagePackageHistoryGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMessagePackageHistoryDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MessagePackageHistoryEntity> GetAllMessagePackageHistoryDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMessagePackageHistoryDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SmsPanel.p_MessagePackageHistoryGetAll", new IDataParameter[] { }));
        }

        public List<MessagePackageHistoryEntity> GetPageMessagePackageHistoryDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_MessagePackageHistory";
            parameters[5].Value = "MessagePackageHistoryId";
            DataSet ds = _intranetDB.RunProcedureDS("SmsPanel.p_TablesGetPage", parameters);
            return GetMessagePackageHistoryDBCollectionFromDataSet(ds, out count);
        }

        public MessagePackageHistoryEntity GetMessagePackageHistoryDBFromDataReader(IDataReader reader)
        {
            return new MessagePackageHistoryEntity(Guid.Parse(reader["MessagePackageHistoryId"].ToString()),
                                    int.Parse(reader["AcctionTypeId"].ToString()),
                                     Convert.IsDBNull(reader["OccasionDayId"]) ? null : (int?)reader["OccasionDayId"],                                      
                                     Convert.IsDBNull(reader["ItemId"]) ? null : (Guid?)reader["ItemId"],
                                     Convert.IsDBNull(reader["PersonMoblieId"]) ? null : (Guid?)reader["PersonMoblieId"],
                                     Convert.IsDBNull(reader["UserAndGroupId"]) ? null : (int?)reader["UserAndGroupId"],                                                                     
                                    Convert.IsDBNull(reader["MessageDraftId"]) ? null : (Guid?)reader["MessageDraftId"],                                    
                                    reader["MessageText"].ToString(),
                                    reader["PersonMoblieIdStr"].ToString(),
                                    DateTime.Parse(reader["SendDate"].ToString()),
                                    Convert.IsDBNull(reader["SmsServiceProviderId"]) ? null : (Guid?)reader["SmsServiceProviderId"],
                                    Convert.IsDBNull(reader["RelationShipId"]) ? null : (Guid?)reader["RelationShipId"],
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["SendTime"].ToString(),
                                    reader["GroupIdStr"].ToString(),
                                    reader["PersonMoblieIdShowSelectControlStr"].ToString(),
                                    reader["FullNameShowSelectControlStr"].ToString());
        }

        public List<MessagePackageHistoryEntity> GetMessagePackageHistoryDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MessagePackageHistoryEntity> lst = new List<MessagePackageHistoryEntity>();
                while (reader.Read())
                    lst.Add(GetMessagePackageHistoryDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MessagePackageHistoryEntity> GetMessagePackageHistoryDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMessagePackageHistoryDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<MessagePackageHistoryEntity> GetMessagePackageHistoryDBCollectionByOccasionDayDB(MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionDayId", SqlDbType.UniqueIdentifier);
            parameter.Value = MessagePackageHistoryEntityParam.OccasionDayId;
            return GetMessagePackageHistoryDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_MessagePackageHistoryGetByOccasionDay", new[] { parameter }));
        }

        public List<MessagePackageHistoryEntity> GetMessagePackageHistoryDBCollectionByAcctionTypeDB(MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@AcctionTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = MessagePackageHistoryEntityParam.AcctionTypeId;
            return GetMessagePackageHistoryDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_MessagePackageHistoryGetByAcctionType", new[] { parameter }));
        }

        public List<MessagePackageHistoryEntity> GetMessagePackageHistoryDBCollectionByMessageDraftDB(MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier);
            parameter.Value = MessagePackageHistoryEntityParam.MessageDraftId;
            return GetMessagePackageHistoryDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_MessagePackageHistoryGetByMessageDraft", new[] { parameter }));
        }


        public DataSet GenerateMessagePackageForSmswithItemDB()
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            return intranetDB.RunProcedureDS("SMSPanel.p_GenerateMessagePackageForSmswithItem", new IDataParameter[] {});
        }

        #endregion

        public DataSet GenerateMessagePackageForSmswithOccasionDB()
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            return intranetDB.RunProcedureDS("SMSPanel.p_GenerateMessagePackageForSmswithOccasion", new IDataParameter[] { });
        }
    }

    
    
}