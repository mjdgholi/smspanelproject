﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/21>
    /// Description: < پیغام پیش نویس>
    /// </summary>


    public class MessageDraftDB
    {        
        #region Methods :

        public void AddMessageDraftDB(MessageDraftEntity messageDraftEntityParam, out Guid MessageDraftId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@MessageDraftText", SqlDbType.NVarChar,-1) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(messageDraftEntityParam.MessageDraftText);           
            parameters[2].Value = messageDraftEntityParam.IsActive;            

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_MessageDraftAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MessageDraftId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateMessageDraftDB(MessageDraftEntity messageDraftEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@MessageDraftText", SqlDbType.NVarChar,-1) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = messageDraftEntityParam.MessageDraftId;
            parameters[1].Value = FarsiToArabic.ToArabic(messageDraftEntityParam.MessageDraftText);            
            parameters[2].Value = messageDraftEntityParam.IsActive;            
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SMSPanel.p_MessageDraftUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMessageDraftDB(MessageDraftEntity MessageDraftEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = MessageDraftEntityParam.MessageDraftId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_MessageDraftDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MessageDraftEntity GetSingleMessageDraftDB(MessageDraftEntity MessageDraftEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = MessageDraftEntityParam.MessageDraftId;

                reader = _intranetDB.RunProcedureReader("SMSPanel.p_MessageDraftGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMessageDraftDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MessageDraftEntity> GetAllMessageDraftDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMessageDraftDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SMSPanel.p_MessageDraftGetAll", new IDataParameter[] { }));
        }
        public List<MessageDraftEntity> GetAllMessageDraftIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMessageDraftDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("[SMSPanel].[p_MessageDraftGetAllIsActive]", new IDataParameter[] { }));
        }

        public List<MessageDraftEntity> GetPageMessageDraftDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_MessageDraft";
            parameters[5].Value = "MessageDraftId";
            DataSet ds = _intranetDB.RunProcedureDS("SMSPanel.p_TablesGetPage", parameters);
            return GetMessageDraftDBCollectionFromDataSet(ds, out count);
        }

        public MessageDraftEntity GetMessageDraftDBFromDataReader(IDataReader reader)
        {
            return new MessageDraftEntity(Guid.Parse(reader["MessageDraftId"].ToString()),
                                    reader["MessageDraftText"].ToString(),                                    
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<MessageDraftEntity> GetMessageDraftDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MessageDraftEntity> lst = new List<MessageDraftEntity>();
                while (reader.Read())
                    lst.Add(GetMessageDraftDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MessageDraftEntity> GetMessageDraftDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMessageDraftDBCollectionFromDataReader(ds.CreateDataReader());
        }

        


        #endregion

        
    }
    
}