﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <پیامک با آیتم>
    /// </summary>

    //-----------------------------------------------------
    #region Class "Group_Item_SmsDB"

    public class Group_Item_SmsDB
    {

        

        #region Methods :

        public void AddGroup_Item_SmsDB(Group_Item_SmsEntity groupItemSmsEntityParam, out Guid Group_Item_SmsId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@Group_Item_SmsId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@GroupIdStr", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageText", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@SendTime", SqlDbType.Time,7) ,
											  new SqlParameter("@FromDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
                                              new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = groupItemSmsEntityParam.GroupIdStr;
            parameters[2].Value = groupItemSmsEntityParam.ItemId;
            parameters[3].Value = (groupItemSmsEntityParam.MessageDraftId == null ? System.DBNull.Value : (object)groupItemSmsEntityParam.MessageDraftId);
            parameters[4].Value = FarsiToArabic.ToArabic(groupItemSmsEntityParam.MessageText);
            parameters[5].Value = groupItemSmsEntityParam.SendTime;
            parameters[6].Value = groupItemSmsEntityParam.FromDate;
            parameters[7].Value = groupItemSmsEntityParam.EndDate;
            parameters[8].Value = groupItemSmsEntityParam.SmsServiceProviderId;
            parameters[9].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Smspanel.p_Group_Item_SmsAdd", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            Group_Item_SmsId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateGroup_Item_SmsDB(Group_Item_SmsEntity groupItemSmsEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@Group_Item_SmsId", SqlDbType.UniqueIdentifier),
													 new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageText", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@SendTime", SqlDbType.Time,7) ,
											  new SqlParameter("@FromDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = groupItemSmsEntityParam.Group_Item_SmsId;
            parameters[1].Value = groupItemSmsEntityParam.GroupId;
            parameters[2].Value = groupItemSmsEntityParam.ItemId;
            parameters[3].Value = (groupItemSmsEntityParam.MessageDraftId == null ? System.DBNull.Value : (object)groupItemSmsEntityParam.MessageDraftId);
            parameters[4].Value = FarsiToArabic.ToArabic(groupItemSmsEntityParam.MessageText);
            parameters[5].Value = groupItemSmsEntityParam.SendTime;
            parameters[6].Value = groupItemSmsEntityParam.FromDate;
            parameters[7].Value = groupItemSmsEntityParam.EndDate;
            parameters[8].Value = groupItemSmsEntityParam.SmsServiceProviderId;

            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Smspanel.p_Group_Item_SmsUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteGroup_Item_SmsDB(Group_Item_SmsEntity Group_Item_SmsEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@Group_Item_SmsId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = Group_Item_SmsEntityParam.Group_Item_SmsId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Smspanel.p_Group_Item_SmsDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public Group_Item_SmsEntity GetSingleGroup_Item_SmsDB(Group_Item_SmsEntity Group_Item_SmsEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@Group_Item_SmsId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = Group_Item_SmsEntityParam.Group_Item_SmsId;

                reader = _intranetDB.RunProcedureReader("Smspanel.p_Group_Item_SmsGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetGroup_Item_SmsDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<Group_Item_SmsEntity> GetAllGroup_Item_SmsDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGroup_Item_SmsDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Smspanel.p_Group_Item_SmsGetAll", new IDataParameter[] { }));
        }

        public List<Group_Item_SmsEntity> GetPageGroup_Item_SmsDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Group_Item_Sms";
            parameters[5].Value = "Group_Item_SmsId";
            DataSet ds = _intranetDB.RunProcedureDS("Smspanel.p_TablesGetPage", parameters);
            return GetGroup_Item_SmsDBCollectionFromDataSet(ds, out count);
        }

        public Group_Item_SmsEntity GetGroup_Item_SmsDBFromDataReader(IDataReader reader)
        {
            return new Group_Item_SmsEntity(Guid.Parse(reader["Group_Item_SmsId"].ToString()),
                                    Guid.Parse(reader["GroupId"].ToString()),
                                    Guid.Parse(reader["ItemId"].ToString()),
                                    Convert.IsDBNull(reader["MessageDraftId"]) ? null : (Guid?)reader["MessageDraftId"],                                    
                                    reader["MessageText"].ToString(),
                                    reader["SendTime"].ToString(),
                                    reader["FromDate"].ToString(),
                                    reader["EndDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),                                    
                                     Convert.IsDBNull(reader["ItemCategoryId"]) ? null : (Guid?)reader["ItemCategoryId"],
                                     Convert.IsDBNull(reader["OccasionDayTypeId"]) ? null : (int?)reader["OccasionDayTypeId"],
                                     Convert.IsDBNull(reader["OccasionDayId"]) ? null : (int?)reader["OccasionDayId"],
                                      Convert.IsDBNull(reader["SmsServiceProviderId"]) ? null : (Guid?)reader["SmsServiceProviderId"]                                    
                                    );
        }

        public List<Group_Item_SmsEntity> GetGroup_Item_SmsDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<Group_Item_SmsEntity> lst = new List<Group_Item_SmsEntity>();
                while (reader.Read())
                    lst.Add(GetGroup_Item_SmsDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<Group_Item_SmsEntity> GetGroup_Item_SmsDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetGroup_Item_SmsDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<Group_Item_SmsEntity> GetGroup_Item_SmsDBCollectionByGroupDB(Group_Item_SmsEntity groupItemSmsEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier);
            parameter.Value = groupItemSmsEntityParam.GroupId;
            return GetGroup_Item_SmsDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Smspanel.p_Group_Item_SmsGetByGroup", new[] { parameter }));
        }

        public List<Group_Item_SmsEntity> GetGroup_Item_SmsDBCollectionByItemDB(Group_Item_SmsEntity groupItemSmsEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier);
            parameter.Value = groupItemSmsEntityParam.ItemId;
            return GetGroup_Item_SmsDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Smspanel.p_Group_Item_SmsGetByItem", new[] { parameter }));
        }

        public List<Group_Item_SmsEntity> GetGroup_Item_SmsDBCollectionByMessageDraftDB(Group_Item_SmsEntity groupItemSmsEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier);
            parameter.Value = groupItemSmsEntityParam.MessageDraftId;
            return GetGroup_Item_SmsDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Smspanel.p_Group_Item_SmsGetByMessageDraft", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}