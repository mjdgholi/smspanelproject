﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/22>
    /// Description: <وضیعت پیامک>
    /// </summary>
    public class MessageStatusDB
    {
            #region Methods :

            public void AddMessageStatusDB(MessageStatusEntity messageStatusEntityParam, out int MessageStatusId)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = 	{
								 new SqlParameter("@MessageStatusId", SqlDbType.Int, 4),
								 new SqlParameter("@MessageStatusTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
                parameters[0].Direction = ParameterDirection.Output;
                parameters[1].Value = messageStatusEntityParam.MessageStatusPersianTitle;
                parameters[2].Value = messageStatusEntityParam.IsActive;                

                parameters[3].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("SmsPanel.p_MessageStatusAdd", parameters);
                var messageError = parameters[3].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
                MessageStatusId = int.Parse(parameters[0].Value.ToString());
            }

            public void UpdateMessageStatusDB(MessageStatusEntity messageStatusEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
			                            	new SqlParameter("@MessageStatusId", SqlDbType.Int, 4),
						 new SqlParameter("@MessageStatusTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,						
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

                parameters[0].Value = messageStatusEntityParam.MessageStatusId;
                parameters[1].Value = messageStatusEntityParam.MessageStatusPersianTitle;
                parameters[2].Value = messageStatusEntityParam.IsActive;

                parameters[3].Direction = ParameterDirection.Output;

                _intranetDB.RunProcedure("SmsPanel.p_MessageStatusUpdate", parameters);
                var messageError = parameters[3].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public void DeleteMessageStatusDB(MessageStatusEntity MessageStatusEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
		                            		new SqlParameter("@MessageStatusId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
                parameters[0].Value = MessageStatusEntityParam.MessageStatusId;
                parameters[1].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("SmsPanel.p_MessageStatusDel", parameters);
                var messageError = parameters[1].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public MessageStatusEntity GetSingleMessageStatusDB(MessageStatusEntity MessageStatusEntityParam)
            {
                IDataReader reader = null;
                try
                {

                    IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                    IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@MessageStatusId", DataTypes.integer, 4, 										MessageStatusEntityParam.MessageStatusId)
						      };

                    reader = _intranetDB.RunProcedureReader("SmsPanel.p_MessageStatusGetSingle", parameters);
                    if (reader != null)
                        if (reader.Read())
                            return GetMessageStatusDBFromDataReader(reader);
                    return null;
                }

                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<MessageStatusEntity> GetAllMessageStatusDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetMessageStatusDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("SmsPanel.p_MessageStatusGetAll", new IDataParameter[] { }));
            }


            public List<MessageStatusEntity> GetAllMessageStatusIsActiveDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetMessageStatusDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("SmsPanel.p_MessageStatusIsActiveGetAll", new IDataParameter[] { }));
            }
            public List<MessageStatusEntity> GetPageMessageStatusDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
                parameters[0].Value = PageSize;
                parameters[1].Value = CurrentPage;
                parameters[2].Value = WhereClause;
                parameters[3].Value = OrderBy;
                parameters[4].Value = "t_MessageStatus";
                parameters[5].Value = "MessageStatusId";
                DataSet ds = _intranetDB.RunProcedureDS("SmsPanel.p_TablesGetPage", parameters);
                return GetMessageStatusDBCollectionFromDataSet(ds, out count);
            }

            public MessageStatusEntity GetMessageStatusDBFromDataReader(IDataReader reader)
            {
                return new MessageStatusEntity(int.Parse(reader["MessageStatusId"].ToString()),
                                            int.Parse(reader["MessageStatusCode"].ToString()),
                                             Convert.IsDBNull(reader["SmsServiceProviderId"]) ? null : (Guid?)reader["SmsServiceProviderId"],                                               
                                        reader["MessageStatusEnglishTitle"].ToString(),
                                        reader["MessageStatusPersianTitle"].ToString(),
                                        bool.Parse(reader["IsActive"].ToString()),
                                        reader["CreationDate"].ToString(),
                                        reader["ModificationDate"].ToString());
            }

            public List<MessageStatusEntity> GetMessageStatusDBCollectionFromDataReader(IDataReader reader)
            {
                try
                {
                    List<MessageStatusEntity> lst = new List<MessageStatusEntity>();
                    while (reader.Read())
                        lst.Add(GetMessageStatusDBFromDataReader(reader));
                    return lst;
                }
                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<MessageStatusEntity> GetMessageStatusDBCollectionFromDataSet(DataSet ds, out int count)
            {
                count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                return GetMessageStatusDBCollectionFromDataReader(ds.CreateDataReader());
            }


            public List<MessageStatusEntity> GetMessageStatusDBCollectionBySmsServiceProviderDB(MessageStatusEntity messageStatusEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter parameter = new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier);
                parameter.Value = messageStatusEntityParam.SmsServiceProviderId;
                return GetMessageStatusDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_MessageStatusBySmsServiceProvider", new[] { parameter }));
            }

            #endregion



        }


    }
    
