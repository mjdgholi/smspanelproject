﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/21>
    /// Description: <نوع مناسبت>
    /// </summary>
    public class OccasionTypeDB
    {
        #region Methods :

        public void AddOccasionTypeDB(OccasionTypeEntity occasionTypeEntityParam, out Guid OccasionTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@OccasionTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@OccasionTypeTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(occasionTypeEntityParam.OccasionTypeTitle);
            parameters[2].Value = occasionTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_OccasionTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            OccasionTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateOccasionTypeDB(OccasionTypeEntity occasionTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@OccasionTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@OccasionTypeTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = occasionTypeEntityParam.OccasionTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(occasionTypeEntityParam.OccasionTypeTitle);
            parameters[2].Value = occasionTypeEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SMSPanel.p_OccasionTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteOccasionTypeDB(OccasionTypeEntity OccasionTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@OccasionTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = OccasionTypeEntityParam.OccasionTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_OccasionTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public OccasionTypeEntity GetSingleOccasionTypeDB(OccasionTypeEntity OccasionTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@OccasionTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = OccasionTypeEntityParam.OccasionTypeId;

                reader = _intranetDB.RunProcedureReader("SMSPanel.p_OccasionTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetOccasionTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OccasionTypeEntity> GetAllOccasionTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOccasionTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SMSPanel.p_OccasionTypeGetAll", new IDataParameter[] { }));
        }

        public List<OccasionTypeEntity> GetAllOccasionTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOccasionTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SMSPanel.p_OccasionTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<OccasionTypeEntity> GetPageOccasionTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_OccasionType";
            parameters[5].Value = "OccasionTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("SMSPanel.p_TablesGetPage", parameters);
            return GetOccasionTypeDBCollectionFromDataSet(ds, out count);
        }

        public OccasionTypeEntity GetOccasionTypeDBFromDataReader(IDataReader reader)
        {
            return new OccasionTypeEntity(Guid.Parse(reader["OccasionTypeId"].ToString()),
                                    reader["OccasionTypeTitle"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<OccasionTypeEntity> GetOccasionTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<OccasionTypeEntity> lst = new List<OccasionTypeEntity>();
                while (reader.Read())
                    lst.Add(GetOccasionTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OccasionTypeEntity> GetOccasionTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetOccasionTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }
}
