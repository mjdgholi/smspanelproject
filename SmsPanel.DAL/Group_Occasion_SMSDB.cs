﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/17>
    /// Description: <پیامک با مناسبت>
    /// </summary>



    public class Group_Occasion_SMSDB
    {


        #region Methods :

        public void AddGroup_Occasion_SMSDB(Group_Occasion_SMSEnity groupOccasionSmsEnityParam, out Guid Group_Occasion_SMSId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@Group_Occasion_SMSId", SqlDbType.UniqueIdentifier),
                                 new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@GroupIdStr", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@OccasionDayId", SqlDbType.Int) ,
											  new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageText", SqlDbType.NVarChar,300) ,
                                              new SqlParameter("@DifferenceDay",SqlDbType.Int), 
											  new SqlParameter("@SendTime", SqlDbType.Time,7) ,
											  new SqlParameter("@FromDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = groupOccasionSmsEnityParam.SmsServiceProviderId;
            parameters[2].Value = groupOccasionSmsEnityParam.GroupIdStr;
            parameters[3].Value = groupOccasionSmsEnityParam.OccasionDayId;
            parameters[4].Value = (groupOccasionSmsEnityParam.MessageDraftId == null ? System.DBNull.Value : (object)groupOccasionSmsEnityParam.MessageDraftId);
            parameters[5].Value = groupOccasionSmsEnityParam.MessageText;
            parameters[6].Value = groupOccasionSmsEnityParam.DifferenceDay;
            parameters[7].Value = groupOccasionSmsEnityParam.SendTime;
            parameters[8].Value = groupOccasionSmsEnityParam.FromDate;
            parameters[9].Value = groupOccasionSmsEnityParam.EndDate;            

            parameters[10].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_Group_Occasion_SMSAdd", parameters);
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            Group_Occasion_SMSId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateGroup_Occasion_SMSDB(Group_Occasion_SMSEnity groupOccasionSmsEnityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@Group_Occasion_SMSId", SqlDbType.UniqueIdentifier),
				                                 new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OccasionDayId", SqlDbType.Int) ,
											  new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageText", SqlDbType.NVarChar,300) ,
                                              new SqlParameter("@DifferenceDay",SqlDbType.Int), 
											  new SqlParameter("@SendTime", SqlDbType.Time,7) ,
											  new SqlParameter("@FromDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,				
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = groupOccasionSmsEnityParam.Group_Occasion_SMSId;
            parameters[1].Value = groupOccasionSmsEnityParam.SmsServiceProviderId;
            parameters[2].Value = groupOccasionSmsEnityParam.GroupId;
            parameters[3].Value = groupOccasionSmsEnityParam.OccasionDayId;
            parameters[4].Value = (groupOccasionSmsEnityParam.MessageDraftId == null ? System.DBNull.Value : (object)groupOccasionSmsEnityParam.MessageDraftId);
            parameters[5].Value = groupOccasionSmsEnityParam.MessageText;
            parameters[6].Value = groupOccasionSmsEnityParam.DifferenceDay;
            parameters[7].Value = groupOccasionSmsEnityParam.SendTime;
            parameters[8].Value = groupOccasionSmsEnityParam.FromDate;
            parameters[9].Value = groupOccasionSmsEnityParam.EndDate;  
            

            parameters[10].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SmsPanel.p_Group_Occasion_SMSUpdate", parameters);
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteGroup_Occasion_SMSDB(Group_Occasion_SMSEnity Group_Occasion_SMSEnityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@Group_Occasion_SMSId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = Group_Occasion_SMSEnityParam.Group_Occasion_SMSId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_Group_Occasion_SMSDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public Group_Occasion_SMSEnity GetSingleGroup_Occasion_SMSDB(Group_Occasion_SMSEnity Group_Occasion_SMSEnityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@Group_Occasion_SMSId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = Group_Occasion_SMSEnityParam.Group_Occasion_SMSId;

                reader = _intranetDB.RunProcedureReader("SmsPanel.p_Group_Occasion_SMSGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetGroup_Occasion_SMSDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<Group_Occasion_SMSEnity> GetAllGroup_Occasion_SMSDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGroup_Occasion_SMSDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SmsPanel.p_Group_Occasion_SMSGetAll", new IDataParameter[] { }));
        }

        public List<Group_Occasion_SMSEnity> GetPageGroup_Occasion_SMSDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Group_Occasion_SMS";
            parameters[5].Value = "Group_Occasion_SMSId";
            DataSet ds = _intranetDB.RunProcedureDS("p_TablesGetPage", parameters);
            return GetGroup_Occasion_SMSDBCollectionFromDataSet(ds, out count);
        }

        public Group_Occasion_SMSEnity GetGroup_Occasion_SMSDBFromDataReader(IDataReader reader)
        {
            return new Group_Occasion_SMSEnity(Guid.Parse(reader["Group_Occasion_SMSId"].ToString()),
                                    Guid.Parse(reader["SmsServiceProviderId"].ToString()),
                                    Guid.Parse(reader["GroupId"].ToString()),
                                    int.Parse(reader["OccasionDayId"].ToString()),
                        Convert.IsDBNull(reader["MessageDraftId"]) ? null : (Guid?)reader["MessageDraftId"],                                      
                                    reader["MessageText"].ToString(),
                                    reader["SendTime"].ToString(),
                                    int.Parse(reader["DifferenceDay"].ToString()),
                                    reader["FromDate"].ToString(),
                                    reader["EndDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    int.Parse(reader["OccasionDayTypeId"].ToString()));
        }

        public List<Group_Occasion_SMSEnity> GetGroup_Occasion_SMSDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<Group_Occasion_SMSEnity> lst = new List<Group_Occasion_SMSEnity>();
                while (reader.Read())
                    lst.Add(GetGroup_Occasion_SMSDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<Group_Occasion_SMSEnity> GetGroup_Occasion_SMSDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetGroup_Occasion_SMSDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<Group_Occasion_SMSEnity> GetGroup_Occasion_SMSDBCollectionByOccasionDayDB(Group_Occasion_SMSEnity groupOccasionSmsEnityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionDayId", SqlDbType.Int);
            parameter.Value = groupOccasionSmsEnityParam.OccasionDayId;
            return GetGroup_Occasion_SMSDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_Group_Occasion_SMSGetByOccasionDay", new[] { parameter }));
        }

        public  List<Group_Occasion_SMSEnity> GetGroup_Occasion_SMSDBCollectionByGroupDB(Group_Occasion_SMSEnity groupOccasionSmsEnityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier);
            parameter.Value = groupOccasionSmsEnityParam.GroupId;
            return GetGroup_Occasion_SMSDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_Group_Occasion_SMSGetByGroup", new[] { parameter }));
        }

        public  List<Group_Occasion_SMSEnity> GetGroup_Occasion_SMSDBCollectionByMessageDraftDB(Group_Occasion_SMSEnity groupOccasionSmsEnityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier);
            parameter.Value = groupOccasionSmsEnityParam.MessageDraftId;
            return GetGroup_Occasion_SMSDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_Group_Occasion_SMSGetByMessageDraft", new[] { parameter }));
        }


        #endregion



    }

}