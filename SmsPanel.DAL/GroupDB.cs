﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{

    //-----93-04-18
    //----- majid Gholibeygian
    public class GroupDB
    {      

        #region Methods :

        public void AddGroupDB(GroupEntity GroupEntityParam, out Guid GroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@OwnerId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@GroupTitle", SqlDbType.NVarChar, 150),
                new SqlParameter("@ChildNo", SqlDbType.Int),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = (GroupEntityParam.OwnerId == null) ?DBNull.Value :(object) GroupEntityParam.OwnerId;
            parameters[2].Value = ITC.Library.Classes.FarsiToArabic.ToArabic(GroupEntityParam.GroupTitle);
            parameters[3].Value = GroupEntityParam.ChildNo; 
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SmsPanel.p_GroupAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            GroupId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateGroupDB(GroupEntity GroupEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@OwnerId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@GroupTitle", SqlDbType.NVarChar, 150),
                new SqlParameter("@ChildNo", SqlDbType.Int),            
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = GroupEntityParam.GroupId;
            parameters[1].Value = (GroupEntityParam.OwnerId == null) ? DBNull.Value : (object)GroupEntityParam.OwnerId;
            parameters[2].Value =ITC.Library.Classes.FarsiToArabic.ToArabic(GroupEntityParam.GroupTitle);
            parameters[3].Value = GroupEntityParam.ChildNo;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SmsPanel.p_GroupUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteGroupDB(GroupEntity GroupEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = GroupEntityParam.GroupId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_GroupDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public GroupEntity GetSingleGroupDB(GroupEntity GroupEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier)
                };
                parameters[0].Value = GroupEntityParam.GroupId;

                reader = _intranetDB.RunProcedureReader("SmsPanel.p_GroupGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetGroupDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GroupEntity> GetAllGroupDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGroupDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("SmsPanel.p_GroupGetAll", new IDataParameter[] {}));
        }

        public List<GroupEntity> GetPageGroupDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "smspanel.t_Group";
            parameters[5].Value = "GroupId";
            DataSet ds = _intranetDB.RunProcedureDS("SmsPanel.p_TablesGetPage", parameters);
            return GetGroupDBCollectionFromDataSet(ds, out count);
        }

        public GroupEntity GetGroupDBFromDataReader(IDataReader reader)
        {
            return new GroupEntity(new Guid(reader["GroupId"].ToString()), 
                (Convert.IsDBNull(reader["OwnerId"]))?(Guid?)null:Guid.Parse(reader["OwnerId"].ToString()),
                reader["GroupTitle"].ToString(),
                int.Parse(reader["ChildNo"].ToString()),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString());
        }

        public List<GroupEntity> GetGroupDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<GroupEntity> lst = new List<GroupEntity>();
                while (reader.Read())
                    lst.Add(GetGroupDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GroupEntity> GetGroupDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetGroupDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

        public void DeleteWithAllChild(Guid groupId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = groupId;
            parameters[1].Direction = ParameterDirection.Output;
            intranetDb.RunProcedure("smspanel.[p_GroupDeleteWithAllChild]", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DataTable GetParentNodes(Guid groupId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("GroupId", SqlDbType.UniqueIdentifier)
                };
            parameter[0].Value = groupId;
            DataTable dataTable = intranetDb.RunProcedureDS("[smspanel].[p_ReportParentTreeForGroup]", parameter).Tables[0];
            return dataTable;
        }

        public DataTable GetAllNodes(string groupTitle)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {                 
                    new SqlParameter("GroupTitle", SqlDbType.NVarChar, 300),                   
                };

            parameter[0].Value = ITC.Library.Classes.FarsiToArabic.ToArabic(groupTitle);
            DataTable dataTable = intranetDb.RunProcedureDS("[smspanel].[p_GroupGetAllWithFilter]", parameter).Tables[0];
            return dataTable;
        }
    }
}