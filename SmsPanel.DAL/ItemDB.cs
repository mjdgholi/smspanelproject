﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <آیتم ها>
    /// </summary>

    public class ItemDB
    {
        #region Methods :

        public void AddItemDB(ItemEntity itemEntityParam, out Guid ItemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ItemCategoryId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ItemTitle", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Priority", SqlDbType.NChar,10) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = itemEntityParam.ItemCategoryId;
            parameters[2].Value = FarsiToArabic.ToArabic(itemEntityParam.ItemTitle.Trim());
            parameters[3].Value = itemEntityParam.Priority;
            parameters[4].Value = itemEntityParam.IsActive;            

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_ItemAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ItemId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateItemDB(ItemEntity itemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ItemCategoryId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ItemTitle", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Priority", SqlDbType.NChar,10) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = itemEntityParam.ItemId;
            parameters[1].Value = itemEntityParam.ItemCategoryId;
            parameters[2].Value = FarsiToArabic.ToArabic(itemEntityParam.ItemTitle.Trim());
            parameters[3].Value = itemEntityParam.Priority;
            parameters[4].Value = itemEntityParam.IsActive;            

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SmsPanel.p_ItemUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteItemDB(ItemEntity ItemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ItemEntityParam.ItemId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_ItemDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ItemEntity GetSingleItemDB(ItemEntity ItemEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ItemEntityParam.ItemId;

                reader = _intranetDB.RunProcedureReader("SmsPanel.p_ItemGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetItemDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ItemEntity> GetAllItemDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetItemDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SmsPanel.p_ItemGetAll", new IDataParameter[] { }));
        }

        public List<ItemEntity> GetPageItemDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Item";
            parameters[5].Value = "ItemId";
            DataSet ds = _intranetDB.RunProcedureDS("SmsPanel.p_TablesGetPage", parameters);
            return GetItemDBCollectionFromDataSet(ds, out count);
        }

        public ItemEntity GetItemDBFromDataReader(IDataReader reader)
        {
            return new ItemEntity(Guid.Parse(reader["ItemId"].ToString()),
                                    Guid.Parse(reader["ItemCategoryId"].ToString()),
                                    reader["ItemTitle"].ToString(),
                                    reader["Priority"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<ItemEntity> GetItemDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ItemEntity> lst = new List<ItemEntity>();
                while (reader.Read())
                    lst.Add(GetItemDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ItemEntity> GetItemDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetItemDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<ItemEntity> GetItemDBCollectionByItemCategoryDB(ItemEntity itemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ItemCategoryId", SqlDbType.UniqueIdentifier);
            parameter.Value = itemEntityParam.ItemCategoryId;
            return GetItemDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_ItemGetByItemCategory", new[] { parameter }));
        }


        #endregion



    }

    
}