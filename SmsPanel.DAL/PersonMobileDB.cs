﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/22>
    /// Description: <شماره موبایل شخص>
    /// </summary>


    public class PersonMobileDB
    {
        #region Methods :

        public void AddPersonMobileDB(PersonMobileEntity personMobileEntityParam, out Guid PersonMoblieId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@PersonMoblieId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MobileNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@IsDefault", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = personMobileEntityParam.PersonId;
            parameters[2].Value = personMobileEntityParam.MobileNo;
            parameters[3].Value = personMobileEntityParam.IsDefault;
            parameters[4].Value = personMobileEntityParam.IsActive;            

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_PersonMobileAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            PersonMoblieId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdatePersonMobileDB(PersonMobileEntity personMobileEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@PersonMoblieId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MobileNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@IsDefault", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,					
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = personMobileEntityParam.PersonMoblieId;
            parameters[1].Value = personMobileEntityParam.PersonId;
            parameters[2].Value = personMobileEntityParam.MobileNo;
            parameters[3].Value = personMobileEntityParam.IsDefault;
            parameters[4].Value = personMobileEntityParam.IsActive;            

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SMSPanel.p_PersonMobileUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeletePersonMobileDB(PersonMobileEntity PersonMobileEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@PersonMoblieId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = PersonMobileEntityParam.PersonMoblieId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_PersonMobileDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public PersonMobileEntity GetSinglePersonMobileDB(PersonMobileEntity PersonMobileEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@PersonMoblieId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = PersonMobileEntityParam.PersonMoblieId;

                reader = _intranetDB.RunProcedureReader("SMSPanel.p_PersonMobileGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetPersonMobileDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PersonMobileEntity> GetAllPersonMobileDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetPersonMobileDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SMSPanel.p_PersonMobileGetAll", new IDataParameter[] { }));
        }

        public List<PersonMobileEntity> GetPagePersonMobileDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_PersonMobile";
            parameters[5].Value = "PersonMoblieId";
            DataSet ds = _intranetDB.RunProcedureDS("SMSPanel.p_TablesGetPage", parameters);
            return GetPersonMobileDBCollectionFromDataSet(ds, out count);
        }

        public PersonMobileEntity GetPersonMobileDBFromDataReader(IDataReader reader)
        {
            return new PersonMobileEntity(Guid.Parse(reader["PersonMoblieId"].ToString()),
                                    Guid.Parse(reader["PersonId"].ToString()),
                                    reader["MobileNo"].ToString(),
                                    bool.Parse(reader["IsDefault"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["FullName"].ToString());
        }

        public List<PersonMobileEntity> GetPersonMobileDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<PersonMobileEntity> lst = new List<PersonMobileEntity>();
                while (reader.Read())
                    lst.Add(GetPersonMobileDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PersonMobileEntity> GetPersonMobileDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetPersonMobileDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<PersonMobileEntity> GetPersonMobileDBCollectionByPersonDB(PersonMobileEntity personMobileEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier);
            parameter.Value = personMobileEntityParam.PersonId;
            return GetPersonMobileDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SMSPanel.p_PersonMobileGetByPerson", new[] { parameter }));
        }


        #endregion
    }    
}