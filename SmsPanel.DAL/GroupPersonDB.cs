﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{

    //-----93-04-18
    //----- majid Gholibeygian

    #region Class "GroupPersonDB"

    public class GroupPersonDB
    {

        #region Methods :

        public void AddGroupPersonDB(GroupPersonEntity GroupPersonEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {              
                new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@PersonMoblieIdStr", SqlDbType.NVarChar,-1),               
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };            
            parameters[0].Value = GroupPersonEntityParam.GroupId;
            parameters[1].Value = GroupPersonEntityParam.PersonMoblieIdStr;            
            parameters[2].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_GroupPersonAdd", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));            
        }

        public void UpdateGroupPersonDB(GroupPersonEntity GroupPersonEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@GroupPersonId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier),
                  new SqlParameter("@PersonMoblieId", SqlDbType.UniqueIdentifier),             
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = GroupPersonEntityParam.GroupPersonId;
            parameters[1].Value = GroupPersonEntityParam.GroupId;
            parameters[2].Value = GroupPersonEntityParam.PersonMoblieId;   
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SmsPanel.p_GroupPersonUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteGroupPersonDB(GroupPersonEntity GroupPersonEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@GroupPersonId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = GroupPersonEntityParam.GroupPersonId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_GroupPersonDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public GroupPersonEntity GetSingleGroupPersonDB(GroupPersonEntity GroupPersonEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    new SqlParameter("@GroupPersonId", SqlDbType.UniqueIdentifier)
                };
                parameters[0].Value = GroupPersonEntityParam.GroupPersonId;

                reader = _intranetDB.RunProcedureReader("SmsPanel.p_GroupPersonGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetGroupPersonDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GroupPersonEntity> GetAllGroupPersonDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGroupPersonDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("SmsPanel.p_GroupPersonGetAll", new IDataParameter[] {}));
        }

        public List<GroupPersonEntity> GetPageGroupPersonDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_GroupPerson";
            parameters[5].Value = "GroupPersonId";
            DataSet ds = _intranetDB.RunProcedureDS("SmsPanel.p_TablesGetPage", parameters);
            return GetGroupPersonDBCollectionFromDataSet(ds, out count);
        }

        public GroupPersonEntity GetGroupPersonDBFromDataReader(IDataReader reader)
        {
            return new GroupPersonEntity(new Guid(reader["GroupPersonId"].ToString()), 
                Guid.Parse(reader["GroupId"].ToString()),
                Guid.Parse(reader["PersonMoblieId"].ToString()),
                Guid.Parse(reader["PersonId"].ToString()),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString(),
                reader["PersonIdStr"].ToString(),
                reader["PersonMoblieIdStr"].ToString(),
                reader["FullName"].ToString());
        }

        public List<GroupPersonEntity> GetGroupPersonDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<GroupPersonEntity> lst = new List<GroupPersonEntity>();
                while (reader.Read())
                    lst.Add(GetGroupPersonDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GroupPersonEntity> GetGroupPersonDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetGroupPersonDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<GroupPersonEntity> GetGroupPersonDBCollectionByPersonDB(Guid PersonId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier);
            parameter.Value = PersonId;
            return GetGroupPersonDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_GroupPersonGetByPerson", new[] {parameter}));
        }

        public List<GroupPersonEntity> GetGroupPersonDBCollectionByGroupDB(Guid GroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@GroupId", SqlDbType.UniqueIdentifier);
            parameter.Value = GroupId;
            return GetGroupPersonDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SmsPanel.p_GroupPersonGetByGroup", new[] {parameter}));
        }


        #endregion



    }

    #endregion
}