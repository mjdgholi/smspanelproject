﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/21>
    /// Description: < مناسبت>
    /// </summary>


    public class OccasionDB
    {       
        #region Methods :

        public void AddOccasionDB(OccasionEntity occasionEntityParam, out Guid OccasionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@OccasionTitle", SqlDbType.NVarChar,250) ,
											  new SqlParameter("@OccasionTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(occasionEntityParam.OccasionTitle);
            parameters[2].Value = occasionEntityParam.OccasionTypeId;
            parameters[3].Value = occasionEntityParam.IsActive;            

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_OccasionAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            OccasionId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateOccasionDB(OccasionEntity occasionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@OccasionTitle", SqlDbType.NVarChar,250) ,
											  new SqlParameter("@OccasionTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = occasionEntityParam.OccasionId;
            parameters[1].Value = FarsiToArabic.ToArabic(occasionEntityParam.OccasionTitle);
            parameters[2].Value = occasionEntityParam.OccasionTypeId;
            parameters[3].Value = occasionEntityParam.IsActive;            

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SMSPanel.p_OccasionUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteOccasionDB(OccasionEntity OccasionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = OccasionEntityParam.OccasionId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_OccasionDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public OccasionEntity GetSingleOccasionDB(OccasionEntity OccasionEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = OccasionEntityParam.OccasionId;

                reader = _intranetDB.RunProcedureReader("SMSPanel.p_OccasionGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetOccasionDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OccasionEntity> GetAllOccasionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOccasionDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SMSPanel.p_OccasionGetAll", new IDataParameter[] { }));
        }
        public List<OccasionEntity> GetAllOccasionIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOccasionDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SMSPanel.p_OccasionIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<OccasionEntity> GetPageOccasionDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Occasion";
            parameters[5].Value = "OccasionId";
            DataSet ds = _intranetDB.RunProcedureDS("SMSPanel.p_TablesGetPage", parameters);
            return GetOccasionDBCollectionFromDataSet(ds, out count);
        }

        public OccasionEntity GetOccasionDBFromDataReader(IDataReader reader)
        {
            return new OccasionEntity(Guid.Parse(reader["OccasionId"].ToString()),
                                    reader["OccasionTitle"].ToString(),
                                    Guid.Parse(reader["OccasionTypeId"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<OccasionEntity> GetOccasionDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<OccasionEntity> lst = new List<OccasionEntity>();
                while (reader.Read())
                    lst.Add(GetOccasionDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OccasionEntity> GetOccasionDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetOccasionDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<OccasionEntity> GetOccasionDBCollectionByOccasionTypeDB(OccasionEntity occasionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = occasionEntityParam.OccasionTypeId;
            return GetOccasionDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SMSPanel.p_OccasionGetByOccasionType", new[] { parameter }));
        }


        #endregion



    }

}