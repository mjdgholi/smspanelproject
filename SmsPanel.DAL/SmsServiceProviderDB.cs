﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/19>
    /// Description: < مهیا کننده سرویس>
    /// </summary>

    //-----------------------------------------------------
    #region Class "SmsServiceProviderDB"

    public class SmsServiceProviderDB
    {
        #region Methods :

        public void AddSmsServiceProviderDB(SmsServiceProviderEntity smsServiceProviderEntityParam, out Guid SmsServiceProviderId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@SmsServiceProviderCode", SqlDbType.Int) ,
											  new SqlParameter("@SmsServiceProviderName", SqlDbType.NVarChar,150) ,
											  new SqlParameter("@SmsServiceProviderPersianName", SqlDbType.NVarChar,150) ,
											  new SqlParameter("@IsDefault", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = smsServiceProviderEntityParam.SmsServiceProviderCode;
            parameters[2].Value = smsServiceProviderEntityParam.SmsServiceProviderName;
            parameters[3].Value = smsServiceProviderEntityParam.SmsServiceProviderPersianName;
            parameters[4].Value = smsServiceProviderEntityParam.IsDefault;            

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_SmsServiceProviderAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SmsServiceProviderId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateSmsServiceProviderDB(SmsServiceProviderEntity smsServiceProviderEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@SmsServiceProviderCode", SqlDbType.Int) ,
											  new SqlParameter("@SmsServiceProviderName", SqlDbType.NVarChar,150) ,
											  new SqlParameter("@SmsServiceProviderPersianName", SqlDbType.NVarChar,150) ,
											  new SqlParameter("@IsDefault", SqlDbType.Bit) ,
	
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = smsServiceProviderEntityParam.SmsServiceProviderId;
            parameters[1].Value = smsServiceProviderEntityParam.SmsServiceProviderCode;
            parameters[2].Value = smsServiceProviderEntityParam.SmsServiceProviderName;
            parameters[3].Value = smsServiceProviderEntityParam.SmsServiceProviderPersianName;
            parameters[4].Value = smsServiceProviderEntityParam.IsDefault;


            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SmsPanel.p_SmsServiceProviderUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSmsServiceProviderDB(SmsServiceProviderEntity SmsServiceProviderEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = SmsServiceProviderEntityParam.SmsServiceProviderId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_SmsServiceProviderDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SmsServiceProviderEntity GetSingleSmsServiceProviderDB(SmsServiceProviderEntity SmsServiceProviderEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@SmsServiceProviderId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = SmsServiceProviderEntityParam.SmsServiceProviderId;

                reader = _intranetDB.RunProcedureReader("SmsPanel.p_SmsServiceProviderGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSmsServiceProviderDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SmsServiceProviderEntity> GetAllSmsServiceProviderDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSmsServiceProviderDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SmsPanel.p_SmsServiceProviderGetAll", new IDataParameter[] { }));
        }

        public List<SmsServiceProviderEntity> GetPageSmsServiceProviderDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_SmsServiceProvider";
            parameters[5].Value = "SmsServiceProviderId";
            DataSet ds = _intranetDB.RunProcedureDS("SmsPanel.p_TablesGetPage", parameters);
            return GetSmsServiceProviderDBCollectionFromDataSet(ds, out count);
        }

        public SmsServiceProviderEntity GetSmsServiceProviderDBFromDataReader(IDataReader reader)
        {
            return new SmsServiceProviderEntity(Guid.Parse(reader["SmsServiceProviderId"].ToString()),
                                    int.Parse(reader["SmsServiceProviderCode"].ToString()),
                                    reader["SmsServiceProviderName"].ToString(),
                                    reader["SmsServiceProviderPersianName"].ToString(),
                                    bool.Parse(reader["IsDefault"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<SmsServiceProviderEntity> GetSmsServiceProviderDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SmsServiceProviderEntity> lst = new List<SmsServiceProviderEntity>();
                while (reader.Read())
                    lst.Add(GetSmsServiceProviderDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SmsServiceProviderEntity> GetSmsServiceProviderDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSmsServiceProviderDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}