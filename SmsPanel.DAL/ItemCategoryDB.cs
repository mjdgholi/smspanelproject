﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    /// Description: < گروه بندی آیتم ها>
    /// </summary>

    //-----------------------------------------------------
    #region Class "ItemCategoryDB"

    public class ItemCategoryDB
    {



        #region Methods :

        public void AddItemCategoryDB(ItemCategoryEntity itemCategoryEntityParam, out Guid ItemCategoryId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ItemCategoryId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@OwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ItemGroupTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ShowOrder", SqlDbType.Int) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,				
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = (itemCategoryEntityParam.OwnerId==null?System.DBNull.Value:(object)itemCategoryEntityParam.OwnerId);
            parameters[2].Value = FarsiToArabic.ToArabic(itemCategoryEntityParam.ItemGroupTitle.Trim());
            parameters[3].Value = itemCategoryEntityParam.ShowOrder;
            parameters[4].Value = itemCategoryEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_ItemCategoryAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ItemCategoryId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateItemCategoryDB(ItemCategoryEntity itemCategoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ItemCategoryId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@OwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ItemGroupTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ShowOrder", SqlDbType.Int) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = itemCategoryEntityParam.ItemCategoryId;
            parameters[1].Value = (itemCategoryEntityParam.OwnerId == null ? System.DBNull.Value : (object)itemCategoryEntityParam.OwnerId);
            parameters[2].Value = FarsiToArabic.ToArabic(itemCategoryEntityParam.ItemGroupTitle.Trim());
            parameters[3].Value = itemCategoryEntityParam.ShowOrder;
            parameters[4].Value = itemCategoryEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SmsPanel.p_ItemCategoryUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteItemCategoryDB(ItemCategoryEntity ItemCategoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ItemCategoryId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ItemCategoryEntityParam.ItemCategoryId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SmsPanel.p_ItemCategoryDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ItemCategoryEntity GetSingleItemCategoryDB(ItemCategoryEntity ItemCategoryEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ItemCategoryId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ItemCategoryEntityParam.ItemCategoryId;

                reader = _intranetDB.RunProcedureReader("SmsPanel.p_ItemCategoryGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetItemCategoryDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ItemCategoryEntity> GetAllItemCategoryDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetItemCategoryDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SmsPanel.p_ItemCategoryGetAll", new IDataParameter[] { }));
        }

        public List<ItemCategoryEntity> GetPageItemCategoryDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ItemCategory";
            parameters[5].Value = "ItemCategoryId";
            DataSet ds = _intranetDB.RunProcedureDS("SmsPanel.p_TablesGetPage", parameters);
            return GetItemCategoryDBCollectionFromDataSet(ds, out count);
        }

        public ItemCategoryEntity GetItemCategoryDBFromDataReader(IDataReader reader)
        {
            return new ItemCategoryEntity(Guid.Parse(reader["ItemCategoryId"].ToString()),
                (Convert.IsDBNull(reader["OwnerId"])) ? (Guid?)null : Guid.Parse(reader["OwnerId"].ToString()),                                    
                                    reader["ItemGroupTitle"].ToString(),
                                    int.Parse(reader["ShowOrder"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<ItemCategoryEntity> GetItemCategoryDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ItemCategoryEntity> lst = new List<ItemCategoryEntity>();
                while (reader.Read())
                    lst.Add(GetItemCategoryDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ItemCategoryEntity> GetItemCategoryDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetItemCategoryDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

        public IList<ItemCategoryEntity> GetAllNodesDB(ItemCategoryEntity itemCategoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			             
											  new SqlParameter("@ItemGroupTitle", SqlDbType.NVarChar,200) ,
											  											  						
				     		  };

            
            parameters[0].Value = (itemCategoryEntityParam.ItemGroupTitle==""?System.DBNull.Value:(object)itemCategoryEntityParam.ItemGroupTitle);
            
            return GetItemCategoryDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SmsPanel.p_ItemCategoryWithFilter", parameters));
        }
    }

    #endregion
}