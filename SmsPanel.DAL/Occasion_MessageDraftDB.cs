﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/22>
    /// Description: < ارتباطی بین مناسبت و پیغام پیش نویس>
    /// </summary>

    

    public class Occasion_MessageDraftDB
    {
       
        #region Methods :

        public void AddOccasion_MessageDraftDB(Occasion_MessageDraftEntity occasionMessageDraftEntityParam, out Guid Occasion_MessageDraftId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@Occasion_MessageDraftId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@OccasionDayId", SqlDbType.Int) ,
											  new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = occasionMessageDraftEntityParam.OccasionDayId;
            parameters[2].Value = occasionMessageDraftEntityParam.MessageDraftId;
            parameters[3].Value = occasionMessageDraftEntityParam.IsActive;
           
            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_Occasion_MessageDraftAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            Occasion_MessageDraftId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateOccasion_MessageDraftDB(Occasion_MessageDraftEntity occasionMessageDraftEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@Occasion_MessageDraftId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@OccasionDayId", SqlDbType.Int) ,
											  new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											 
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = occasionMessageDraftEntityParam.Occasion_MessageDraftId;
            parameters[1].Value = occasionMessageDraftEntityParam.OccasionDayId;
            parameters[2].Value = occasionMessageDraftEntityParam.MessageDraftId;
            parameters[3].Value = occasionMessageDraftEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("SMSPanel.p_Occasion_MessageDraftUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteOccasion_MessageDraftDB(Occasion_MessageDraftEntity Occasion_MessageDraftEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@Occasion_MessageDraftId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = Occasion_MessageDraftEntityParam.Occasion_MessageDraftId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("SMSPanel.p_Occasion_MessageDraftDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public Occasion_MessageDraftEntity GetSingleOccasion_MessageDraftDB(Occasion_MessageDraftEntity Occasion_MessageDraftEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@Occasion_MessageDraftId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = Occasion_MessageDraftEntityParam.Occasion_MessageDraftId;

                reader = _intranetDB.RunProcedureReader("SMSPanel.p_Occasion_MessageDraftGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetOccasion_MessageDraftDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<Occasion_MessageDraftEntity> GetAllOccasion_MessageDraftDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOccasion_MessageDraftDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("SMSPanel.p_Occasion_MessageDraftGetAll", new IDataParameter[] { }));
        }

        public List<Occasion_MessageDraftEntity> GetPageOccasion_MessageDraftDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Occasion_MessageDraft";
            parameters[5].Value = "Occasion_MessageDraftId";
            DataSet ds = _intranetDB.RunProcedureDS("SMSPanel.p_TablesGetPage", parameters);
            return GetOccasion_MessageDraftDBCollectionFromDataSet(ds, out count);
        }

        public Occasion_MessageDraftEntity GetOccasion_MessageDraftDBFromDataReader(IDataReader reader)
        {
            return new Occasion_MessageDraftEntity(Guid.Parse(reader["Occasion_MessageDraftId"].ToString()),
                                    int.Parse(reader["OccasionDayId"].ToString()),
                                    Guid.Parse(reader["MessageDraftId"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["MessageDraftText"].ToString());
        }

        public List<Occasion_MessageDraftEntity> GetOccasion_MessageDraftDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<Occasion_MessageDraftEntity> lst = new List<Occasion_MessageDraftEntity>();
                while (reader.Read())
                    lst.Add(GetOccasion_MessageDraftDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<Occasion_MessageDraftEntity> GetOccasion_MessageDraftDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetOccasion_MessageDraftDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<Occasion_MessageDraftEntity> GetOccasion_MessageDraftDBCollectionByMessageDraftDB(Occasion_MessageDraftEntity occasionMessageDraftEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MessageDraftId", SqlDbType.UniqueIdentifier);
            parameter.Value = occasionMessageDraftEntityParam.MessageDraftId;
            return GetOccasion_MessageDraftDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SMSPanel.p_Occasion_MessageDraftGetByMessageDraft", new[] { parameter }));
        }

        public  List<Occasion_MessageDraftEntity> GetOccasion_MessageDraftDBCollectionByOccasionDB(Occasion_MessageDraftEntity occasionMessageDraftEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionDayId", SqlDbType.Int);
            parameter.Value = occasionMessageDraftEntityParam.OccasionDayId;
            return GetOccasion_MessageDraftDBCollectionFromDataReader(_intranetDB.RunProcedureReader("SMSPanel.p_Occasion_MessageDraftGetByOccasion", new[] { parameter }));
        }


        #endregion
    }
}