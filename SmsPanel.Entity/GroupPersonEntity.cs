﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    
    //-----93-04-18
    //----- majid Gholibeygian
    public class GroupPersonEntity
    {
        
        #region Properties :

        public Guid GroupPersonId { get; set; }
        public Guid GroupId { get; set; }
        public Guid PersonMoblieId { get; set; }


        public Guid PersonId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        public string PersonIdStr { get; set; }
        public string FullName   { get; set; }
        public string PersonMoblieIdStr { get; set; }

        #endregion

        #region Constrauctors :

        public GroupPersonEntity()
        {
        }

        public GroupPersonEntity(Guid _GroupPersonId, Guid GroupId,Guid PersonMoblieId, Guid PersonId, string CreationDate, string ModificationDate, string personIdStr,string PersonMoblieIdStr,string fullName)
        {
            GroupPersonId = _GroupPersonId;
            this.GroupId = GroupId;
            this.PersonId = PersonId;
            this.PersonMoblieId = PersonMoblieId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.PersonIdStr = personIdStr;
            this.PersonMoblieIdStr = PersonMoblieIdStr;
            this.FullName = fullName;

        }

        #endregion
 
    }
}