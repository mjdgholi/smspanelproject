﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/21>
    /// Description: < پیغام پیش نویس>
    /// </summary>
    public class MessageDraftEntity
    {  
        #region Properties :

        public Guid MessageDraftId { get; set; }
        public string MessageDraftText { get; set; }        
        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public MessageDraftEntity()
        {
        }

        public MessageDraftEntity(Guid _MessageDraftId, string MessageDraftText, bool IsActive, string CreationDate, string ModificationDate)
        {
            MessageDraftId = _MessageDraftId;
            this.MessageDraftText = MessageDraftText;            
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
 
         
    }
}