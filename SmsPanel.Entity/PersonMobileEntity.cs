﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/22>
    /// Description: <شماره موبایل شخص>
    /// </summary>
    public class PersonMobileEntity
    {
        #region Properties :

        public Guid PersonMoblieId { get; set; }
        public Guid PersonId { get; set; }

        public string MobileNo { get; set; }

        public bool IsDefault { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public string FullName { get; set; }    


        #endregion

        #region Constrauctors :

        public PersonMobileEntity()
        {
        }

        public PersonMobileEntity(Guid _PersonMoblieId, Guid PersonId, string MobileNo, bool IsDefault, bool IsActive, string CreationDate, string ModificationDate,string fullName)
        {
            PersonMoblieId = _PersonMoblieId;
            this.PersonId = PersonId;
            this.MobileNo = MobileNo;
            this.IsDefault = IsDefault;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.FullName = fullName;

        }

        #endregion

         
    }
}