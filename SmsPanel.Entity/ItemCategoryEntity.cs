﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    /// Description: < گروه بندی آیتم ها>
    /// </summary>
    public class ItemCategoryEntity
    {
                    #region Properties :

        public Guid ItemCategoryId { get; set; }
        public Guid? OwnerId { get; set; }

        public string ItemGroupTitle { get; set; }

        public int? ShowOrder { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public ItemCategoryEntity()
        {
        }

        public ItemCategoryEntity(Guid _ItemCategoryId, Guid? OwnerId, string ItemGroupTitle, int? ShowOrder, bool IsActive, string CreationDate, string ModificationDate)
        {
            ItemCategoryId = _ItemCategoryId;
            this.OwnerId = OwnerId;
            this.ItemGroupTitle = ItemGroupTitle;
            this.ShowOrder = ShowOrder;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}