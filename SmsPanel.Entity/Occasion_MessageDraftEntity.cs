﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/22>
    /// Description: < ارتباطی بین مناسبت و پیغام پیش نویس>
    /// </summary>
    public class Occasion_MessageDraftEntity
    {
        #region Properties :

        public Guid Occasion_MessageDraftId { get; set; }
        public int? OccasionDayId { get; set; }

        public Guid MessageDraftId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        public string MessageDraftText { get; set; } 



        #endregion

        #region Constrauctors :

        public Occasion_MessageDraftEntity()
        {
        }

        public Occasion_MessageDraftEntity(Guid _Occasion_MessageDraftId, int? OccasionDayId, Guid MessageDraftId, bool IsActive, string CreationDate, string ModificationDate, string MessageDraftText)
        {
            Occasion_MessageDraftId = _Occasion_MessageDraftId;
            this.OccasionDayId = OccasionDayId;
            this.MessageDraftId = MessageDraftId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.MessageDraftText = MessageDraftText;

        }

        #endregion
         
    }
}