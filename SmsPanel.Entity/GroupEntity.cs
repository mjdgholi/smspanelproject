﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{

    //-----93-04-18
    //----- majid Gholibeygian
    public class GroupEntity
    {
        #region Properties :

        public Guid GroupId { get; set; }
        public Guid? OwnerId { get; set; }

        public string GroupTitle { get; set; }

        public int ChildNo { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public GroupEntity()
        {
        }

        public GroupEntity(Guid groupId, Guid? ownerId, string groupTitle, int childNo, string creationDate, string modificationDate)
        {
            GroupId = groupId;
            this.OwnerId = ownerId;
            this.GroupTitle = groupTitle;
            this.ChildNo = childNo;
            this.CreationDate = creationDate;
            this.ModificationDate = modificationDate;

        }

        #endregion
    }
}