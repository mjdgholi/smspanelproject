﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <پیامک با آیتم>
    /// </summary>
    public class Group_Item_SmsEntity
    {
         #region Properties :

        public Guid Group_Item_SmsId { get; set; }
        public Guid GroupId { get; set; }

        public Guid ItemId { get; set; }

        public Guid? MessageDraftId { get; set; }

        public string MessageText { get; set; }

        public string SendTime { get; set; }

        public string FromDate { get; set; }

        public string EndDate { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public string GroupIdStr { get; set; }
        public Guid? ItemCategoryId   { get; set; }
        public int? OccasionDayTypeId   { get; set; }
        public int? OccasionDayId    { get; set; }
        public Guid? SmsServiceProviderId { get; set; }
        #endregion

        #region Constrauctors :

        public Group_Item_SmsEntity()
        {
        }

        public Group_Item_SmsEntity(Guid _Group_Item_SmsId, Guid GroupId, Guid ItemId, Guid? MessageDraftId, string MessageText, string SendTime, string FromDate, string EndDate, string CreationDate, string ModificationDate, Guid? ItemCategoryId, int? OccasionDayTypeId, int? OccasionDayId, Guid? SmsServiceProviderId)
        {
            Group_Item_SmsId = _Group_Item_SmsId;
            this.GroupId = GroupId;
            this.ItemId = ItemId;
            this.MessageDraftId = MessageDraftId;
            this.MessageText = MessageText;
            this.SendTime = SendTime;
            this.FromDate = FromDate;
            this.EndDate = EndDate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;            
            this.ItemCategoryId = ItemCategoryId;
            this.OccasionDayTypeId = OccasionDayTypeId;
            this.OccasionDayId = OccasionDayId;
            this.SmsServiceProviderId = SmsServiceProviderId;
        }

        #endregion
    }
}