﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/21>
    /// Description: < مناسبت>
    /// </summary>
	public class OccasionEntity
	{
        #region Properties :

        public Guid OccasionId { get; set; }
        public string OccasionTitle { get; set; }

        public Guid OccasionTypeId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public OccasionEntity()
        {
        }

        public OccasionEntity(Guid _OccasionId, string OccasionTitle, Guid OccasionTypeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            OccasionId = _OccasionId;
            this.OccasionTitle = OccasionTitle;
            this.OccasionTypeId = OccasionTypeId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
	}
}
