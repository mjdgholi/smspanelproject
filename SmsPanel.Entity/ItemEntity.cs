﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <آیتم ها>
    /// </summary>
    public class ItemEntity
    {
          #region Properties :

        public Guid ItemId { get; set; }
        public Guid? ItemCategoryId { get; set; }

        public string ItemTitle { get; set; }

        public string Priority { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public ItemEntity()
        {
        }

        public ItemEntity(Guid _ItemId, Guid? ItemCategoryId, string ItemTitle, string Priority, bool IsActive, string CreationDate, string ModificationDate)
        {
            ItemId = _ItemId;
            this.ItemCategoryId = ItemCategoryId;
            this.ItemTitle = ItemTitle;
            this.Priority = Priority;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion 
    }
}