﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/23>
    /// Description: < بسته پیامک>
    /// </summary>
    public class MessagePackageHistoryEntity
    {
                  #region Properties :

        public Guid MessagePackageHistoryId { get; set; }
        public int AcctionTypeId { get; set; }

        public int? OccasionDayId { get; set; }

        public Guid? ItemId { get; set; }

        public Guid? PersonMoblieId { get; set; }

        public int? UserAndGroupId { get; set; }

        public Guid? MessageDraftId { get; set; }

        public string MessageText { get; set; }

        public string PersonMoblieIdStr { get; set; }

        public DateTime SendDate { get; set; }
        public Guid? SmsServiceProviderId { get; set; }
        public Guid? RelationShipId { get; set; }
        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public string SendTime { get; set; }
        public  string GroupIdStr { get; set; }
        public string PersonMoblieIdShowSelectControlStr  { get; set; }
        public string FullNameShowSelectControlStr { get; set; }

        #endregion

        #region Constrauctors :

        public MessagePackageHistoryEntity()
        {
        }

        public MessagePackageHistoryEntity(Guid _MessagePackageHistoryId, int AcctionTypeId, int? OccasionDayId, Guid? ItemId, Guid? PersonMoblieId, int? UserAndGroupId, Guid? MessageDraftId, string MessageText, string PersonMoblieIdStr, DateTime SendDate, Guid? SmsServiceProviderId, Guid? RelationShipId, string CreationDate, string ModificationDate, string SendTime, string GroupIdStr, string PersonMoblieIdShowSelectControlStr, string FullNameShowSelectControlStr)
        {
            MessagePackageHistoryId = _MessagePackageHistoryId;
            this.AcctionTypeId = AcctionTypeId;
            this.OccasionDayId = OccasionDayId;
            this.ItemId = ItemId;
            this.PersonMoblieId = PersonMoblieId;
            this.UserAndGroupId = UserAndGroupId;
            this.MessageDraftId = MessageDraftId;
            this.MessageText = MessageText;
            this.PersonMoblieIdStr = PersonMoblieIdStr;
            this.SendDate = SendDate;
            this.SmsServiceProviderId = SmsServiceProviderId;
            this.RelationShipId = RelationShipId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.SendTime = SendTime;
            this.GroupIdStr = GroupIdStr;
            this.PersonMoblieIdShowSelectControlStr = PersonMoblieIdShowSelectControlStr;
            this.FullNameShowSelectControlStr = FullNameShowSelectControlStr;
        }

        #endregion
    }
}