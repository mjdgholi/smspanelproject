﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/17>
    /// Description: <پیامک با مناسبت>
    /// </summary>
    public class Group_Occasion_SMSEnity
    {
         
        #region Properties :

        public Guid Group_Occasion_SMSId { get; set; }

        public Guid SmsServiceProviderId { get; set; }
        
        
        public Guid GroupId { get; set; }

        public int OccasionDayId { get; set; }

        public Guid? MessageDraftId { get; set; }

        public string MessageText { get; set; }

        public string SendTime { get; set; }

        public int DifferenceDay { get; set; }  
        public string FromDate { get; set; }

        public string EndDate { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        public string GroupIdStr { get; set; }
        public int? OccasionDayTypeId { get; set; }

        #endregion

        #region Constrauctors :

        public Group_Occasion_SMSEnity()
        {
        }

        public Group_Occasion_SMSEnity(Guid _Group_Occasion_SMSId, Guid smsServiceProviderId, Guid GroupId, int OccasionDayId, Guid? MessageDraftId, string MessageText, string SendTime, int DifferenceDay, string FromDate, string EndDate, string CreationDate, string ModificationDate, int? OccasionDayTypeId)
        {
            Group_Occasion_SMSId = _Group_Occasion_SMSId;
            this.SmsServiceProviderId = smsServiceProviderId;
            this.GroupId = GroupId;
            this.OccasionDayId = OccasionDayId;
            this.MessageDraftId = MessageDraftId;
            this.MessageText = MessageText;
            this.SendTime = SendTime;
            this.DifferenceDay = DifferenceDay;
            this.FromDate = FromDate;
            this.EndDate = EndDate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.OccasionDayTypeId = OccasionDayTypeId;

        }

        #endregion
    }
}