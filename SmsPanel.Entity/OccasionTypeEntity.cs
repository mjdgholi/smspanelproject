﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/21>
    /// Description: <نوع مناسبت>
    /// </summary>
    public class OccasionTypeEntity
    {
        #region Properties :

        public Guid OccasionTypeId { get; set; }
        public string OccasionTypeTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public OccasionTypeEntity()
        {
        }

        public OccasionTypeEntity(Guid _OccasionTypeId, string OccasionTypeTitle, bool IsActive, string CreationDate,
            string ModificationDate)
        {
            OccasionTypeId = _OccasionTypeId;
            this.OccasionTypeTitle = OccasionTypeTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
