﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/19>
    /// Description: < مهیا کننده سرویس>
    /// </summary>
    public class SmsServiceProviderEntity
    {
                 #region Properties :

        public Guid SmsServiceProviderId { get; set; }
        public int SmsServiceProviderCode { get; set; }

        public string SmsServiceProviderName { get; set; }

        public string SmsServiceProviderPersianName { get; set; }

        public bool IsDefault { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public SmsServiceProviderEntity()
        {
        }

        public SmsServiceProviderEntity(Guid _SmsServiceProviderId, int SmsServiceProviderCode, string SmsServiceProviderName, string SmsServiceProviderPersianName, bool IsDefault, string CreationDate, string ModificationDate)
        {
            SmsServiceProviderId = _SmsServiceProviderId;
            this.SmsServiceProviderCode = SmsServiceProviderCode;
            this.SmsServiceProviderName = SmsServiceProviderName;
            this.SmsServiceProviderPersianName = SmsServiceProviderPersianName;
            this.IsDefault = IsDefault;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}