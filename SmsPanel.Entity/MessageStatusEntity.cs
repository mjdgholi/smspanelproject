﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/22>
    /// Description: <وضیعت پیامک>
    /// </summary>
    public class MessageStatusEntity
    {
  #region Properties :

            public int MessageStatusId { get; set; }
            public int MessageStatusCode { get; set; }
            public Guid? SmsServiceProviderId { get; set; }
            public string MessageStatusEnglishTitle { get; set; }
            public string MessageStatusPersianTitle { get; set; }
        

            public bool IsActive { get; set; }

            public string CreationDate { get; set; }

            public string ModificationDate { get; set; }



            #endregion

            #region Constrauctors :

            public MessageStatusEntity()
            {
            }

            public MessageStatusEntity(int _MessageStatusId, int MessageStatusCode, Guid? SmsServiceProviderId, string MessageStatusEnglishTitle, string MessageStatusPersianTitle, bool IsActive, string CreationDate, string ModificationDate)
            {
                MessageStatusId = _MessageStatusId;
                this.MessageStatusCode =MessageStatusCode;
                this.SmsServiceProviderId = SmsServiceProviderId;
                this.MessageStatusEnglishTitle = MessageStatusEnglishTitle;
                this.MessageStatusPersianTitle = MessageStatusPersianTitle;
                this.IsActive = IsActive;
                this.CreationDate = CreationDate;
                this.ModificationDate = ModificationDate;

            }

            #endregion
    }
}