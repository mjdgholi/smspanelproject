﻿using System;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <آیتم فرد>
    /// </summary>
    public class PersonItemEntity
    {
                 #region Properties :

        public Guid PersonItemId { get; set; }
        public Guid PersonId { get; set; }

        public Guid ItemId { get; set; }

        public string Date { get; set; }

        public int MonthId { get; set; }

        public int DayNo { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public Guid ItemCategoryId { get; set; }
        public string FullName { get; set; }


        #endregion

        #region Constrauctors :

        public PersonItemEntity()
        {
        }

        public PersonItemEntity(Guid _PersonItemId, Guid PersonId, Guid ItemId, string Date, int MonthId, int DayNo, bool IsActive, string CreationDate, string ModificationDate, Guid ItemCategoryId,string FullName)
        {
            PersonItemId = _PersonItemId;
            this.PersonId = PersonId;
            this.ItemId = ItemId;
            this.Date = Date;
            this.MonthId = MonthId;
            this.DayNo = DayNo;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.ItemCategoryId = ItemCategoryId;
            this.FullName = FullName;

        }

        #endregion
    }
}