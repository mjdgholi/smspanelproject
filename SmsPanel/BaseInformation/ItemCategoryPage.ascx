﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemCategoryPage.ascx.cs" Inherits="Intranet.DesktopModules.SmsPanelProject.SmsPanel.BaseInformation.ItemCategoryPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
<script type="text/javascript" >
    function NodeClicking(sender, eventArgs) {
        var node = eventArgs.get_node();
        var isSelected = node.get_selected();

        if (isSelected) {
            node.set_selected(false);
        }
        else {
            node.set_selected(true);
        }

        eventArgs.set_cancel(true);
    }
    function ContextMenuClick(sender, eventArgs) {
        var node = eventArgs.get_node();
        var item = eventArgs.get_menuItem();
        if (item.get_value() == "remove") {
            eventArgs.set_cancel(!confirm('آیا مطمئن هستید؟'));
        }
    }  
</script>

</telerik:RadScriptBlock>

<asp:Panel ID="pnlAll" runat="server" BackColor="White">
    <table dir="rtl" width="100%" class="MainTableOfASCX" >
        <tr>
            <td >
                <table dir="rtl">
                    <tr>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click">
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                                <Icon PrimaryIconCssClass="rbEdit" />
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnBack" runat="server" CausesValidation="False" 
                                CustomeButtonType="Back" onclick="BtnBack_Click"
                                >
                                <Icon PrimaryIconCssClass="rbBack" />
                            </cc1:CustomRadButton>
                        </td>
              
                        <td align="right">
                            <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                                  <td width="10%" nowrap="nowrap">
                <asp:Label ID="LblItemGroupTitle" runat="server" Text="عنوان گروه بندی مناسبت فردی:"></asp:Label>
            </td>
            <td width="30%">
                <telerik:RadTextBox ID="txtItemGroupTitle" runat="server"  Width="300px">
                </telerik:RadTextBox>
            </td>
            <td >
                <asp:RequiredFieldValidator ID="rfvItemGroupTitle" runat="server" ControlToValidate="txtItemGroupTitle"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td> 
                    </tr>
                            <tr>
            <td nowrap="nowrap" >
                <asp:Label ID="LblShowOrder" runat="server" Text="ترتیب:"></asp:Label>
            </td>
            <td >
                <telerik:RadNumericTextBox ID="txtShowOrder" runat="server" 
                    Width="50px">
                    <NumberFormat DecimalDigits="0" ZeroPattern="n" />
                </telerik:RadNumericTextBox>
            </td>
            <td >
                <asp:RequiredFieldValidator ID="rfvShowOrder" runat="server" 
                    ControlToValidate="txtShowOrder" ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>

        </tr>
                                <tr>
                            <td>
                                <label>
                                    وضیعت رکورد<font color="red">*</font>:</label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbIsActive" runat="server">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
               
                            </td>
                            <td>
                                                 <asp:RequiredFieldValidator ID="rfvalidatorVisibility" runat="server" ControlToValidate="cmbIsActive"
                                    ErrorMessage="وضیعت رکورد" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                </table>
            </td>

        </tr>

        <tr>
            <td  nowrap="nowrap" valign="top">
                <asp:Label ID="LblParentCategorize" runat="server" Text="گروه مناسبت فردی دربرگیرنده:"></asp:Label>
            </td>
            </tr>
            <tr>
            <td  valign="top">
                <table width="100%">
<%--                    <tr>
                        <td width="15%">
                            <telerik:RadTextBox ID="TxtGroupName" runat="server" LabelWidth="75px" Width="120px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgbtnSearch" runat="server" CausesValidation="False" Height="25px"
                                ImageUrl="../Images/SearchOrganizationPhysicalChart.png" 
                                ToolTip="جستجو" Width="25px" />
                        </td>

                    </tr>--%>
                    <tr>
                        <td>
                            <telerik:RadTreeView ID="trvItemCategory" runat="server" BorderColor="#999999" Skin="Office2007"
                                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" dir="rtl" OnClientContextMenuItemClicking="ContextMenuClick" OnClientNodeClicking="NodeClicking" 
                                
                                Width="99%" oncontextmenuitemclick="trvItemCategory_ContextMenuItemClick">
                                <ContextMenus>
                                    <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server" Skin="Office2007">
                                        <Items>
                                            <telerik:RadMenuItem ImageUrl="../Images/edit.png" PostBack="True" Text="ویرایش"
                                                Value="edit">
                                            </telerik:RadMenuItem>
                                            <telerik:RadMenuItem ImageUrl="../Images/delete.png" Text="حذف" Value="remove">
                                            </telerik:RadMenuItem>
                                        </Items>
                                        <CollapseAnimation Type="OutQuint" />
                                    </telerik:RadTreeViewContextMenu>
                                </ContextMenus>
                            </telerik:RadTreeView>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
       
    </table>
</asp:Panel>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="BtnAdd">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="trvItemCategory">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Office2007">
</telerik:RadAjaxLoadingPanel>