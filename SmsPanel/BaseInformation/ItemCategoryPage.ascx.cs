﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;
using ITC.Library.Controls;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    /// Description: < گروه بندی مناسبت فردی ها>
    /// </summary>
    public partial class ItemCategoryPage : ItcBaseControl
    {
         private ArrayList _searchGroupIdList = new ArrayList();
        private ItemCategoryBL _itemCategoryBL=new ItemCategoryBL();


#region procedure

        protected void SetTrvGroup(string groupTitle)
        {
            trvItemCategory.DataTextField = "ItemGroupTitle";
            trvItemCategory.DataValueField = "ItemCategoryId";
            trvItemCategory.DataFieldID = "ItemCategoryId";
            trvItemCategory.DataFieldParentID = "OwnerId";
            ItemCategoryEntity itemCategoryEntity = new ItemCategoryEntity()
            {
                ItemGroupTitle = groupTitle
            }
                ;
            trvItemCategory.DataSource = _itemCategoryBL.GetAllNodes(itemCategoryEntity);
            trvItemCategory.DataBind();
            if (trvItemCategory.Nodes.Count > 0)
            {
                trvItemCategory.Nodes[0].Expanded = true;
            }
        }



        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            BtnAdd.Visible = true;            
            BtnEdit.Visible = false;
            BtnBack.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            BtnAdd.Visible = false;
            BtnBack.Visible = true;            
            BtnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtItemGroupTitle.Text = "";
            txtShowOrder.Text = "";            
            cmbIsActive.SelectedIndex = 1;
            trvItemCategory.UnselectAllNodes();

        }

        private void SetDataShow()
        {
            var itemCategoryEntity = new ItemCategoryEntity()
            {
                ItemCategoryId = new Guid(ViewState["ItemCategoryId"].ToString())
            };
            ItemCategoryEntity myItemCategoryEntity = _itemCategoryBL.GetSingleById(itemCategoryEntity);
            txtItemGroupTitle.Text = myItemCategoryEntity.ItemGroupTitle;
            txtShowOrder.Text = myItemCategoryEntity.ShowOrder.ToString();
            cmbIsActive.SelectedValue = myItemCategoryEntity.IsActive.ToString();
             SetSelectedNodeForTree((myItemCategoryEntity.OwnerId==null)?(Guid?)null: Guid.Parse(myItemCategoryEntity.OwnerId.ToString()));
        }

        protected void SetSelectedNodeForTree(Guid? treeId)
        {
            SetTrvGroup("");
            if (treeId != null)
            {
                foreach (var radTreeNode in trvItemCategory.GetAllNodes())
                {
                    if (Guid.Parse(radTreeNode.Value) == treeId)
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Selected = true;

                    }
                }
            }
        }
        #endregion







#region ControlEvent
                /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";               
                SetTrvGroup("");
                BtnEdit.Visible = false;
                Session["ItemSearch"] = "";
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                    ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                    ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                    pnlAll.DefaultButton = "BtnAdd";
                    SetTrvGroup("");
                    BtnEdit.Visible = false;
                    Session["ItemSearch"] = "";
                    SetClearToolBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

        }
     
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Guid itemCategoryId;
               ItemCategoryEntity itemCategoryEntity=new ItemCategoryEntity()
                   {
                       
                       ItemGroupTitle = txtItemGroupTitle.Text,
                       ShowOrder = (txtShowOrder.Text==""?(int?)null:int.Parse(txtShowOrder.Text)),
                       OwnerId = (String.IsNullOrEmpty(trvItemCategory.SelectedValue)?(Guid?)null:Guid.Parse(trvItemCategory.SelectedValue)),
                       IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),                       
                       
                       };
               _itemCategoryBL.Add(itemCategoryEntity,out itemCategoryId);
                SetTrvGroup("");                
                SetSelectedNodeForTree(itemCategoryId);
                SetClearToolBox();
               CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {               
                   ItemCategoryEntity itemCategoryEntity=new ItemCategoryEntity()
                   {
                       ItemCategoryId = Guid.Parse(ViewState["ItemCategoryId"].ToString()),
                       ItemGroupTitle = txtItemGroupTitle.Text,
                       ShowOrder = (txtShowOrder.Text==""?(int?)null:int.Parse(txtShowOrder.Text)),
                       OwnerId = (String.IsNullOrEmpty(trvItemCategory.SelectedValue)?(Guid?)null:Guid.Parse(trvItemCategory.SelectedValue)),
                       IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),                       
                       
                       };
                _itemCategoryBL.Update(itemCategoryEntity);
                SetTrvGroup("");
                SetPanelFirst();
                SetSelectedNodeForTree(Guid.Parse(ViewState["ItemCategoryId"].ToString()));
                SetClearToolBox();
               CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            SetClearToolBox();
            SetPanelFirst();
        }
        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            SetClearToolBox();
            SetTrvGroup("");
        }


        protected void trvItemCategory_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            try
            {
                RadTreeNode clickedNode = e.Node;
                if (e.MenuItem.Value.ToLower() == "edit")
                {
                    ViewState["ItemCategoryId"] = new Guid(clickedNode.Value);

                    SetDataShow();
                    SetPanelLast();
                }
                if (e.MenuItem.Value.ToLower() == "remove")
                {
                    ItemCategoryEntity itemCategoryEntity = new ItemCategoryEntity()
                    {
                        ItemCategoryId = Guid.Parse(clickedNode.Value)
                    };
                    _itemCategoryBL.Delete(itemCategoryEntity);
                    SetTrvGroup("");


                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }




        #endregion


        

       
    }
}
