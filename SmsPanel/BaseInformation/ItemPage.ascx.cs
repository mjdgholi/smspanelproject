﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <مناسبت فردی ها>
    /// </summary>
    public partial class ItemPage : ItcBaseControl
    {

        #region PublicParam:
        private readonly ItemCategoryBL _itemCategoryBL = new ItemCategoryBL();
        private readonly ItemBL _itemBL = new ItemBL();
        private const string TableName = "SMSPanel.V_Item";
        private const string PrimaryKey = "ItemId";

        #endregion


        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtItemTitle.Text = "";
            txtPriority.Text = "";
            radtreeItemCategory.UnselectAllNodes();            
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var itemEntity = new ItemEntity()
            {
                ItemId = new Guid(ViewState["ItemId"].ToString())
            };
            var myitem = _itemBL.GetSingleById(itemEntity);
            txtItemTitle.Text = myitem.ItemTitle;
            txtPriority.Text = myitem.Priority;            
            cmbIsActive.SelectedValue = myitem.IsActive.ToString();
            SetSelectedNodeForTree((myitem.ItemCategoryId == null) ? (Guid?)null : Guid.Parse(myitem.ItemCategoryId.ToString()));
        }


        protected void SetSelectedNodeForTree(Guid? treeId)
        {
            SetTrvGroup("");
            if (treeId != null)
            {
                foreach (var radTreeNode in radtreeItemCategory.GetAllNodes())
                {
                    if (Guid.Parse(radTreeNode.Value) == treeId)
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Selected = true;

                    }
                }
            }
        }

        protected void SetTrvGroup(string groupTitle)
        {
            radtreeItemCategory.DataTextField = "ItemGroupTitle";
            radtreeItemCategory.DataValueField = "ItemCategoryId";
            radtreeItemCategory.DataFieldID = "ItemCategoryId";
            radtreeItemCategory.DataFieldParentID = "OwnerId";
            ItemCategoryEntity itemCategoryEntity = new ItemCategoryEntity()
            {
                ItemGroupTitle = groupTitle
            }
                ;
            radtreeItemCategory.DataSource = _itemCategoryBL.GetAllNodes(itemCategoryEntity);
            radtreeItemCategory.DataBind();
            if (radtreeItemCategory.Nodes.Count > 0)
            {
                radtreeItemCategory.Nodes[0].Expanded = true;
            }
        }
        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdItem,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        private void CheckValidation()
        {
            if (String.IsNullOrEmpty(radtreeItemCategory.SelectedValue))
            {
                throw new ItcApplicationErrorManagerException("از گروه مناسبت فردی یک نود را انتخاب نمایید");
            }
        }

        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetTrvGroup("");
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetTrvGroup("");
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid itemId ;
                ViewState["WhereClause"] = "";
                CheckValidation();
                var itemEntity = new ItemEntity()
                {                    
                    ItemCategoryId = Guid.Parse(radtreeItemCategory.SelectedNode.Value),
                    ItemTitle = txtItemTitle.Text,
                    Priority = txtPriority.Text,
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)
                };

                _itemBL.Add(itemEntity, out itemId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, itemId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }



        /// <summary>
        /// جستجو بر اساس مناسبت فردیهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtItemTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ItemTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtItemTitle.Text.Trim()) + "%'";
                if (txtPriority.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Priority ='" +
                                               txtPriority.Text.Trim() + "'";
                if(!String.IsNullOrEmpty(radtreeItemCategory.SelectedValue))
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ItemCategoryId ='" +
                            radtreeItemCategory.SelectedNode.Value+ "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdItem.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdItem.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                CheckValidation();
                var itemEntity = new ItemEntity()
                {
                    ItemCategoryId = Guid.Parse(radtreeItemCategory.SelectedNode.Value),
                    ItemTitle = txtItemTitle.Text,
                    Priority = txtPriority.Text,
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    ItemId = Guid.Parse(ViewState["ItemId"].ToString())
                };
                _itemBL.Update(itemEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, new Guid(ViewState["ItemId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void radtreeItemCategory_NodeClick(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {
            try
            {

                ViewState["WhereClause"] = "  ItemCategoryId ='" +
               radtreeItemCategory.SelectedNode.Value + "'";
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdItem_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyEdit")
                {
                    ViewState["ItemId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyDelete")
                {
                    ViewState["WhereClause"] = "";

                    var itemEntity = new ItemEntity()
                    {
                        ItemId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _itemBL.Delete(itemEntity);
                    var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>



        protected void grdItem_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdItem_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {

            try
            {
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdItem_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion
        

        
    }
}