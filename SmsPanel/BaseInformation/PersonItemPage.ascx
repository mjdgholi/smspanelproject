﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonItemPage.ascx.cs" Inherits="Intranet.DesktopModules.SmsPanelProject.SmsPanel.BaseInformation.PersonItemPage" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; " class="MainTableOfASCX" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    >
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    >
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
          
                            <td>
                             <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr >
            <td>
                <table style="width: 100%;" dir="rtl">
                    <tr>
                        <td width="25%" valign="top">
                            <table width="100%">
                                <tr>
                                    <td valign="top">
                                        <asp:Label ID="labItemCategory" runat="server" Text="گروه بندی مناسبت فردی"
                                            Font-Bold="True" Font-Names="Arial" Font-Size="12pt" ForeColor="#000099"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadTreeView ID="radtreeItemCategory" runat="server" Height="450px" 
                                            CausesValidation="False" onnodeclick="radtreeItemCategory_NodeClick">
                                        </telerik:RadTreeView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <table width="100%"  align="right">
                                <tr>
                                    <td valign="top">
                                        <asp:Panel ID="pnlToolBox" runat="server" Width="100%">
                                            <table width="100%">
                                                <tr>
                                                    <td align="right" dir="rtl" style="height: 20px; width: 15%;" valign="top">
                                                        <asp:Label ID="lblItemTitle" runat="server" Text="">عنوان مناسبت فردی<font color="red">*</font>:</asp:Label>
                                                    </td>
                                                    <td >
                                                        <cc1:CustomRadComboBox ID="cmbItemTitle" runat="server" 
                                                            AppendDataBoundItems="True">
                                                        </cc1:CustomRadComboBox>
                                                        <asp:RequiredFieldValidator ID="rfvItemTitle" runat="server" ControlToValidate="cmbItemTitle"
                                                           ErrorMessage="عنوان مناسبت فردی">*</asp:RequiredFieldValidator>
                                               
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblDate"> تاریخ<font color="red">*</font>:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <cc1:CustomItcCalendar ID="txtDate" runat="server"></cc1:CustomItcCalendar>
                                                        <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                                             ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                                               <tr>
                                        <td>
                                            <label>
                                                اشخاص<font color="red">*</font>:</label>
                                        </td>
                                        <td>
                                            <cc1:SelectControl ID="SelectControlPerson" runat="server" PortalPathUrl="GeneralProject/General/ModalForm/PersonPage.aspx" imageName="SelectPerson"  />
                                        </td>
                                    </tr>
              
                                                <tr>
                                                    <td>
                                                        <label>
                                                            وضیعت رکورد<font color="red">*</font>:</label>
                                                    </td>
                                                    <td>
                                                        <cc1:CustomRadComboBox ID="cmbIsActive" runat="server">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Text="" Value="" />
                                                                <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                                                <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                                            </Items>
                                                        </cc1:CustomRadComboBox>
                                                        <asp:RequiredFieldValidator ID="rfvalidatorVisibility" runat="server" ControlToValidate="cmbIsActive"
                                                            ErrorMessage="وضیعت رکورد" InitialValue=" ">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>

                                <tr>
                                    <td valign="top" bgcolor="White">
                                        <asp:Panel ID="pnlGrid" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <telerik:RadGrid ID="grdPersonItem" runat="server" AllowPaging="True"
                                                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" GridLines="None"
                                                            AllowCustomPaging="True" BorderWidth="3px" Width="98%" 
                                                            onitemcommand="grdPersonItem_ItemCommand" 
                                                            onpageindexchanged="grdPersonItem_PageIndexChanged" 
                                                            onpagesizechanged="grdPersonItem_PageSizeChanged" 
                                                            onsortcommand="grdPersonItem_SortCommand">
                                                            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                                                            </HeaderContextMenu>
                                                            <MasterTableView DataKeyNames="PersonItemId" Dir="RTL" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                                                    <HeaderStyle Width="20px" />
                                                                </RowIndicatorColumn>
                                                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                                                    <HeaderStyle Width="20px" />
                                                                </ExpandCollapseColumn>
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="ItemTitle" HeaderText="عنوان مناسبت فردی"
                                                                        SortExpression="ItemTitle">
                                                                    </telerik:GridBoundColumn>
                                                                         <telerik:GridBoundColumn DataField="FirstName" HeaderText="نام"
                                                                        SortExpression="FirstName">
                                                                    </telerik:GridBoundColumn>
                                                                         <telerik:GridBoundColumn DataField="LastName" HeaderText="نام خانوادگی"
                                                                        SortExpression="LastName">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ItemGroupTitle" HeaderText="عنوان دسته بندی مناسبت فردی" SortExpression="ItemGroupTitle">
                                                                    </telerik:GridBoundColumn>
                                                             <telerik:GridBoundColumn DataField="date" HeaderText="تاریخ" SortExpression="Date">
                                                                    </telerik:GridBoundColumn>
                                                     
                                          
                                                                    <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                                                    </telerik:GridCheckBoxColumn>
                                                                    
                                                                    <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                                        UniqueName="TemplateColumn1">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("PersonItemId").ToString() %>'
                                                                                CommandName="_MyEdit" ForeColor="#000066" ImageUrl="../Images/Edit.bmp" /></ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                                        UniqueName="TemplateColumn">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                                                CommandArgument='<%#Eval("PersonItemId").ToString() %>' CommandName="_MyDelete"
                                                                                ForeColor="#000066" ImageUrl="../Images/Delete.bmp" /></ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                      
                                                                </Columns>
                                                                <EditFormSettings>
                                                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                                                    </EditColumn>
                                                                </EditFormSettings>
                                                                <PagerStyle FirstPageToolTip="صفحه اول" LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی"
                                                                    NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, مناسبت فردی &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                                                    PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                                                    VerticalAlign="Middle" HorizontalAlign="Center" />
                                                            </MasterTableView><HeaderStyle Height="40px" />
                                                            <ClientSettings>
                                                                <Selecting AllowRowSelect="False" />
                                                            </ClientSettings>
                                                            <FilterMenu EnableImageSprites="False">
                                                            </FilterMenu>
                                                        </telerik:RadGrid>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="radtreeItemCategory" />
                <telerik:AjaxUpdatedControl ControlID="pnlToolBox" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="radtreeItemCategory" />
                <telerik:AjaxUpdatedControl ControlID="pnlToolBox" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="radtreeItemCategory" />
                <telerik:AjaxUpdatedControl ControlID="pnlToolBox" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="radtreeItemCategory" />
                <telerik:AjaxUpdatedControl ControlID="pnlToolBox" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="radtreeItemCategory" />
                <telerik:AjaxUpdatedControl ControlID="pnlToolBox" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radtreeItemCategory">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="radtreeItemCategory" />
                <telerik:AjaxUpdatedControl ControlID="cmbItemTitle" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdPersonItem">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="radtreeItemCategory" />
                <telerik:AjaxUpdatedControl ControlID="pnlToolBox" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" 
    Skin="Default">
</telerik:RadAjaxLoadingPanel>
