﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupPage.ascx.cs" Inherits="Intranet.DesktopModules.SmsPanelProject.SmsPanel.BaseInformation.GroupPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>

<telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <script type="text/javascript">

        var myValue = new Array(<%=Session["ItemSearch"].ToString() %>); 
        var index= -1;
           
        function NextNode() {    
            alert('a');
            if (index <  myValue.length - 1) {                      
                index = index + 1;                 
                expandNode(myValue[index]);
                   
                return true;
            }
            else
            {

                return false;
            }
        }

        function PreviousNode() {
            //  alert(index);
            if (index > 0)
            {
                index = index - 1;  
                expandNode(myValue[index]);               
                return true;
            }
            else
            {
                return false;
            }
        }


        function expandNode(nodeid) {
            var treeView = $find("<%= trvGroup.ClientID %>");
            var node = treeView.findNodeByValue(nodeid);
            if (node) {
                if (node.get_parent() != node.get_treeView()) {
                    node.get_parent().expand(); // root node must not expand 

                    node.get_parent().expand();
                    node.expand();
                    //node.select();
                    node.set_selected(true);
                    scrollToNode(treeView, node);
                    return true;
                }
                return false;
            }
        }


        function scrollToNode(treeview, node) {
                   var nodeElement = node.get_contentElement();
                   var treeViewElement = treeview.get_element();

                   var nodeOffsetTop = treeview._getTotalOffsetTop(nodeElement);
                   var treeOffsetTop = treeview._getTotalOffsetTop(treeViewElement);
                   var relativeOffsetTop = nodeOffsetTop - treeOffsetTop;

                   if (relativeOffsetTop < treeViewElement.scrollTop) {
                       treeViewElement.scrollTop = relativeOffsetTop;
                   }

                   var height = nodeElement.offsetHeight;

                   if (relativeOffsetTop + height > (treeViewElement.clientHeight + treeViewElement.scrollTop)) {
                       treeViewElement.scrollTop += ((relativeOffsetTop + height) - (treeViewElement.clientHeight + treeViewElement.scrollTop));
                   }
               }               
    </script>
</telerik:RadCodeBlock>
<asp:Panel ID="pnlAll" runat="server">
    <table style="width: 100%; " class="MainTableOfASCX" dir="rtl">
        <tr>
            <td colspan="6">
                <table dir="rtl">
                    <tr>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click">
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                                <Icon PrimaryIconCssClass="rbEdit" />
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnSearch" runat="server" CausesValidation="False" CustomeButtonType="Search"
                                OnClick="BtnSearch_Click">
                                <Icon PrimaryIconCssClass="rbSearch" />
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnShowAll" runat="server" CausesValidation="False" CustomeButtonType="ShowAll"
                                OnClick="BtnShowAll_Click">
                                <Icon PrimaryIconCssClass="rbRefresh" />
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomMessageErrorControl ID="MessageErrorControl1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="10%" nowrap="nowrap">
                <asp:Label ID="LblName" runat="server" Text="نام گروه کاربری:"></asp:Label>
            </td>
            <td width="30%">
                <telerik:RadTextBox ID="TxtName" runat="server" Skin="Office2007" Width="300px">
                </telerik:RadTextBox>
            </td>
            <td width="10%">
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="TxtName"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td width="10%"></td>
            <td width="30%"></td>
            <td width="10%"></td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="10%">
                <asp:Label ID="LblPriority" runat="server" Text="ترتیب:"></asp:Label>
            </td>
            <td width="30%">
                <telerik:RadNumericTextBox ID="TxtPriority" runat="server" Skin="Office2007"
                    Width="50px">
                    <NumberFormat DecimalDigits="0" ZeroPattern="n" />
                </telerik:RadNumericTextBox>
            </td>
            <td width="10%">
                <asp:RequiredFieldValidator ID="rfvName0" runat="server"
                    ControlToValidate="TxtName" ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td width="10%">&nbsp;</td>
            <td width="30%">&nbsp;</td>
            <td width="10%">&nbsp;</td>
        </tr>
        <tr>
            <td width="10%" nowrap="nowrap" valign="top">
                <asp:Label ID="LblParentCategorize" runat="server" Text="گروه کاربری دربرگیرنده:"></asp:Label>
            </td>
            <td width="30%" valign="top">
                <table>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="TxtGroupName" runat="server" LabelWidth="75px" Width="120px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgbtnSearch" runat="server" CausesValidation="False" Height="25px"
                                ImageUrl="../Images/SearchOrganizationPhysicalChart.png" OnClick="imgbtnSearch_Click"
                                ToolTip="جستجو" Width="25px" />
                        </td>
                        <td>
                            <img alt="جلو" height="25" onclick="NextNode();" src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Next.png"%>'
                                width="25" />
                        </td>
                        <td>
                            <img alt="عقب" height="25" onclick="PreviousNode();" src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Previous.png"%>'
                                width="25" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <telerik:RadTreeView ID="trvGroup" runat="server" BorderColor="#999999" Skin="Office2007"
                                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" dir="rtl" OnClientContextMenuItemClicking="ContextMenuClick"
                                OnContextMenuItemClick="trvGroup_ContextMenuItemClick" OnNodeDataBound="trvGroup_NodeDataBound"
                                Width="99%">
                                <ContextMenus>
                                    <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server" Skin="Office2007">
                                        <Items>
                                            <telerik:RadMenuItem ImageUrl="../Images/edit.png" PostBack="True" Text="ویرایش"
                                                Value="edit">
                                            </telerik:RadMenuItem>
                                            <telerik:RadMenuItem ImageUrl="../Images/delete.png" Text="حذف" Value="remove">
                                            </telerik:RadMenuItem>
                                        </Items>
                                        <CollapseAnimation Type="OutQuint" />
                                    </telerik:RadTreeViewContextMenu>
                                </ContextMenus>
                            </telerik:RadTreeView>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="10%" valign="top">&nbsp;
            </td>
            <td width="10%" style="direction: ltr">&nbsp;
            </td>
            <td width="30%">&nbsp;
            </td>
            <td width="10%">&nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" valign="top" width="10%">&nbsp;
            </td>
            <td valign="top" width="30%">&nbsp;
            </td>
            <td valign="top" width="10%">&nbsp;
            </td>
            <td width="10%">&nbsp;
            </td>
            <td width="30%">&nbsp;
            </td>
            <td width="10%">&nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        function ContextMenuClick(sender, eventArgs) {
            var node = eventArgs.get_node();
            var item = eventArgs.get_menuItem();
            if (item.get_value() == "remove") {
                eventArgs.set_cancel(!confirm('آیا مطمئن هستید؟'));
            }
        }  
    </script>
</telerik:RadScriptBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="BtnAdd">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="trvGroup">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll"
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadCodeBlock2">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" />
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Office2007">
</telerik:RadAjaxLoadingPanel>
