﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BaseInformation
{

    //-----93-04-18
    //----- majid Gholibeygian
    public partial class GroupPage : ItcBaseControl
    {
        private ArrayList _searchGroupIdList = new ArrayList();
        private GroupBL _groupBL=new GroupBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";               
                SetTrvGroup("");
                BtnEdit.Visible = false;
                Session["ItemSearch"] = "";
                ViewState["PageStatus"] = "IsPostBack";
            }
        }
     
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {                                   
                Guid? ownerId;
                if (trvGroup.SelectedNode != null)
                    ownerId = new Guid(trvGroup.SelectedValue);
                else
                {
                    ownerId = (Guid?)null;
                }
                var groupEntity=new GroupEntity {GroupTitle = TxtName.Text, ChildNo = Int32.Parse(TxtPriority.Text), OwnerId = ownerId};
                Guid groupId;
                 _groupBL.Add(groupEntity,out groupId);
               
                SetTrvGroup("");
                groupEntity.GroupId = groupId;
                var myGroup = _groupBL.GetSingleById(groupEntity);
                SetSelectedNodeForTree(myGroup.GroupId);
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {               
                var groupId = Guid.Parse(ViewState["GroupId"].ToString());
                Guid? ownerId;
                if (trvGroup.SelectedNode != null)
                    ownerId = new Guid(trvGroup.SelectedValue);
                else
                {
                    ownerId = (Guid?)null;
                }
                var groupEntity = new GroupEntity {GroupTitle = TxtName.Text, ChildNo = Int32.Parse(TxtPriority.Text), OwnerId = ownerId, GroupId = groupId};
                _groupBL.Update(groupEntity);
                SetTrvGroup("");
                SetPageControlForAdd();
                SetSelectedNodeForTree(groupId);
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }       

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            SetPageControl();
            SetTrvGroup("");
        }

        protected void SetTrvGroup(string groupTitle)
        {
            trvGroup.DataTextField = "GroupTitle";
            trvGroup.DataValueField = "GroupId";
            trvGroup.DataFieldID = "GroupId";
            trvGroup.DataFieldParentID = "OwnerId";
            trvGroup.DataSource = _groupBL.GetAllNodes(groupTitle);
            trvGroup.DataBind();
            if (trvGroup.Nodes.Count > 0)
            {
                trvGroup.Nodes[0].Expanded = true;
            }
        }

        protected void SetPageControlForEdit()
        {
            BtnAdd.Visible = false;
            BtnEdit.Visible = true;
        }

        protected void SetPageControlForAdd()
        {
            BtnAdd.Visible = true;
            BtnEdit.Visible = false;
        }

        protected void SetPageControl()
        {          
            TxtName.Text = "";
            TxtPriority.Text = string.Empty;
            trvGroup.ClearSelectedNodes();
        }

        protected void trvGroup_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            try
            {
                RadTreeNode clickedNode = e.Node;
                if (e.MenuItem.Value.ToLower() == "edit")
                {
                    var groupId = new Guid(clickedNode.Value);
                    var groupEntity=new GroupEntity {GroupId = groupId};
                    var myGroup = _groupBL.GetSingleById(groupEntity);                  
                    TxtName.Text = myGroup.GroupTitle;
                    TxtPriority.Text = myGroup.ChildNo.ToString();
                    if (e.Node.ParentNode != null)
                    {
                        Guid ownerId = Guid.Parse(clickedNode.ParentNode.Value);
                        SetSelectedNodeForTree(ownerId);
                        clickedNode.ParentNode.Selected = true;
                    }
                    else
                    {
                        e.Node.Selected = false;
                    }

                    ViewState["GroupId"] = groupId;
                    SetPageControlForEdit();
                }
                if (e.MenuItem.Value.ToLower() == "remove")
                {
                    var groupId = Guid.Parse(clickedNode.Value);
                 
                    _groupBL.DeleteWithAllChild(groupId);
                    SetTrvGroup("");
                    if (clickedNode.ParentNode != null)
                    {
                                         var ownerId = clickedNode.ParentNode.Value;
                    trvGroup.FindNodeByValue(ownerId).ExpandParentNodes();
                    trvGroup.FindNodeByValue(ownerId).Expanded = true;  
                    }
 

                    MessageErrorControl1.ShowSuccesMessage("حذف موفق");
                }
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void SetSelectedNodeForTree(Guid groupId)
        {
            var parrentArr = new ArrayList();
            var parentDt = _groupBL.GetParentNodes(groupId);
            foreach (DataRow parent in parentDt.Rows)
            {
                parrentArr.Add(parent["GroupId"].ToString());
            }
            foreach (var radTreeNode in trvGroup.GetAllNodes())
            {
                if (parrentArr.Contains(radTreeNode.Value))
                {
                    radTreeNode.Expanded = true;
                }
                if (radTreeNode.Value == groupId.ToString())
                {
                    radTreeNode.Selected = true;
                    return;
                }
            }
        }
      
        protected void imgbtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (TxtGroupName.Text != "")
            {
                var searchNodes = _groupBL.GetAllNodes(TxtGroupName.Text);

                _searchGroupIdList = new ArrayList();

                foreach (DataRow datarow in searchNodes.Rows)
                {
                    _searchGroupIdList.Add(Guid.Parse(datarow["GroupId"].ToString()));

                    Session["ItemSearch"] += (string.IsNullOrEmpty(Session["ItemSearch"].ToString())) ? "'" + datarow["GroupId"] + "'" : "," + "'" + datarow["GroupId"] + "'";
                }
                SetTrvGroup("");
            }
        }

        protected void trvGroup_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            if (_searchGroupIdList.Contains((Guid.Parse(e.Node.Value))))
            {
                e.Node.ExpandParentNodes();
                e.Node.Text = e.Node.Text.Replace(ITC.Library.Classes.FarsiToArabic.ToArabic(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", ITC.Library.Classes.FarsiToArabic.ToArabic(TxtGroupName.Text)));
                // node.Selected = true;
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            if (TxtGroupName.Text != "")
            {
                var searchNodes = _groupBL.GetAllNodes(TxtGroupName.Text);

                _searchGroupIdList = new ArrayList();

                foreach (DataRow datarow in searchNodes.Rows)
                {
                    _searchGroupIdList.Add(Int32.Parse(datarow["RoleAndCategorizeId"].ToString()));

                    Session["ItemSearch"] += (string.IsNullOrEmpty(Session["ItemSearch"].ToString())) ? "'" + datarow["RoleAndCategorizeId"].ToString() + "'" : "," + "'" + datarow["RoleAndCategorizeId"].ToString() + "'";
                }
                SetTrvGroup("");
            }
        }
       
    }
}