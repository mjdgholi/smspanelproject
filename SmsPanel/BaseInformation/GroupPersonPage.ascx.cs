﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BaseInformation
{
    public partial class GroupPersonPage : ItcBaseControl
    {
        /// <summary>
        // Author:		<majid Gholibeygian>
        // Edit <Narges Kamran>
        // Create date: <1393/04/18>
        // Edit Date : <1393/04/21>
        // Description:	<فرم گروه اشخاص>
        /// </summary>
        #region PublicParam:
        private readonly GroupPersonBL _GroupPersonBL = new GroupPersonBL();
        private  readonly  GroupBL _groupBL=new GroupBL();
        private const string TableName = "SmsPanel.V_GroupPerson";
        private const string PrimaryKey = "GroupPersonId";

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetTrvGroup();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }


        }

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            radTreeGroup.UnselectAllNodes();
            SelectControlPersonMoblie.KeyId = "";
            SelectControlPersonMoblie.Title = "";
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var GroupPersonEntity = new GroupPersonEntity();
            {
                GroupPersonEntity.GroupPersonId = new Guid(ViewState["GroupPersonId"].ToString());
            }
            var myGroupPersonBL = _GroupPersonBL.GetSingleById(GroupPersonEntity);
            SetSelectedNodeForTree(myGroupPersonBL.GroupId);            
            SelectControlPersonMoblie.KeyId = myGroupPersonBL.PersonMoblieId.ToString();
            SelectControlPersonMoblie.Title = myGroupPersonBL.FullName;
            SelectControlPersonMoblie.WhereClause = myGroupPersonBL.PersonMoblieId+"|"+myGroupPersonBL.FullName;
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdGroupPerson,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        protected void SetTrvGroup()
        {
            radTreeGroup.DataTextField = "GroupTitle";
            radTreeGroup.DataValueField = "GroupId";
            radTreeGroup.DataFieldID = "GroupId";
            radTreeGroup.DataFieldParentID = "OwnerId";
            radTreeGroup.DataSource = _groupBL.GetAllNodes("");
            radTreeGroup.DataBind();
       
        }

        private void SetSelectedNodeForTree(Guid? groupId)
        {
            SetTrvGroup();
            if (groupId != null)
            {
                foreach (var radTreeNode in radTreeGroup.GetAllNodes())
                {
                    if (Guid.Parse(radTreeNode.Value) == groupId)
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Selected = true;

                    }
                }
            }
        }

        private void Checkvalidation()
        {
            if (radTreeGroup.SelectedValue == "")
                throw (new ItcApplicationErrorManagerException("لطفا از درخت گروه یک نود را انتخاب نمایید."));
            if (SelectControlPersonMoblie.KeyId == "")
                throw (new ItcApplicationErrorManagerException("اشخاص را انتخاب نمایید."));
        }
        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetTrvGroup();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Checkvalidation();
                ViewState["WhereClause"] = "";
                SetPanelFirst();

                var groupPersonEntity = new GroupPersonEntity
                {
                    GroupId = Guid.Parse(radTreeGroup.SelectedValue),
                    PersonMoblieIdStr= SelectControlPersonMoblie.KeyId
                };

                _GroupPersonBL.Add(groupPersonEntity);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0,new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                //SelectControlPersonMoblie.WhereClause = "ADDMode";
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
            finally
            {
                var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
        }

        

        /// <summary>
        /// جستجو بر اساس مناسبت فردیهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (radTreeGroup.SelectedValue != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and GroupId= '" +
                                               Guid.Parse(radTreeGroup.SelectedNode.Value.Trim()) + "'";
                if (SelectControlPersonMoblie.KeyId != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And PersonMoblieId='" +
                                               Guid.Parse(SelectControlPersonMoblie.KeyId.Trim()) + "'";
                grdGroupPerson.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                //SelectControlPersonMoblie.WhereClause = "ADDMode";
                var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdGroupPerson.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                //SelectControlPersonMoblie.WhereClause = "ADDMode";
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Checkvalidation();
                 var groupPersonEntity = new GroupPersonEntity
                {
                    GroupPersonId = new Guid(ViewState["GroupPersonId"].ToString()),
                    GroupId = Guid.Parse(radTreeGroup.SelectedNode.Value),
                    PersonMoblieId = Guid.Parse(SelectControlPersonMoblie.KeyId)
                };
                 _GroupPersonBL.Update(groupPersonEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0, new Guid(ViewState["GroupPersonId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                //SelectControlPersonMoblie.WhereClause = "ADDMode";
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
                //SelectControlPersonMoblie.WhereClause = "ADDMode";
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdGroupPerson_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["GroupPersonId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    //SelectControlPersonMoblie.WhereClause = "EditMode";
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var GroupPersonEntity = new GroupPersonEntity();
                    {
                        GroupPersonEntity.GroupPersonId = new Guid(e.CommandArgument.ToString());
                    }
                    _GroupPersonBL.Delete(GroupPersonEntity);
                    var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    //SelectControlPersonMoblie.WhereClause = "ADDMode";
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdGroupPerson_PageGroupPersonChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdGroupPerson_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdGroupPerson_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdGroupPerson.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}