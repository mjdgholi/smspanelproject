﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestPage.ascx.cs" Inherits="Intranet.DesktopModules.SmsPanelProject.SmsPanel.BaseInformation.TestPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>

<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; " class="MainTableOfASCX"dir="rtl">
        <tr>
            <td>
                <cc1:SelectControl ID="SelectControlPerson" runat="server" PortalPathUrl="IsargaranProject/Isargaran/ModalForm/PersonMultiSelectPage.aspx" />
            </td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Button" /></td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="Panel1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="Panel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
