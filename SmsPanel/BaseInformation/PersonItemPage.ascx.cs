﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <مناسبت فردی فرد>
    /// </summary>
    public partial class PersonItemPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly ItemCategoryBL _itemCategoryBL = new ItemCategoryBL();
        private readonly ItemBL _itemBL = new ItemBL();
        private  readonly  PersonItemBL _personItemBL=new PersonItemBL();
        private const string TableName = "SMSPanel.V_PersonItem";
        private const string PrimaryKey = "PersonItemId";

        #endregion


        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDate.Text = "";            
            radtreeItemCategory.UnselectAllNodes();
            cmbIsActive.SelectedIndex = 1;
            cmbItemTitle.ClearSelection();
            SelectControlPerson.KeyId = "";
            SelectControlPerson.Title = "";
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var personItemEntity = new PersonItemEntity()
            {
                PersonItemId = new Guid(ViewState["PersonItemId"].ToString())
            };
            var mypersonitem = _personItemBL.GetSingleById(personItemEntity);
            txtDate.Text = mypersonitem.Date;
            
            cmbIsActive.SelectedValue = mypersonitem.IsActive.ToString();
            SelectControlPerson.KeyId = mypersonitem.PersonId.ToString();
            SelectControlPerson.Title = mypersonitem.FullName;
            SetSelectedNodeForTree((mypersonitem.ItemCategoryId == null) ? (Guid?)null : Guid.Parse(mypersonitem.ItemCategoryId.ToString()));
            SetCmbItem(Guid.Parse(mypersonitem.ItemCategoryId.ToString()));
            cmbItemTitle.SelectedValue = mypersonitem.ItemId.ToString();
        }


        protected void SetSelectedNodeForTree(Guid? treeId)
        {
            SetTrvGroup("");
            if (treeId != null)
            {
                foreach (var radTreeNode in radtreeItemCategory.GetAllNodes())
                {
                    if (Guid.Parse(radTreeNode.Value) == treeId)
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Selected = true;

                    }
                }
            }
        }

        protected void SetTrvGroup(string groupTitle)
        {
            radtreeItemCategory.DataTextField = "ItemGroupTitle";
            radtreeItemCategory.DataValueField = "ItemCategoryId";
            radtreeItemCategory.DataFieldID = "ItemCategoryId";
            radtreeItemCategory.DataFieldParentID = "OwnerId";
            ItemCategoryEntity itemCategoryEntity = new ItemCategoryEntity()
            {
                ItemGroupTitle = groupTitle
            }
                ;
            radtreeItemCategory.DataSource = _itemCategoryBL.GetAllNodes(itemCategoryEntity);
            radtreeItemCategory.DataBind();
            if (radtreeItemCategory.Nodes.Count > 0)
            {
                radtreeItemCategory.Nodes[0].Expanded = true;
            }
        }
        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdPersonItem,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        private void SetCmbItem(Guid itemCategoryId)
        {
            cmbItemTitle.Items.Clear();
            cmbItemTitle.Items.Add(new RadComboBoxItem(""));
            ItemEntity itemEntity=new ItemEntity()
            {
                ItemCategoryId = itemCategoryId
            };
            cmbItemTitle.DataSource = _itemBL.GetItemCollectionByItemCategory(itemEntity);
            cmbItemTitle.DataTextField = "ItemTitle";
            cmbItemTitle.DataValueField = "ItemId";
            cmbItemTitle.DataBind();
        }
        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetTrvGroup("");
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetTrvGroup("");
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid personItemId;
                ViewState["WhereClause"] = "";

                PersonItemEntity personItemEntity=new PersonItemEntity()
                {
                    PersonId = Guid.Parse(SelectControlPerson.KeyId),
                    ItemId = Guid.Parse(cmbItemTitle.SelectedValue),
                    Date = ITC.Library.Classes.ItcToDate.ShamsiToMiladi(txtDate.Text),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),                                        
                };

                _personItemBL.Add(personItemEntity, out personItemId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, 0, personItemId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس مناسبت فردیهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and [Date] ='" +
                                               txtDate.Text + "'";
                if(cmbItemTitle.SelectedIndex>0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ItemId ='" +
                                               (cmbItemTitle.SelectedValue) + "'";
                if (SelectControlPerson.KeyId.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonId ='" +
                                               SelectControlPerson.KeyId+ "'";
                if (!String.IsNullOrEmpty(radtreeItemCategory.SelectedValue))
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ItemCategoryId ='" +
                            radtreeItemCategory.SelectedNode.Value + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdPersonItem.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdPersonItem.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                PersonItemEntity personItemEntity = new PersonItemEntity()
                {
                    PersonItemId = Guid.Parse(ViewState["PersonItemId"].ToString()),
                    PersonId = Guid.Parse(SelectControlPerson.KeyId),
                    ItemId = Guid.Parse(cmbItemTitle.SelectedValue),
                    Date = ITC.Library.Classes.ItcToDate.ShamsiToMiladi(txtDate.Text),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };
                _personItemBL.Update(personItemEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, 0, new Guid(ViewState["PersonItemId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void radtreeItemCategory_NodeClick(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {
            try
            {

                SetCmbItem(Guid.Parse(e.Node.Value));
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdPersonItem_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyEdit")
                {
                    ViewState["PersonItemId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyDelete")
                {
                    ViewState["WhereClause"] = "";

                    var personItemEntity = new PersonItemEntity()
                    {
                        PersonItemId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _personItemBL.Delete(personItemEntity);
                    var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdPersonItem_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdPersonItem_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdPersonItem_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdPersonItem.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion

        


   
    }
}