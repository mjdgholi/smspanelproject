﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EducationSystem.Admin.Classes;
using Intranet.Configuration.Settings;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel
{
    public partial class BasicMenuInImagePage : ItcBaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "سامانه پیامکی";
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/FrmLogin2.aspx");
            }
            if (!IsPostBack)
            {
                ArrangeMenu();
            }
        }



        protected void ArrangeMenu()
        {
            var userId = -1;
            if (!string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
            {
                userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            }

            var tblGeneral = new Table
            {
                Width = new Unit(100, UnitType.Percentage),
                BorderWidth = new Unit(3),
                CellPadding = 7,
                CellSpacing = 5,
                HorizontalAlign = HorizontalAlign.Center,
                CssClass = "MainTableOfASCX"
            };
            PlaceHolder1.Controls.Add(tblGeneral);
            int count;
            string moduleToolTip;
            var moduleArrangementDt = Modules.GetModuleMenuWithImage(int.Parse(Request["OwnerId"].ToString()),int.Parse(HttpContext.Current.User.Identity.Name)).Tables[0];

            var searchedModuleArr = new ArrayList();



            DataTable settingDt = Modules.GetModuleSetting();
            if (!string.IsNullOrEmpty(settingDt.Rows[0]["BackGroundColor"].ToString()))
            {
                var backColor = settingDt.Rows[0]["BackGroundColor"].ToString();
                tblGeneral.BackColor = System.Drawing.ColorTranslator.FromHtml(backColor);
            }
            if (!string.IsNullOrEmpty(settingDt.Rows[0]["BackGroundImagePath"].ToString()))
            {
                var backGroundImagePath = settingDt.Rows[0]["BackGroundImagePath"].ToString();

                tblGeneral.BackImageUrl = "Images/" + backGroundImagePath;
            }
            var isCellMouseOverActive = bool.Parse(settingDt.Rows[0]["IsCell_MouseOver_Active"].ToString());
            var mouseOverColor = settingDt.Rows[0]["MouseOverColor"].ToString();
            var mouseOutColor = settingDt.Rows[0]["MouseOutColor"].ToString();
            var rowNo = Int32.Parse(settingDt.Rows[0]["RowNumber"].ToString());
            var colNo = Int32.Parse(settingDt.Rows[0]["ColumnNumber"].ToString());
            var isPrevModuleExternal = true;
            for (var i = 0; i < rowNo; i++)
            {
                var tableRow = new TableRow();
                tableRow.HorizontalAlign = HorizontalAlign.Center;
                for (var j = 0; j < colNo; j++)
                {
                    if (moduleArrangementDt.Rows.Count > 0)
                    {
                        if (isPrevModuleExternal && !string.IsNullOrEmpty(moduleArrangementDt.Rows[0]["isinternal"].ToString()) && bool.Parse(moduleArrangementDt.Rows[0]["isinternal"].ToString())) // برای جداسازی ماژولهای داخلی از خارجی
                        {
                            isPrevModuleExternal = false;
                            break;
                        }
                        var mainPicPath = moduleArrangementDt.Rows[0]["MainPicPath"].ToString();
                        var mouseOverPicPath = moduleArrangementDt.Rows[0]["MouseOverPicPath"].ToString();
                        var portalUrl = moduleArrangementDt.Rows[0]["PortalUrl"].ToString();
                        moduleToolTip = moduleArrangementDt.Rows[0]["ModuleToolTip"].ToString();
                        var hyperLink = new HyperLink();
                        portalUrl = Intranet.Common.IntranetUI.BuildUrl(@portalUrl);
                        hyperLink.NavigateUrl = portalUrl;
                        hyperLink.Target = "_blank";
                        hyperLink.ToolTip = moduleToolTip;
                        hyperLink.ImageUrl = PortalSettings.PortalPath + "/DeskTopModules/SmsPanelProject/SmsPanel/Images/" + mainPicPath;

                        var mouseOverImage = PortalSettings.PortalPath + "/DeskTopModules/SmsPanelProject/SmsPanel/Images/" + mouseOverPicPath;
                        var mouseOutImage = PortalSettings.PortalPath + "/DeskTopModules/SmsPanelProject/SmsPanel/Images/" + mainPicPath;

                        hyperLink.Attributes.Add("onmouseover", "this.firstChild.src = '" + mouseOverImage + "';");
                        hyperLink.Attributes.Add("onmouseout", "this.firstChild.src = '" + mouseOutImage + "';");

                        var tableCell = new TableCell { };
                        if (isCellMouseOverActive)
                        {
                            tableCell.Attributes.Add("onmouseover",
                                                     "this.style.backgroundColor = '" + mouseOverColor + "';");
                            tableCell.Attributes.Add("onmouseout",
                                                     "this.style.backgroundColor = '" + mouseOutColor + "';");
                        }
                        if (searchedModuleArr.Contains(moduleArrangementDt.Rows[0]["ModuleArrangementId"].ToString()))
                        {
                            tableCell.BackColor = Color.DarkOrange;
                        }

                        tableCell.VerticalAlign = VerticalAlign.Top;
                        tableCell.Controls.Add(hyperLink);
                        tableCell.BorderWidth = 0;
                        tableCell.BorderColor = Color.DarkBlue;
                        moduleArrangementDt.Rows.RemoveAt(0);
                        tableRow.Controls.Add(tableCell);

                    }
                }
                tblGeneral.Controls.Add(tableRow);
            }
        }
    }
}