﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GroupSmsMessagePage.aspx.cs" Inherits="Intranet.DesktopModules.SmsPanelProject.SmsPanel.ModalForm.GroupSmsMessagePage" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table dir="rtl" width="100%" class="MainTableOfASCX">
        <tr>
            <td align="center">
              <asp:Label runat="server" Text="نتایج انجام عملیات ساخت بسته های پیامکی" 
                    Font-Bold="True" ForeColor="#CC0000"></asp:Label> 
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadGrid ID="radgridSms" runat="server">
                </telerik:RadGrid>

            <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            </telerik:RadScriptManager>
            </td>
        </tr>
    </table>
    </div>
    </form>
    
</body>
</html>
