﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.ModalForm
{
    public partial class PersonMobilePage : PortalPage
    {
        #region PublicParam:
        private readonly PersonMobileBL _personMobileBL = new PersonMobileBL();
        private const string TableName = "SMSPanel.V_PersonMobile";
        private const string PrimaryKey = "PersonMoblieId";

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!String.IsNullOrEmpty(Request["WhereClause"]))
                    {
                        string[] arr = Request["WhereClause"].Split('|');
                        hiddenfieldKeyId.Value = arr[0].ToLower();
                        hiddenfieldTitle.Value = arr[1];
                    }

                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdPersonMobile.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);                    
                    SetClearToolBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

        }

        #region Procedure:
        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdPersonMobile,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            SelectControlPerson.KeyId = "";
            SelectControlPerson.Title = "";
            txtMobileNo.Text = "";
            cmbIsActive.SelectedIndex = 1;
            cmbIsDefault.ClearSelection();

        }
        #endregion
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";

                if (txtMobileNo.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MobileNo Like N'%" +
                                               txtMobileNo.Text + "%'";
                if (SelectControlPerson.KeyId != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonId ='" +
                                               Guid.Parse(SelectControlPerson.KeyId) + "'";
                if (cmbIsDefault.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsDefault='" +
                                               (cmbIsDefault.SelectedItem.Value.Trim()) + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdPersonMobile.MasterTableView.CurrentPageIndex = 0;                
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdPersonMobile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdPersonMobile.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {

        }

        protected void grdPersonMobile_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {

        }

        protected void grdPersonMobile_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {

        }

        protected void grdPersonMobile_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {

        }
    }
}