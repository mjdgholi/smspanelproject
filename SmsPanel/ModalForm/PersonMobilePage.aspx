﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PersonMobilePage.aspx.cs" Inherits="Intranet.DesktopModules.SmsPanelProject.SmsPanel.ModalForm.PersonMobilePage" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        var myArrayKey = new Array();
        var myArrayTitle = new Array();
        var hiddenfieldvalue = '';
        var hiddenfieldtitle = '';
        var str = '';
        function RowSelected(sender, eventArgs) {

            KeyNamevalue = eventArgs._tableView.get_clientDataKeyNames()[0];
            KeyNameTitle = eventArgs._tableView.get_clientDataKeyNames()[1];

            hiddenfieldvalue = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
            hiddenfieldtitle = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value;
            var textKeyvalue = eventArgs.getDataKeyValue(KeyNamevalue);
            var txtTitlevalue = (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1])).trim();
            if (include(myArrayKey, textKeyvalue) != true) {
                if (myArrayKey.length > 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + textKeyvalue;
                hiddenfieldtitle = hiddenfieldtitle + str + (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1])).trim();
                myArrayKey.push(textKeyvalue);
                myArrayTitle.push(txtTitlevalue);
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
            document.getElementById('<%=hiddenfieldTitle.ClientID %>').value = hiddenfieldtitle;
//            alert(document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value);
//            alert(document.getElementById('<%=hiddenfieldTitle.ClientID %>').value);
        }

        function RowDeselected(sender, eventArgs) {

            str = '';
            hiddenfieldvalue = '';
            hiddenfieldtitle = '';
            KeyName = eventArgs._tableView.get_clientDataKeyNames()[0];
            KeyNameTitle = eventArgs._tableView.get_clientDataKeyNames()[1];
            for (var i = 0; i < myArrayKey.length; i++) {
                if (myArrayKey[i] == eventArgs.getDataKeyValue(KeyName))
                    myArrayKey.splice(i, 1);
            }
            for (var j = 0; j < myArrayTitle.length; j++) {
                if (myArrayTitle[j] == (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1])).trim())
                    myArrayTitle.splice(j, 1);
            }

            for (var i = 0; i < myArrayKey.length; i++) {
                if (i != 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + myArrayKey[i];
            }
            str = '';
            for (var j = 0; j < myArrayTitle.length; j++) {
                if (j != 0)
                    str = ',';
                hiddenfieldtitle = hiddenfieldtitle + str + myArrayTitle[j];
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
            document.getElementById('<%=hiddenfieldTitle.ClientID %>').value = hiddenfieldtitle;
        }

        function MasterTableViewCreated(sender, eventArgs) {
//            alert(document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value);
//            alert(document.getElementById('<%=hiddenfieldTitle.ClientID %>').value);
            if (document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value != '')
                myArrayKey = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value.split(',');
            if (document.getElementById('<%=hiddenfieldTitle.ClientID %>').value != '')
                myArrayTitle = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value.split(',');
            KeyName = sender.MasterTableView.get_clientDataKeyNames()[0];
            for (var j = 0; j < sender.get_masterTableView().get_dataItems().length; j++)
                for (var i = 0; i < myArrayKey.length; i++) {
                    if (myArrayKey[i] == sender.get_masterTableView().get_dataItems()[j].getDataKeyValue(KeyName))
                        sender.get_masterTableView().get_dataItems()[j].set_selected(true);
                }
        }

        function include(arr, obj) {
            if (arr.length == 0)
                return false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == obj)
                    return true;
            }
        }

        function CloseRadWindow() {

            var oArg = new Object();
            oArg.Title = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value;
            oArg.KeyId = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
            var oWnd = GetRadWindow();
            oWnd.close(oArg);
        }


        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</telerik:RadScriptBlock>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; " class="MainTableOfASCX" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back"  onclientclicked="CloseRadWindow">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Panel ID="pnlDetail" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td nowrap="nowrap" width="10%">
                                <asp:Label runat="server" ID="lblMobileNo">شماره موبایل<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="90%">
                                <telerik:RadTextBox ID="txtMobileNo" runat="server">
                                </telerik:RadTextBox>

                                <asp:RequiredFieldValidator ID="rfvMobileNo" runat="server" ControlToValidate="txtMobileNo"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblFullName">شخص<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                              
                                
                               <cc1:SelectControl ID="SelectControlPerson" runat="server" PortalPathUrl="GeneralProject/General/ModalForm/PersonPage.aspx" imageName="SelectPerson" />           
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <label>
                                    وضیعت پیش فرض شماره موبایل<font color="red">*</font>:</label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbIsDefault" runat="server">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="بله" Value="True" />
                                        <telerik:RadComboBoxItem Text="خیر" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                                <asp:RequiredFieldValidator ID="rfvIsDefault" runat="server" ControlToValidate="cmbIsDefault"
                                    ErrorMessage="وضیعت پیش فرض شماره موبایل" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    وضیعت رکورد<font color="red">*</font>:</label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbIsActive" runat="server">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                                <asp:RequiredFieldValidator ID="rfvalidatorVisibility" runat="server" ControlToValidate="cmbIsActive"
                                    ErrorMessage="وضیعت رکورد" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdPersonMobile" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                    AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" 
                                    onpageindexchanged="grdPersonMobile_PageIndexChanged" 
                                    onpagesizechanged="grdPersonMobile_PageSizeChanged" 
                                    onsortcommand="grdPersonMobile_SortCommand" AllowMultiRowSelection="True">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="PersonMoblieId" Dir="RTL" GroupsDefaultExpanded="False" ClientDataKeyNames="PersonMoblieId,FullName"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="FullName" HeaderText="نام و نام خانوادگی" SortExpression="FullName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MobileNo" HeaderText="شماره موبایل"
                                                SortExpression="MobileNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                              <telerik:GridCheckBoxColumn DataField="IsDefault" HeaderText="شماره موبایل پیش فرض" SortExpression="IsDefault">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                            <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                       <telerik:GridClientSelectColumn UniqueName="Select">
                        </telerik:GridClientSelectColumn>

                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, مناسبت فردی &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                         <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents OnRowSelected="RowSelected" OnRowDeselected="RowDeselected" OnMasterTableViewCreated="MasterTableViewCreated">
                                        </ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadScriptManager ID="RadScriptManager1" runat="server">
</telerik:RadScriptManager>
<asp:HiddenField ID="hiddenfieldKeyId" runat="server" />
<asp:HiddenField ID="hiddenfieldTitle" runat="server" />
    </div>
    </form>
</body>
</html>



