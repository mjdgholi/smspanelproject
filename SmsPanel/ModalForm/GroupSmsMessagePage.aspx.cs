﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.ModalForm
{
    public partial class GroupSmsMessagePage : PortalPage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                radgridSms.DataSource = Session["GroupSmsDS"];
                radgridSms.DataBind();    
            }
            
        }
    }
}