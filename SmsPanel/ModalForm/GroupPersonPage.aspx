﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GroupPersonPage.aspx.cs" Inherits="Intranet.DesktopModules.SmsPanelProject.SmsPanel.ModalForm.GroupPersonPage" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <table class="MainTableOfASCX">
            <tr>
                <td>
                       <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                                                                 <telerik:RadGrid ID="grdGroupPerson" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                                AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                                AutoGenerateColumns="False"  OnPageIndexChanged="grdGroupPerson_PageGroupPersonChanged"
                                                OnPageSizeChanged="grdGroupPerson_PageSizeChanged" OnSortCommand="grdGroupPerson_SortCommand">
                                                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                                </HeaderContextMenu>
                                                <MasterTableView DataKeyNames="GroupPersonId" Dir="RTL" GroupsDefaultExpanded="False"
                                                    NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="GroupTitle" HeaderText="عنوان گروه" SortExpression="GroupTitle">
                                                            <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                        </telerik:GridBoundColumn>
                                                                 <telerik:GridBoundColumn DataField="FullName" HeaderText="اشخاص" SortExpression="FullName">
                                                            <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                        </telerik:GridBoundColumn>
                                               <telerik:GridBoundColumn DataField="MobileNo" HeaderText="شماره موبایل"
                                                SortExpression="MobileNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                        
                                                    </Columns>
                                                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                                    </RowIndicatorColumn>
                                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                                    </ExpandCollapseColumn>
                                                    <EditFormSettings>
                                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                                        </EditColumn>
                                                    </EditFormSettings>
                                                    <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                                        NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, مناسبت فردی &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                                        PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                                        VerticalAlign="Middle" />
                                                </MasterTableView>
                                                <FilterMenu EnableImageSprites="False">
                                                </FilterMenu>
                                            </telerik:RadGrid> 
                </td>
            </tr>
        </table>

        
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
    </div>
    </form>
</body>
</html>
