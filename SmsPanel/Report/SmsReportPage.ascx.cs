﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.Report
{
    public partial class SmsReportPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly OccasionDayTypeBL _occasionDayTypeBL = new OccasionDayTypeBL();
        private readonly OccasionDayBL _occasionDayBL = new OccasionDayBL();
        private  readonly  MessageDraftBL messageDraftBL=new MessageDraftBL();
        private readonly MessageStatusBL _messageStatusBL = new MessageStatusBL();
        private  readonly  Occasion_MessageDraftBL _occasionMessageDraftBL=new Occasion_MessageDraftBL();
        private readonly SmsServiceProviderBL _smsServiceProviderBL = new SmsServiceProviderBL();
        private const string TableName = "SMSPanel.V_SmsReport";
        private const string PrimaryKey = "SmsId";

        #endregion
        #region Procedure:
        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtMessageText.Text = "";
            cmbOccasionType.ClearSelection();
            cmbOccasionTitle.ClearSelection();
            cmbMessageDraft.ClearSelection();            
            txtSendDate.Text = "";
            txtSendTime.Text = "";            
            SelectControlPerson.KeyId = "";
            SelectControlPerson.Title = "";
            txtMobileNo.Text = "";
            cmbMessageStatus.ClearSelection();

        }

        void SetComboBox()
        {
            cmbOccasionType.Items.Clear();
            cmbOccasionType.Items.Add(new RadComboBoxItem(""));
            cmbOccasionType.DataSource = _occasionDayTypeBL.GetAllIsActive();
            cmbOccasionType.DataTextField = "OccasionDayTypeTitle";
            cmbOccasionType.DataValueField = "OccasionDayTypeId";
            cmbOccasionType.DataBind();

            cmbOccasionTitle.Items.Clear();
            cmbOccasionTitle.Items.Add(new RadComboBoxItem(""));
            cmbOccasionTitle.DataSource = _occasionDayBL.GetAll();
            cmbOccasionTitle.DataTextField = "OccasionDayTitle";
            cmbOccasionTitle.DataValueField = "OccasionDayId";
            cmbOccasionTitle.DataBind();

            cmbMessageDraft.Items.Clear();
            cmbMessageDraft.Items.Add(new RadComboBoxItem(""));
            cmbMessageDraft.DataSource = messageDraftBL.GetAllIsActive();
            cmbMessageDraft.DataTextField = "MessageDraftText";
            cmbMessageDraft.DataValueField = "MessageDraftId";
            cmbMessageDraft.DataBind();


            cmbMessageStatus.Items.Clear();
            cmbMessageStatus.Items.Add(new RadComboBoxItem(""));
            cmbMessageStatus.DataSource = _messageStatusBL.GetAllIsActive();
            cmbMessageStatus.DataTextField = "MessageStatusPersianTitle";
            cmbMessageStatus.DataValueField = "MessageStatusId";
            cmbMessageStatus.DataBind();



            cmbSmsServiceProvider.Items.Clear();
            cmbSmsServiceProvider.Items.Add(new RadComboBoxItem(""));
            cmbSmsServiceProvider.DataSource = _smsServiceProviderBL.GetAll();
            cmbSmsServiceProvider.DataTextField = "SmsServiceProviderName";
            cmbSmsServiceProvider.DataValueField = "SmsServiceProviderId";
            cmbSmsServiceProvider.DataBind();
        }

        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdSms,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }

        void SetComboBoxOccasion(int OccasionTypeId)
        {
            cmbOccasionTitle.Items.Clear();
            cmbOccasionTitle.Items.Add(new RadComboBoxItem(""));
            OccasionDayEntity occasionEntity = new OccasionDayEntity()
            {
                OccasionDayTypeId = OccasionTypeId
            };
            cmbOccasionTitle.DataSource = _occasionDayBL.GetOccasionDayCollectionByOccasionDayType(occasionEntity);
            cmbOccasionTitle.DataTextField = "OccasionDayTitle";
            cmbOccasionTitle.DataValueField = "OccasionDayId";
            cmbOccasionTitle.DataBind();
        }
        void SetComboBoxMessageDraft(int OccasionId)
        {
            cmbMessageDraft.Items.Clear();
            cmbMessageDraft.Items.Add(new RadComboBoxItem(""));
            Occasion_MessageDraftEntity occasionMessageDraftEntity = new Occasion_MessageDraftEntity()
            {
                OccasionDayId = OccasionId
            };
            cmbMessageDraft.DataSource = _occasionMessageDraftBL.GetOccasion_MessageDraftCollectionByOccasion(occasionMessageDraftEntity);
            cmbMessageDraft.DataTextField = "MessageDraftText";
            cmbMessageDraft.DataValueField = "MessageDraftId";
            cmbMessageDraft.DataBind();
        }
        private void SetMessageStatus(Guid SmsServiceProviderId)
        {

            cmbMessageStatus.Items.Clear();
            cmbMessageStatus.Items.Add(new RadComboBoxItem(""));
            MessageStatusEntity messageStatusEntity = new MessageStatusEntity()
            {
                SmsServiceProviderId = SmsServiceProviderId
            };
            cmbMessageStatus.DataSource = _messageStatusBL.GetMessageStatusDBCollectionBySmsServiceProvider(messageStatusEntity);
            cmbMessageStatus.DataTextField = "MessageStatusPersianTitle";
            cmbMessageStatus.DataValueField = "MessageStatusId";
            cmbMessageStatus.DataBind();
        }
        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = "SendDate";
                ViewState["SortType"] = "DESC";
                var gridParamEntity = SetGridParam(grdSms.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);                
                SetClearToolBox();
                SetComboBox();                

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = "SendDate desc,SendTime";
                    ViewState["SortType"] = "DESC";
                    var gridParamEntity = SetGridParam(grdSms.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetClearToolBox();
                    SetComboBox();

                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtMessageText.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageText Like N'%" +
                                               FarsiToArabic.ToArabic(txtMessageText.Text.Trim()) + "%'";
                if (cmbMessageDraft.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageDraftId ='" +
                                               Guid.Parse(cmbMessageDraft.SelectedValue) + "'";
                if (cmbOccasionTitle.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayId ='" +
                                               int.Parse(cmbOccasionTitle.SelectedValue) + "'";         
                if (txtSendDate.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SendDate = '" +
                                               txtSendDate.Text + "'";
                if (txtSendTime.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SendTime = '" +
                                               txtSendTime.TextWithLiterals + "'";
                if (cmbOccasionType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayTypeId ='" +
                                               int.Parse(cmbOccasionType.SelectedValue) + "'";
                if (cmbSmsServiceProvider.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SmsServiceProviderId ='" +
                                               Guid.Parse(cmbSmsServiceProvider.SelectedValue) + "'";

                if (SelectControlPerson.KeyId!="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonId ='" +
                                               Guid.Parse(SelectControlPerson.KeyId) + "'";
                if (txtMobileNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MobileNo Like N'%" +
                                              txtMobileNo.Text.Trim() + "%'";
                if (cmbMessageStatus.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageStatusId ='" +
                                               int.Parse(cmbMessageStatus.SelectedValue) + "'";
                grdSms.MasterTableView.CurrentPageIndex = 0;
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdSms.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdSms.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {                
                SetClearToolBox();
                SetComboBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdSms.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdSms_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdSms.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdSms_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdSms.MasterTableView.PageSize, 0,-1);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdSms_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdSms.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void cmbOccasionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value != "")
            {
                SetComboBoxOccasion(int.Parse(e.Value));
            }
            else
            {
                cmbOccasionTitle.Items.Clear();
                cmbMessageDraft.Items.Clear();
            }
        }

        protected void cmbOccasionTitle_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value != "")
            {
                SetComboBoxMessageDraft(int.Parse(e.Value));
            }
            else
            {
                cmbMessageDraft.Items.Clear();
            }
        }

        protected void btnReportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                //ViewState["WhereClause"] = "1 = 1";
                //if (txtMessageText.Text.Trim() != "")
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageText Like N'%" +
                //                               FarsiToArabic.ToArabic(txtMessageText.Text.Trim()) + "%'";
                //if (cmbMessageDraft.SelectedIndex > 0)
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageDraftId ='" +
                //                               Guid.Parse(cmbMessageDraft.SelectedValue) + "'";
                //if (cmbOccasionTitle.SelectedIndex > 0)
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionId ='" +
                //                               Guid.Parse(cmbOccasionTitle.SelectedValue) + "'";
                //if (txtSendDate.Text != "")
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SendDate = '" +
                //                               txtSendDate.Text + "'";
                //if (txtSendTime.Text != "")
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SendTime = '" +
                //                               txtSendTime.Text + "'";
                //if (cmbOccasionType.SelectedIndex > 0)
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionTypeId ='" +
                //                               Guid.Parse(cmbOccasionType.SelectedValue) + "'";
                //if (SelectControlPerson.KeyId != "")
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonId ='" +
                //                               Guid.Parse(SelectControlPerson.KeyId) + "'";
                //if (txtMobileNo.Text.Trim() != "")
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MobileNo Like N'%" +
                //                              txtMobileNo.Text.Trim() + "%'";
                //if (cmbMessageStatus.SelectedIndex > 0)
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageStatusId ='" +
                //                               int.Parse(cmbMessageStatus.SelectedValue) + "'";

  
                var oldPageSize = grdSms.MasterTableView.PageSize;
                grdSms.PageSize = 100000;
                var gridParamEntity = SetGridParam(grdSms.MasterTableView.PageSize, 0, -1);
                grdSms.DataSource = DatabaseManager.ItcGetPageDataSet(gridParamEntity).Tables[0];
                grdSms.ExportSettings.IgnorePaging = true;
                grdSms.ExportSettings.ExportOnlyData = true;
                grdSms.ExportSettings.OpenInNewWindow = true;
                grdSms.MasterTableView.ExportToExcel();
                grdSms.PageSize = oldPageSize;
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void btnOperationWithoutAjaxReportPdf_Click(object sender, EventArgs e)
        {
            try
            {
                var oldPageSize = grdSms.MasterTableView.PageSize;
                grdSms.PageSize = 100000;
                var gridParamEntity = SetGridParam(grdSms.MasterTableView.PageSize, 0, -1);
                grdSms.DataSource = DatabaseManager.ItcGetPageDataSet(gridParamEntity).Tables[0];
                grdSms.ExportSettings.IgnorePaging = true;
                grdSms.ExportSettings.ExportOnlyData = true;
                grdSms.ExportSettings.OpenInNewWindow = true;
                grdSms.MasterTableView.ExportToPdf();                
                grdSms.PageSize = oldPageSize;
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void cmbSmsServiceProvider_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Value))
            {
                SetMessageStatus(Guid.Parse(e.Value));
            }
            //else
            //{

            //    cmbMessageStatus.Items.Clear();
            //    cmbMessageStatus.Items.Add(new RadComboBoxItem(""));
            //    cmbMessageStatus.DataSource = _messageStatusBL.GetAllIsActive();
            //    cmbMessageStatus.DataTextField = "MessageStatusPersianTitle";
            //    cmbMessageStatus.DataValueField = "MessageStatusId";
            //    cmbMessageStatus.DataBind();
            //}
        }


    }
}