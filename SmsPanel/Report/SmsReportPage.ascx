﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SmsReportPage.ascx.cs"
    Inherits="Intranet.DesktopModules.SmsPanelProject.SmsPanel.Report.SmsReportPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; " class="MainTableOfASCX"dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click" style="direction: ltr">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnOperationWithoutAjaxReportExcel" runat="server" 
                                    CustomeButtonType="ExcelExport" CausesValidation="False" onclick="btnReportExcel_Click"
                                    >
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
         <%--                                               <td>
                                <cc1:CustomRadButton ID="btnOperationWithoutAjaxReportPdf" runat="server" 
                                    CustomeButtonType="PdfExport" CausesValidation="False" onclick="btnOperationWithoutAjaxReportPdf_Click" 
                                    >
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>--%>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="15%">
                                <asp:Label ID="lblOccasionType" runat="server">نوع مناسبت:</asp:Label>
                            </td>
                            <td width="35%">
                                <cc1:CustomRadComboBox ID="cmbOccasionType" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="cmbOccasionType_SelectedIndexChanged">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td nowrap="nowrap" width="15%">
                                <asp:Label runat="server" ID="lblOccasionTitle">عنوان  مناسبت:</asp:Label>
                            </td>
                            <td width="35%">
                                <cc1:CustomRadComboBox ID="cmbOccasionTitle" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="cmbOccasionTitle_SelectedIndexChanged">
                                </cc1:CustomRadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblMessageDraft">عنوان نسخه پیش نویس:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbMessageDraft" runat="server" AppendDataBoundItems="True"
                                     Width="400px">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblMessageText">متن پیغام:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtMessageText" runat="server" TextMode="MultiLine" Width="400px">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblSendDate">تاریخ ارسال:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar ID="txtSendDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblSendTime">زمان ارسال:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadMaskedTextBox Width="45" ID="txtSendTime" Mask="##:##" runat="server">
                                </telerik:RadMaskedTextBox>
                            </td>
                        </tr>
                        <tr>
                             <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblFullName">نام و نام خانوادگی:</asp:Label>
                            </td>
                            <td>
                               <cc1:SelectControl ID="SelectControlPerson" runat="server" PortalPathUrl="GeneralProject/General/ModalForm/PersonPage.aspx"  />           
                            </td>
                            <td>
                                 <asp:Label runat="server" ID="lblMobileNo">شماره موبایل:</asp:Label>
                            </td>
                            <td>
                                <cc1:NumericTextBox ID="txtMobileNo" runat="server" Text=""></cc1:NumericTextBox>
                            </td>

                        </tr>
                        <tr>
                                                                                                 
                            <td nowrap="nowrap" width="15%">
                                <asp:Label runat="server" ID="lblSmsServiceProvider">عنوان مهیا کننده سرویس:</asp:Label>
                            </td>
                            <td width="100%">
                                <cc1:CustomRadComboBox ID="cmbSmsServiceProvider" runat="server" 
                                    AppendDataBoundItems="True" AutoPostBack="True" onselectedindexchanged="cmbSmsServiceProvider_SelectedIndexChanged"  
                                    >
                                </cc1:CustomRadComboBox>
                                 
                            </td>
               

                                                        <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblMessageStatus">وضیعت پیامک:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbMessageStatus" runat="server" 
                                    AppendDataBoundItems="True" Width="300px">
                                </cc1:CustomRadComboBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td dir="rtl">
                                <telerik:RadGrid ID="grdSms" runat="server" AllowCustomPaging="True" AllowPaging="True" 
                                    AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Right" 
                                    AutoGenerateColumns="False" onpageindexchanged="grdSms_PageIndexChanged" 
                                    onpagesizechanged="grdSms_PageSizeChanged" 
                                    onsortcommand="grdSms_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                     <ExportSettings>
                                 <Pdf DefaultFontFamily="Arial Unicode MS" />
                                     </ExportSettings>
                                    <MasterTableView DataKeyNames="SmsId" Dir="RTL" GroupsDefaultExpanded="False" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد"  >
                                        <Columns>
                                             <telerik:GridBoundColumn DataField="FullName" HeaderText="نام و نام خانوادگی"
                                                SortExpression="FullName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                                 <telerik:GridBoundColumn DataField="MobileNo" HeaderText="شماره موبایل"
                                                SortExpression="MobileNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="OccasionDayTypeTitle" HeaderText="عنوان نوع مناسبت "
                                                SortExpression="OccasionDayTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="OccasionDayTitle" HeaderText="عنوان مناسبت " SortExpression="OccasionDayTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MessageDraftText" HeaderText="عنوان نسخه پیش نویس "
                                                SortExpression="MessageDraftText">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="متن پیام" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="MessageText">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtMessageText" runat="server"  Text='<%#Eval("MessageText").ToString() %>'
                                                        TextMode="MultiLine" BackColor="#FFFFCC"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="SendDate" HeaderText="تاریخ ارسال" SortExpression="SendDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="SendTime" HeaderText="زمان ارسال" SortExpression="SendTime">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MessageStatusPersianTitle" HeaderText="وضیعت پیامک"
                                                SortExpression="MessageStatusPersianTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                        </Columns>

                                        <CommandItemSettings ExportToPdfText="Export to PDF" />

                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, مناسبت فردی &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbOccasionType">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbOccasionType" />
                <telerik:AjaxUpdatedControl ControlID="cmbOccasionTitle" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbOccasionTitle">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbOccasionTitle" />
                <telerik:AjaxUpdatedControl ControlID="cmbMessageDraft" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbSmsServiceProvider">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbSmsServiceProvider" />
                <telerik:AjaxUpdatedControl ControlID="cmbMessageStatus" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
