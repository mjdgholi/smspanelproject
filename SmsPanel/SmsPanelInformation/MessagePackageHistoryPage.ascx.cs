﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using Intranet.Security;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.SmsPanelInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/23>
    /// Description: < بسته پیامک>
    /// </summary>
    public partial class MessagePackageHistoryPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly MessageDraftBL _messageDraftBL = new MessageDraftBL();
        private readonly MessagePackageHistoryBL _messagePackageHistoryBL = new MessagePackageHistoryBL();
        private readonly OccasionDayBL _occasionDayBL = new OccasionDayBL();
        private readonly OccasionDayTypeBL _occasionDayTypeBL = new OccasionDayTypeBL();
        private  readonly  Occasion_MessageDraftBL _occasionMessageDraftBL=new Occasion_MessageDraftBL();
        private  readonly  GroupBL _groupBL=new GroupBL();
        private  readonly  SmsServiceProviderBL _smsServiceProviderBL=new SmsServiceProviderBL();  
        private const string TableName = "SMSPanel.V_MessagePackageHistory";
        private const string PrimaryKey = "MessagePackageHistoryId";
        #endregion


        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;            
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtMessageText.Text = "";
            cmbOccasionType.ClearSelection();
            cmbOccasionTitle.ClearSelection();
            cmbMessageDraft.ClearSelection();
            cmbOccasionTitle.Items.Clear();
            cmbMessageDraft.Items.Clear();            
            txtSendDate.Text = "";
            txtSendTime.Text = "";
            radTreeGroup.UncheckAllNodes();
            SelectControlPerson.KeyId = "";
            SelectControlPerson.Title = "";
            SelectControlPerson.WhereClause = "";

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var messagePackageHistoryEntity = new MessagePackageHistoryEntity()
            {
                MessagePackageHistoryId = new Guid(ViewState["MessagePackageHistoryId"].ToString())
            };
            var myMessagePackageHistory = _messagePackageHistoryBL.GetSingleById(messagePackageHistoryEntity);
            txtMessageText.Text = myMessagePackageHistory.MessageText;
            if (!string.IsNullOrEmpty(myMessagePackageHistory.OccasionDayId.ToString()))
            {
                var occastion = new OccasionDayEntity()
                {
                    OccasionDayId = int.Parse(myMessagePackageHistory.OccasionDayId.ToString())
                };
                var myoccastion = _occasionDayBL.GetSingleById(occastion);
                cmbOccasionType.SelectedValue = myoccastion.OccasionDayId.ToString();
                SetComboBoxOccasion(myoccastion.OccasionDayTypeId);
                cmbOccasionTitle.SelectedValue = myMessagePackageHistory.OccasionDayId.ToString();
                SetComboBoxMessageDraft(int.Parse(myMessagePackageHistory.OccasionDayId.ToString()));
                cmbMessageDraft.SelectedValue = myMessagePackageHistory.MessageDraftId.ToString();
            }
            
     
            txtSendDate.Text = myMessagePackageHistory.SendDate.ToString("yyyy/MM/dd");
            txtSendTime.Text = myMessagePackageHistory.SendTime;
            SetSelectedNodeForTree(myMessagePackageHistory.GroupIdStr);
            SelectControlPerson.WhereClause = myMessagePackageHistory.PersonMoblieIdShowSelectControlStr+"|"+myMessagePackageHistory.FullNameShowSelectControlStr;
            SelectControlPerson.ToolTip = myMessagePackageHistory.FullNameShowSelectControlStr;
            SelectControlPerson.Title = myMessagePackageHistory.FullNameShowSelectControlStr;
            SelectControlPerson.KeyId = myMessagePackageHistory.PersonMoblieIdShowSelectControlStr;
            cmbSmsServiceProvider.SelectedValue = myMessagePackageHistory.SmsServiceProviderId.ToString();
        }


        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdMessagePackageHistory,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        void SetComboBox()
        {
            cmbOccasionType.Items.Clear();
            cmbOccasionType.Items.Add(new RadComboBoxItem(""));
            cmbOccasionType.DataSource = _occasionDayTypeBL.GetAllIsActive();
            cmbOccasionType.DataTextField = "OccasionDayTypeTitle";
            cmbOccasionType.DataValueField = "OccasionDayTypeId";
            cmbOccasionType.DataBind();

            cmbSmsServiceProvider.Items.Clear();
            cmbSmsServiceProvider.DataSource = _smsServiceProviderBL.GetAll();
            cmbSmsServiceProvider.DataTextField = "SmsServiceProviderName";
            cmbSmsServiceProvider.DataValueField = "SmsServiceProviderId";
            cmbSmsServiceProvider.DataBind();
   
        }

        void SetComboBoxOccasion(int? OccasionTypeId)
        {
            cmbOccasionTitle.Items.Clear();
            cmbOccasionTitle.Items.Add(new RadComboBoxItem(""));
            OccasionDayEntity occasionEntity=new OccasionDayEntity()
            {
                OccasionDayTypeId= OccasionTypeId
            };
            cmbOccasionTitle.DataSource = _occasionDayBL.GetOccasionDayCollectionByOccasionDayType(occasionEntity);
            cmbOccasionTitle.DataTextField = "OccasionDayTitle";
            cmbOccasionTitle.DataValueField = "OccasionDayId";
            cmbOccasionTitle.DataBind();
        }
        void SetComboBoxMessageDraft(int OccasionId)
        {
            cmbMessageDraft.Items.Clear();
            cmbMessageDraft.Items.Add(new RadComboBoxItem(""));
            Occasion_MessageDraftEntity occasionMessageDraftEntity=new Occasion_MessageDraftEntity()
            {
                OccasionDayId = OccasionId
            };
            cmbMessageDraft.DataSource = _occasionMessageDraftBL.GetOccasion_MessageDraftCollectionByOccasion(occasionMessageDraftEntity);
            cmbMessageDraft.DataTextField = "MessageDraftText";
            cmbMessageDraft.DataValueField = "MessageDraftId";
            cmbMessageDraft.DataBind();
        }

        void SetTreeGroup()
        {
            radTreeGroup.DataTextField = "GroupTitle";
            radTreeGroup.DataValueField = "GroupId";
            radTreeGroup.DataFieldID = "GroupId";
            radTreeGroup.DataFieldParentID = "OwnerId";
            radTreeGroup.DataSource = _groupBL.GetAllNodes("");
            radTreeGroup.DataBind();
        }

         string GetValueCheckinTree()
         {
             string nodeCheckvalue="";
            foreach (var node in radTreeGroup.CheckedNodes)
            {
                nodeCheckvalue = nodeCheckvalue+(nodeCheckvalue == "" ? "" : ",") + node.Value;
            }
             return nodeCheckvalue;
         }
         private void SetSelectedNodeForTree(string groupIdStr)
         {
             SetTreeGroup();
             string[] arr=groupIdStr.Split(',');

             for (int i = 0; i < arr.Length; i++)
             {
                 foreach (var radTreeNode in radTreeGroup.GetAllNodes())
                 {
                     if (radTreeNode.Value.ToLower() == arr[i].ToLower())
                     {
                         radTreeNode.ExpandParentNodes();
                         radTreeNode.Checked = true;
                     }
                 }
             }

         }

       void checkvalidation()
        {
           if(radTreeGroup.CheckedNodes.Count==0 && SelectControlPerson.KeyId=="" )
                    throw new ItcApplicationErrorManagerException("لطفا از درخت گروه کاربران و یا سایر اشخاص انتخاب نمایید");
        }
        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "AcctionTypeId = 1";
                ViewState["SortExpression"] = "CreationDate";
                ViewState["SortType"] = "Desc";
                var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
                SetTreeGroup();
            
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "AcctionTypeId = 1";
                ViewState["SortExpression"] = "CreationDate";
                ViewState["SortType"] = "Desc";
                var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
                SetTreeGroup();

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Guid MessagePackageHistoryId;
            try
            {
                checkvalidation();
                ViewState["WhereClause"] = "AcctionTypeId = 1";
                
                var messagePackageHistoryEntity = new MessagePackageHistoryEntity()
                {
                    MessageDraftId =(cmbMessageDraft.SelectedIndex > 0 ? Guid.Parse(cmbMessageDraft.SelectedValue) : (Guid?) null),
                    OccasionDayId =(cmbOccasionTitle.SelectedIndex > 0 ? int.Parse(cmbOccasionTitle.SelectedValue) : (int?) null),
                    MessageText = txtMessageText.Text,
                    SendDate =DateTime.Parse(ItcToDate.ShamsiToMiladi(txtSendDate.Text) + " " + txtSendTime.TextWithLiterals),
                    GroupIdStr = GetValueCheckinTree(),
                    PersonMoblieIdStr = SelectControlPerson.KeyId,
                    UserAndGroupId = int.Parse(System.Web.HttpContext.Current.User.Identity.Name),
                    SmsServiceProviderId = Guid.Parse(cmbSmsServiceProvider.SelectedValue)
                };

                _messagePackageHistoryBL.Add(messagePackageHistoryEntity, out MessagePackageHistoryId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, 0, MessagePackageHistoryId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس مناسبت فردیهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                 ViewState["WhereClause"] = "AcctionTypeId = 1"; 
                if (txtMessageText.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageText Like N'%" +
                                               FarsiToArabic.ToArabic(txtMessageText.Text.Trim()) + "%'";
                if (cmbMessageDraft.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageDraftId ='" +
                                               Guid.Parse(cmbMessageDraft.SelectedValue) + "'";
                if (cmbOccasionType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayTypeId ='" +
                                               int.Parse(cmbOccasionType.SelectedValue) + "'";
                if (cmbOccasionTitle.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayId ='" +
                                               int.Parse(cmbOccasionTitle.SelectedValue) + "'";
                if (txtSendDate.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SendDate = '" +
                                               txtSendDate.Text + "'";
                if (txtSendTime.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SendTime = '" +
                                               txtSendTime.TextWithLiterals + "'";
                if (radTreeGroup.CheckedNodes.Count != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and GroupId in (select value from dbo.Fn_CommaStr2Table ('" +
                                  GetValueCheckinTree() + "'))";
                grdMessagePackageHistory.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdMessagePackageHistory.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "AcctionTypeId = 1";
                var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                checkvalidation();

                var messagePackageHistoryEntity = new MessagePackageHistoryEntity()
                {
                    MessagePackageHistoryId = Guid.Parse(ViewState["MessagePackageHistoryId"].ToString()),
                    MessageDraftId = (cmbMessageDraft.SelectedIndex > 0 ? Guid.Parse(cmbMessageDraft.SelectedValue) : (Guid?)null),
                    OccasionDayId = (cmbOccasionTitle.SelectedIndex > 0 ? int.Parse(cmbOccasionTitle.SelectedValue) : (int?)null),
                    MessageText = txtMessageText.Text,
                    SendDate = DateTime.Parse(ItcToDate.ShamsiToMiladi(txtSendDate.Text) + " " + txtSendTime.TextWithLiterals),
                    GroupIdStr = GetValueCheckinTree(),
                    PersonMoblieIdStr = SelectControlPerson.KeyId,
                    UserAndGroupId = int.Parse(System.Web.HttpContext.Current.User.Identity.Name),
                    SmsServiceProviderId = Guid.Parse(cmbSmsServiceProvider.SelectedValue)
                };
                _messagePackageHistoryBL.Update(messagePackageHistoryEntity);
                SetPanelFirst();
                SetClearToolBox();
      

                var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdMessagePackageHistory_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["MessagePackageHistoryId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetClearToolBox();
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "AcctionTypeId = 1";

                    var messagePackageHistoryEntity = new MessagePackageHistoryEntity()
                    {
                        MessagePackageHistoryId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _messagePackageHistoryBL.Delete(messagePackageHistoryEntity);
                    var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdMessagePackageHistory_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdMessagePackageHistory_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdMessagePackageHistory_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdMessagePackageHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
     

        protected void cmbOccasionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value != "")
            {
                SetComboBoxOccasion(int.Parse(e.Value));    
            }
            else
            {
                cmbOccasionTitle.Items.Clear();
                cmbOccasionTitle.Text = "";
                cmbMessageDraft.Items.Clear();
                cmbMessageDraft.Text = "";
            }
            
        }

        protected void cmbOccasionTitle_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value != "")
            {
                SetComboBoxMessageDraft(int.Parse(e.Value));    
            }
            else
            {
            cmbMessageDraft.Items.Clear();
                cmbMessageDraft.Text = "";
            }
            
        }

        protected void radTreeGroup_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            try
            {

                var navigateUrl = "../ModalForm/GroupPersonPage.aspx?WhereClause='" + e.Node.Value + "'";
                var newWindow = new RadWindow
                {
                    NavigateUrl = navigateUrl,
                    Top = Unit.Pixel(22),
                    VisibleOnPageLoad = true,
                    Left = Unit.Pixel(0),
                    DestroyOnClose = true,
                    Modal = true,
                    ReloadOnShow = true,
                };
                RadWindowItemValue.Windows.Add(newWindow);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion

    }
}
