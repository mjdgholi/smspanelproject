﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Common.ItcException;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.SmsPanelInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/17>
    /// Description: <پیامک با مناسبت>
    /// </summary>
    public partial class Group_Occasion_SMSPage : ItcBaseControl
    {
        #region PublicParam:

        private readonly MessageDraftBL _messageDraftBL = new MessageDraftBL();
        private readonly SmsServiceProviderBL _smsServiceProviderBL = new SmsServiceProviderBL();

        private readonly Group_Occasion_SMSBL _groupOccasionSmsbl = new Group_Occasion_SMSBL();
        private readonly OccasionDayBL _occasionDayBL = new OccasionDayBL();
        private readonly OccasionDayTypeBL _occasionDayTypeBL = new OccasionDayTypeBL();
        private readonly Occasion_MessageDraftBL _occasionMessageDraftBL = new Occasion_MessageDraftBL();
        private readonly GroupBL _groupBL = new GroupBL();
        private const string TableName = "SMSPanel.V_Group_Occasion_SMS";
        private const string PrimaryKey = "Group_Occasion_SMSId";
        private readonly MessagePackageHistoryBL _messagePackageHistoryBL = new MessagePackageHistoryBL();
        #endregion


        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            radTreeGroup.CheckBoxes = true;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            radTreeGroup.CheckBoxes = false;

        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtMessageText.Text = "";
            cmbOccasionType.ClearSelection();
            cmbOccasionTitle.ClearSelection();
            cmbMessageDraft.ClearSelection();
            cmbOccasionTitle.Items.Clear();
            cmbMessageDraft.Items.Clear();
            txtFromDate.Text = "";
            txtEndDate.Text = "";
            txtSendTime.Text = "";
            radTreeGroup.UncheckAllNodes();
            txtDifferenceDay.Text = "0";
            radiobtnDifferenceDay.Items[radiobtnDifferenceDay.SelectedIndex].Selected = false;
            radiobtnDifferenceDay.Items[1].Selected = true;
            radTreeGroup.UnselectAllNodes();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var groupOccasionSmsEnity = new Group_Occasion_SMSEnity()
            {
                Group_Occasion_SMSId = new Guid(ViewState["Group_Occasion_SMSId"].ToString())
            };
            var mygroupOccasionSms = _groupOccasionSmsbl.GetSingleById(groupOccasionSmsEnity);
            txtMessageText.Text = mygroupOccasionSms.MessageText;
            txtFromDate.Text = mygroupOccasionSms.FromDate;
            txtEndDate.Text = mygroupOccasionSms.EndDate;
            txtSendTime.Text = mygroupOccasionSms.SendTime;
            cmbOccasionType.SelectedValue = mygroupOccasionSms.OccasionDayTypeId.ToString();
            if (mygroupOccasionSms.OccasionDayTypeId != null)
                SetComboBoxOccasion(mygroupOccasionSms.OccasionDayTypeId);
            cmbOccasionTitle.SelectedValue = mygroupOccasionSms.OccasionDayId.ToString();
            if (mygroupOccasionSms.OccasionDayId != null)
                SetComboBoxMessageDraft(mygroupOccasionSms.OccasionDayId);
            cmbMessageDraft.SelectedValue = mygroupOccasionSms.MessageDraftId.ToString();

            if (mygroupOccasionSms.DifferenceDay.ToString().StartsWith("-"))
            {
                txtDifferenceDay.Text = mygroupOccasionSms.DifferenceDay.ToString()
                    .Substring(1, mygroupOccasionSms.DifferenceDay.ToString().Length - 1);

                radiobtnDifferenceDay.Items[radiobtnDifferenceDay.SelectedIndex].Selected = false;
                radiobtnDifferenceDay.Items[0].Selected = true;

            }

            else
            {
                txtDifferenceDay.Text = mygroupOccasionSms.DifferenceDay.ToString();
                radiobtnDifferenceDay.Items[radiobtnDifferenceDay.SelectedIndex].Selected = false;
                radiobtnDifferenceDay.Items[1].Selected = true;
            }
            


            RadTreeNode radTreeNodeGroup = radTreeGroup.FindNodeByValue(mygroupOccasionSms.GroupId.ToString());
            radTreeNodeGroup.Selected = true;

            while (radTreeNodeGroup.ParentNode != null && radTreeNodeGroup != null)
            {

                radTreeNodeGroup.ParentNode.Expanded = true;
                radTreeNodeGroup = (radTreeNodeGroup.ParentNode == null ? null : (radTreeGroup.Nodes.FindNodeByValue(radTreeNodeGroup.ParentNode.Value)));
            }
        }



        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdGroup_Occasion_SMS,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        private void SetComboBox()
        {
            cmbOccasionType.Items.Clear();
            cmbOccasionType.Items.Add(new RadComboBoxItem(""));
            cmbOccasionType.DataSource = _occasionDayTypeBL.GetAllIsActive();
            cmbOccasionType.DataTextField = "OccasionDayTypeTitle";
            cmbOccasionType.DataValueField = "OccasionDayTypeId";
            cmbOccasionType.DataBind();



            cmbSmsServiceProvider.Items.Clear();
            cmbSmsServiceProvider.DataSource = _smsServiceProviderBL.GetAll();
            cmbSmsServiceProvider.DataTextField = "SmsServiceProviderName";
            cmbSmsServiceProvider.DataValueField = "SmsServiceProviderId";
            cmbSmsServiceProvider.DataBind();
        }

        private void SetComboBoxOccasion(int? OccasionTypeId)
        {
            cmbOccasionTitle.Items.Clear();
            cmbOccasionTitle.Items.Add(new RadComboBoxItem(""));
            OccasionDayEntity occasionEntity = new OccasionDayEntity()
            {
                OccasionDayTypeId = OccasionTypeId
            };
            cmbOccasionTitle.DataSource = _occasionDayBL.GetOccasionDayCollectionByOccasionDayType(occasionEntity);
            cmbOccasionTitle.DataTextField = "OccasionDayTitle";
            cmbOccasionTitle.DataValueField = "OccasionDayId";
            cmbOccasionTitle.DataBind();
        }

        private void SetComboBoxMessageDraft(int? OccasionId)
        {
            cmbMessageDraft.Items.Clear();
            cmbMessageDraft.Items.Add(new RadComboBoxItem(""));
            Occasion_MessageDraftEntity occasionMessageDraftEntity = new Occasion_MessageDraftEntity()
            {
                OccasionDayId = OccasionId
            };
            cmbMessageDraft.DataSource =
                _occasionMessageDraftBL.GetOccasion_MessageDraftCollectionByOccasion(occasionMessageDraftEntity);
            cmbMessageDraft.DataTextField = "MessageDraftText";
            cmbMessageDraft.DataValueField = "MessageDraftId";
            cmbMessageDraft.DataBind();
        }

        private void SetTreeGroup()
        {
            radTreeGroup.DataTextField = "GroupTitle";
            radTreeGroup.DataValueField = "GroupId";
            radTreeGroup.DataFieldID = "GroupId";
            radTreeGroup.DataFieldParentID = "OwnerId";
            radTreeGroup.DataSource = _groupBL.GetAll();
            radTreeGroup.DataBind();
        }

        private string GetValueCheckinTree()
        {
            string nodeCheckvalue = "";
            foreach (var node in radTreeGroup.CheckedNodes)
            {
                nodeCheckvalue = nodeCheckvalue + (nodeCheckvalue == "" ? "" : ",") + node.Value;
            }
            return nodeCheckvalue;
        }

        private void SetSelectedNodeForTree(string groupIdStr)
        {
            SetTreeGroup();
            string[] arr = groupIdStr.Split(',');

            for (int i = 0; i < arr.Length; i++)
            {
                foreach (var radTreeNode in radTreeGroup.GetAllNodes())
                {
                    if (radTreeNode.Value.ToLower() == arr[i].ToLower())
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Checked = true;
                    }
                }
            }

        }

        #endregion


        #region ControlEvent:
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "";
                    ViewState["SortExpression"] = "CreationDate";
                    ViewState["SortType"] = "Desc";
                    var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetComboBox();
                    SetTreeGroup();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "";
                ViewState["SortExpression"] = "CreationDate";
                ViewState["SortType"] = "Desc";
                var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
                SetTreeGroup();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid groupItemSmsId;
                ViewState["WhereClause"] = "";
                if (radTreeGroup.CheckedNodes.Count == 0)
                    throw new ItcApplicationErrorManagerException("لطفا از درخت گروه کاربران  انتخاب نمایید");
                var groupOccasionSmsEnity = new Group_Occasion_SMSEnity()
                {
                    FromDate = ItcToDate.ShamsiToMiladi(txtFromDate.Text),
                    SmsServiceProviderId = Guid.Parse(cmbSmsServiceProvider.SelectedValue),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    MessageDraftId =
                        (cmbMessageDraft.SelectedIndex > 0 ? Guid.Parse(cmbMessageDraft.SelectedValue) : (Guid?) null),
                    GroupIdStr = GetValueCheckinTree(),
                    MessageText = txtMessageText.Text,
                    SendTime = txtSendTime.TextWithLiterals,
                    OccasionDayId = int.Parse(cmbOccasionTitle.SelectedValue),
                    DifferenceDay = int.Parse((radiobtnDifferenceDay.SelectedItem.Value+txtDifferenceDay.Text)),                    
                };

                _groupOccasionSmsbl.Add(groupOccasionSmsEnity, out groupItemSmsId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس مناسبت فردیهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtMessageText.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageText Like N'%" +
                                               FarsiToArabic.ToArabic(txtMessageText.Text.Trim()) + "%'";
                if (txtFromDate.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FromDate ='" +
                                               (txtFromDate.Text.Trim()) + "'";
                if (txtEndDate.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                               (txtEndDate.Text.Trim()) + "'";
                if (cmbMessageDraft.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageDraftId ='" +
                                               Guid.Parse(cmbMessageDraft.SelectedValue) + "'";
                if (cmbOccasionType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayTypeId ='" +
                                               int.Parse(cmbOccasionType.SelectedValue) + "'";
                if (cmbOccasionTitle.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayId ='" +
                                               int.Parse(cmbOccasionTitle.SelectedValue) + "'";

                if (txtSendTime.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SendTime = '" +
                                               txtSendTime.TextWithLiterals + "'";

                if (radTreeGroup.CheckedNodes.Count != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] +
                                               " and GroupId in (select value from dbo.Fn_CommaStr2Table ('" +
                                               GetValueCheckinTree() + "'))";
                grdGroup_Occasion_SMS.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdGroup_Occasion_SMS.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(radTreeGroup.SelectedValue))
                    throw new ItcApplicationErrorManagerException("لطفا از درخت گروه کاربران انتخاب نمایید");
                var groupOccasionSmsEnity = new Group_Occasion_SMSEnity()
                {
                    Group_Occasion_SMSId = Guid.Parse(ViewState["Group_Occasion_SMSId"].ToString()),
                    FromDate = ItcToDate.ShamsiToMiladi(txtFromDate.Text),
                    SmsServiceProviderId = Guid.Parse(cmbSmsServiceProvider.SelectedValue),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    MessageDraftId =
                        (cmbMessageDraft.SelectedIndex > 0 ? Guid.Parse(cmbMessageDraft.SelectedValue) : (Guid?)null),
                    GroupId= Guid.Parse(radTreeGroup.SelectedValue),
                    MessageText = txtMessageText.Text,
                    SendTime = txtSendTime.TextWithLiterals,
                    OccasionDayId = int.Parse(cmbOccasionTitle.SelectedValue),
                    DifferenceDay = int.Parse((radiobtnDifferenceDay.SelectedItem.Value + txtDifferenceDay.Text)),
                };
                _groupOccasionSmsbl.Update(groupOccasionSmsEnity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, 0,
                    new Guid(ViewState["Group_Occasion_SMSId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>



        protected void grdGroup_Occasion_SMS_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["Group_Occasion_SMSId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetPanelLast();
                    SetDataShow();

                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";
                    var groupOccasionSmsEnity = new Group_Occasion_SMSEnity()
                    {
                        Group_Occasion_SMSId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _groupOccasionSmsbl.Delete(groupOccasionSmsEnity);
                    var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdGroup_Occasion_SMS_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, e.NewPageIndex,
                    new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdGroup_Occasion_SMS_PageSizeChanged(object sender,
            Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdGroup_Occasion_SMS_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdGroup_Occasion_SMS.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        protected void cmbOccasionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value != "")
            {
                SetComboBoxOccasion(int.Parse(e.Value));
            }
            else
            {
                cmbOccasionTitle.Items.Clear();
                cmbOccasionTitle.Text = "";
                cmbMessageDraft.Items.Clear();
                cmbMessageDraft.Text = "";
            }
        }

        protected void cmbOccasionTitle_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value != "")
            {
                SetComboBoxMessageDraft(int.Parse(e.Value));
            }
            else
            {
                cmbMessageDraft.Items.Clear();
                cmbMessageDraft.Text = "";
            }
        }

        protected void btnRunJob_Click(object sender, EventArgs e)
        {


            try
            {
                Session["GroupSmsDS"] = _messagePackageHistoryBL.GenerateMessagePackageForSmswithOccasion();
                var navigateUrl = "../ModalForm/GroupSmsMessagePage.aspx";
                var newWindow = new RadWindow
                {
                    NavigateUrl = navigateUrl,
                    Top = Unit.Pixel(22),
                    VisibleOnPageLoad = true,
                    Left = Unit.Pixel(0),
                    DestroyOnClose = true,
                    Modal = true,
                    ReloadOnShow = true,
                };
                RadWindowItemValue.Windows.Add(newWindow);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        protected void radTreeGroup_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            try
            {

                var navigateUrl = "../ModalForm/GroupPersonPage.aspx?WhereClause='" + e.Node.Value + "'";
                var newWindow = new RadWindow
                {
                    NavigateUrl = navigateUrl,
                    Top = Unit.Pixel(22),
                    VisibleOnPageLoad = true,
                    Left = Unit.Pixel(0),
                    DestroyOnClose = true,
                    Modal = true,
                    ReloadOnShow = true,
                };
                RadWindowItemValue.Windows.Add(newWindow);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion

    }
}


