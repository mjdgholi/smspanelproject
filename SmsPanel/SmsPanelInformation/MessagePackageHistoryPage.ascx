﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessagePackageHistoryPage.ascx.cs"
    Inherits="Intranet.DesktopModules.SmsPanelProject.SmsPanel.SmsPanelInformation.MessagePackageHistoryPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<telerik:RadScriptBlock runat="server">
<script type="text/javascript">
    function ShowMessageText(sender, args) {
        $find("<%= txtMessageText.ClientID %>").set_value(args.get_item().get_text()); ;       

    }
</script>
</telerik:RadScriptBlock>
<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; " class="MainTableOfASCX"dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Panel ID="pnlDetail" runat="server" Width="100%" HorizontalAlign="Right">
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblGroupTitle" runat="server" Text="درخت گروه اشخاص" 
                                    Font-Bold="True"></asp:Label>                                    
                                        </td>
                                        <td>
                                        <asp:Label ID="Label1" runat="server" Text="و" 
                                    Font-Bold="True"></asp:Label>                                        
                                        </td>
                                        <td>
                                 <asp:Label ID="lblotherPersonMobile" runat="server" Text="سایر اشخاص" 
                                    Font-Bold="True"></asp:Label>                                    
                                        </td>
                                        <td>
                                            <cc1:SelectControl ID="SelectControlPerson" runat="server" PortalPathUrl="SmsPanelProject/SmsPanel/ModalForm/PersonMobilePage.aspx"  imageName="SelectPerson"/>
                                        </td>

                                    </tr>
                                </table>
                                
                            </td>  
                                             
                        </tr>
                        <tr>
                            <td valign="top" width="30%">
                                    <telerik:RadTreeView ID="radTreeGroup" runat="server" BorderColor="#999999" BorderStyle="Solid"
                                    BorderWidth="1px" dir="rtl" Skin="WebBlue" CausesValidation="False" Height="300px"
                                    Width="100%" CheckBoxes="True"  oncontextmenuitemclick="radTreeGroup_ContextMenuItemClick">
                                                                                                                          <ContextMenus>
                                    <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server" Skin="Office2007">
                                        <Items>
                                            <telerik:RadMenuItem ImageUrl="../Images/GroupPersonIcon.png" PostBack="True" Text="اشخاص در گروه" Height="27"
                                                Value="GroupPerson">
                                            </telerik:RadMenuItem>
                            
                                        </Items>
                                        <CollapseAnimation Type="OutQuint" />
                                    </telerik:RadTreeViewContextMenu>
                                </ContextMenus>
                                </telerik:RadTreeView> 
                            </td>
                            <td valign="top" width="70%">
                                <table width="100%" dir="rtl">
                                                                                                                  <tr>
                            <td nowrap="nowrap" width="15%">
                                <asp:Label runat="server" ID="lblSmsServiceProvider">عنوان مهیا کننده سرویس<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="100%">
                                <cc1:CustomRadComboBox ID="cmbSmsServiceProvider" runat="server" 
                                    AppendDataBoundItems="True"  
                                    >
                                </cc1:CustomRadComboBox>
                                                             <asp:RequiredFieldValidator ID="rfvSmsServiceProvider" runat="server" ControlToValidate="cmbSmsServiceProvider"
                                                ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                                 <tr>
                            <td nowrap="nowrap" width="15%">
                                <asp:Label runat="server" ID="lblOccasionType">نوع  مناسبت:</asp:Label>
                            </td>
                            <td width="100%">
                                <cc1:CustomRadComboBox ID="cmbOccasionType" runat="server"  AllowCustomText="True"      Filter="Contains"
                                    AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="False" 
                                    onselectedindexchanged="cmbOccasionType_SelectedIndexChanged">
                                </cc1:CustomRadComboBox>
                      
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="15%">
                                <asp:Label runat="server" ID="lblOccasionTitle">عنوان  مناسبت:</asp:Label>
                            </td>
                            <td width="100%">
                                <cc1:CustomRadComboBox ID="cmbOccasionTitle" runat="server"  AllowCustomText="True"      Filter="Contains"
                                    AppendDataBoundItems="True"  AutoPostBack="True" CausesValidation="False" 
                                    onselectedindexchanged="cmbOccasionTitle_SelectedIndexChanged">
                                </cc1:CustomRadComboBox>
                      
                            </td>
                        </tr>
                        <tr>
                            
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblMessageDraft">عنوان نسخه پیش نویس:</asp:Label>
                            </td>
                            <td width="100%">
                                <cc1:CustomRadComboBox ID="cmbMessageDraft" runat="server"  AllowCustomText="True"      Filter="Contains"
                                    AppendDataBoundItems="True" onclientselectedindexchanged="ShowMessageText" 
                                    Width="400px">
                                </cc1:CustomRadComboBox>
            
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblMessageText">متن پیغام<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="100%">
                                <telerik:RadTextBox ID="txtMessageText" runat="server" TextMode="MultiLine" 
                                    Width="400px">
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="rfvMessageText" runat="server" ControlToValidate="txtMessageText"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" >
                                <asp:Label runat="server" ID="lblSendDate">تاریخ ارسال<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="100%" >
                                <cc1:CustomItcCalendar ID="txtSendDate" runat="server"></cc1:CustomItcCalendar>
                                <asp:RequiredFieldValidator ID="rfvSendDate" runat="server" ControlToValidate="txtSendDate"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblSendTime">زمان ارسال<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="100%">
                             <telerik:RadMaskedTextBox  Width="45" ID="txtSendTime" Mask="##:##"  runat="server"></telerik:RadMaskedTextBox>
                                <asp:RequiredFieldValidator ID="rfvSendTime" runat="server" ControlToValidate="txtSendTime"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                                      <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdMessagePackageHistory" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" 
                                    onitemcommand="grdMessagePackageHistory_ItemCommand" 
                                    onpageindexchanged="grdMessagePackageHistory_PageIndexChanged" 
                                    onpagesizechanged="grdMessagePackageHistory_PageSizeChanged" 
                                    onsortcommand="grdMessagePackageHistory_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="MessagePackageHistoryId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                                                                   <telerik:GridBoundColumn DataField="GroupTitle" HeaderText="عنوان گروه" SortExpression="GroupTitle">
                                                                            <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                        </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="OccasionDayTitle" HeaderText="عنوان مناسبت" SortExpression="OccasionDayTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MessageDraftText" HeaderText="متن نسخه پیش نویس"
                                                SortExpression="MessageDraftText">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MessageText" HeaderText="متن پیغام" SortExpression="MessageText">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="SendDate" HeaderText="تاریخ ارسال" SortExpression="SendDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="SendTime" HeaderText="زمان ارسال" SortExpression="SendTime">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("MessagePackageHistoryId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("MessagePackageHistoryId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, مناسبت فردی &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>  
                            </td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>                    
                </asp:Panel>
            </td>
        </tr>

    </table>
</asp:Panel>
            <telerik:RadWindowManager ID="RadWindowItemValue" runat="server" DestroyOnClose="True" CssClass="windowcss"  Width="850" Height="520"
                EnableViewState="False"  Modal="True"
                ReloadOnShow="True" Skin="Office2007" >
                <Localization Cancel="انصراف" Close="بستن" Maximize="بیشینه" Minimize="کمینه" No="خیر"
                    OK="باشه" Reload="بازنشانی" Yes="بلی" />
            </telerik:RadWindowManager>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radTreeGroup">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="radTreeGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowItemValue" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbOccasionType">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbOccasionType" />
                <telerik:AjaxUpdatedControl ControlID="cmbOccasionTitle" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="cmbMessageDraft" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbOccasionTitle">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbOccasionTitle" />
                <telerik:AjaxUpdatedControl ControlID="cmbMessageDraft" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdMessagePackageHistory">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
