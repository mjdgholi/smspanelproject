﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.SmsPanelInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <پیامک با مناسبت فردی>
    /// </summary>
    public partial class Group_Item_SmsPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly MessageDraftBL _messageDraftBL = new MessageDraftBL();
        private readonly SmsServiceProviderBL _smsServiceProviderBL = new SmsServiceProviderBL();
        private readonly ItemBL _itemBL = new ItemBL();
        private readonly ItemCategoryBL _itemCategoryBL = new ItemCategoryBL();
        private readonly Group_Item_SmsBL _groupItemSmsBL = new Group_Item_SmsBL();
        private readonly OccasionDayBL _occasionDayBL = new OccasionDayBL();
        private readonly OccasionDayTypeBL _occasionDayTypeBL = new OccasionDayTypeBL();
        private readonly Occasion_MessageDraftBL _occasionMessageDraftBL = new Occasion_MessageDraftBL();
        private readonly GroupBL _groupBL = new GroupBL();
        private readonly MessagePackageHistoryBL _messagePackageHistoryBL = new MessagePackageHistoryBL();
        private const string TableName = "SMSPanel.V_Group_Item_Sms";
        private const string PrimaryKey = "Group_Item_SmsId";
        #endregion


        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            radTreeGroup.CheckBoxes = true;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            radTreeGroup.CheckBoxes = false;

        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtMessageText.Text = "";
            cmbOccasionType.ClearSelection();
            cmbOccasionTitle.ClearSelection();
            cmbMessageDraft.ClearSelection();
            cmbOccasionTitle.Items.Clear();
            cmbMessageDraft.Items.Clear();
            txtFromDate.Text = "";
            txtEndDate.Text = "";
            txtSendTime.Text = "";
            radTreeGroup.UncheckAllNodes();
            cmbItem.ClearSelection();
            cmbItemCategory.ClearSelection();
            cmbItemCategory.Text = "";
            RadTreeView tree = (RadTreeView)cmbItemCategory.Items[0].FindControl("radtreeItemCategory");
            tree.UnselectAllNodes();
            radTreeGroup.UnselectAllNodes();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var groupItemSmsEntity = new Group_Item_SmsEntity()
            {
                Group_Item_SmsId = new Guid(ViewState["Group_Item_SmsId"].ToString())
            };
            var mygroupItemSms = _groupItemSmsBL.GetSingleById(groupItemSmsEntity);
            txtMessageText.Text = mygroupItemSms.MessageText;
            txtFromDate.Text = mygroupItemSms.FromDate;
            txtEndDate.Text = mygroupItemSms.EndDate;
            txtSendTime.Text = mygroupItemSms.SendTime;

            cmbSmsServiceProvider.SelectedValue = mygroupItemSms.SmsServiceProviderId.ToString();
            cmbItemCategory.SelectedValue = mygroupItemSms.ItemCategoryId.ToString();
            cmbOccasionType.SelectedValue = mygroupItemSms.OccasionDayTypeId.ToString();
            if(mygroupItemSms.OccasionDayTypeId!=null)
            SetComboBoxOccasion(mygroupItemSms.OccasionDayTypeId);
            cmbOccasionTitle.SelectedValue = mygroupItemSms.OccasionDayId.ToString();
            if (mygroupItemSms.OccasionDayId != null)
            SetComboBoxMessageDraft(mygroupItemSms.OccasionDayId);
            cmbMessageDraft.SelectedValue = mygroupItemSms.MessageDraftId.ToString();
                        
            RadTreeView tree = (RadTreeView)cmbItemCategory.Items[0].FindControl("radtreeItemCategory");
            RadTreeNode radTreeNode=tree.FindNodeByValue(mygroupItemSms.ItemCategoryId.ToString());
            radTreeNode.Selected = true;

            cmbItemCategory.Text = radTreeNode.Text;
            SetComboBoxItem(mygroupItemSms.ItemCategoryId);
            cmbItem.SelectedValue = (mygroupItemSms.ItemId.ToString());

            
            RadTreeNode radTreeNodeGroup = radTreeGroup.FindNodeByValue(mygroupItemSms.GroupId.ToString());
            radTreeNodeGroup.Selected = true;

            while (radTreeNodeGroup.ParentNode != null && radTreeNodeGroup != null)
            {
                
                radTreeNodeGroup.ParentNode.Expanded = true;
                radTreeNodeGroup = (radTreeNodeGroup.ParentNode==null?null:(radTreeGroup.Nodes.FindNodeByValue(radTreeNodeGroup.ParentNode.Value)));
            }
            
            
            
        }

        /// <summary>
        /// 
        /// </summary>
        void SetTreeViewInComboBox()
        {
            RadTreeView tree = (RadTreeView)cmbItemCategory.Items[0].FindControl("radtreeItemCategory");
            tree.Nodes.Clear();
            tree.DataTextField = "ItemGroupTitle";
            tree.DataValueField = "ItemCategoryId";
            tree.DataFieldID = "ItemCategoryId";
            tree.DataFieldParentID = "OwnerId";
            tree.DataSource=_itemCategoryBL.GetAll();
            tree.DataBind();
            
        }
        
        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdGroup_Item_Sms,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        void SetComboBox()
        {
            cmbOccasionType.Items.Clear();
            cmbOccasionType.Items.Add(new RadComboBoxItem(""));
            cmbOccasionType.DataSource = _occasionDayTypeBL.GetAllIsActive();
            cmbOccasionType.DataTextField = "OccasionDayTypeTitle";
            cmbOccasionType.DataValueField = "OccasionDayTypeId";
            cmbOccasionType.DataBind();


            cmbSmsServiceProvider.Items.Clear();            
            cmbSmsServiceProvider.DataSource = _smsServiceProviderBL.GetAll();
            cmbSmsServiceProvider.DataTextField = "SmsServiceProviderName";
            cmbSmsServiceProvider.DataValueField = "SmsServiceProviderId";
            cmbSmsServiceProvider.DataBind();
        }

        void SetComboBoxOccasion(int? OccasionTypeId)
        {
            cmbOccasionTitle.Items.Clear();
            cmbOccasionTitle.Items.Add(new RadComboBoxItem(""));
            OccasionDayEntity occasionEntity = new OccasionDayEntity()
            {
                OccasionDayTypeId = OccasionTypeId
            };
            cmbOccasionTitle.DataSource = _occasionDayBL.GetOccasionDayCollectionByOccasionDayType(occasionEntity);
            cmbOccasionTitle.DataTextField = "OccasionDayTitle";
            cmbOccasionTitle.DataValueField = "OccasionDayId";
            cmbOccasionTitle.DataBind();
        }
        void SetComboBoxMessageDraft(int? OccasionId)
        {
            cmbMessageDraft.Items.Clear();
            cmbMessageDraft.Items.Add(new RadComboBoxItem(""));
            Occasion_MessageDraftEntity occasionMessageDraftEntity = new Occasion_MessageDraftEntity()
            {
                OccasionDayId = OccasionId
            };
            cmbMessageDraft.DataSource = _occasionMessageDraftBL.GetOccasion_MessageDraftCollectionByOccasion(occasionMessageDraftEntity);
            cmbMessageDraft.DataTextField = "MessageDraftText";
            cmbMessageDraft.DataValueField = "MessageDraftId";
            cmbMessageDraft.DataBind();
        }
        void SetComboBoxItem(Guid? itemCategoryId)
        {
            cmbItem.Items.Clear();
            cmbItem.Items.Add(new RadComboBoxItem(""));
            ItemEntity itemEntity = new ItemEntity()
            {
                ItemCategoryId = itemCategoryId
            };
            cmbItem.DataSource = _itemBL.GetItemCollectionByItemCategory(itemEntity);
            cmbItem.DataTextField = "ItemTitle";
            cmbItem.DataValueField = "ItemId";
            cmbItem.DataBind();
        }
        void SetTreeGroup()
        {
            radTreeGroup.DataTextField = "GroupTitle";
            radTreeGroup.DataValueField = "GroupId";
            radTreeGroup.DataFieldID = "GroupId";
            radTreeGroup.DataFieldParentID = "OwnerId";
            radTreeGroup.DataSource = _groupBL.GetAll();
            radTreeGroup.DataBind();
        }

        string GetValueCheckinTree()
        {
            string nodeCheckvalue = "";
            foreach (var node in radTreeGroup.CheckedNodes)
            {
                nodeCheckvalue = nodeCheckvalue + (nodeCheckvalue == "" ? "" : ",") + node.Value;
            }
            return nodeCheckvalue;
        }
        private void SetSelectedNodeForTree(string groupIdStr)
        {
            SetTreeGroup();
            string[] arr = groupIdStr.Split(',');

            for (int i = 0; i < arr.Length; i++)
            {
                foreach (var radTreeNode in radTreeGroup.GetAllNodes())
                {
                    if (radTreeNode.Value.ToLower() == arr[i].ToLower())
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Checked = true;
                    }
                }
            }

        }



        private void SetCmbItem(Guid itemCategoryId)
        {
            cmbItem.Items.Clear();
            cmbItem.Items.Add(new RadComboBoxItem(""));
            ItemEntity itemEntity = new ItemEntity()
            {
                ItemCategoryId = itemCategoryId
            };
            cmbItem.DataSource = _itemBL.GetItemCollectionByItemCategory(itemEntity);
            cmbItem.DataTextField = "ItemTitle";
            cmbItem.DataValueField = "ItemId";
            cmbItem.DataBind();
        }

        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = "CreationDate";
                    ViewState["SortType"] = "Desc";
                    var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetTreeViewInComboBox();
                    SetComboBox();
                    SetTreeGroup();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "";
                ViewState["SortExpression"] = "CreationDate";
                ViewState["SortType"] = "Desc";
                var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetTreeViewInComboBox();
                SetComboBox();
                SetTreeGroup();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid groupItemSmsId;
                ViewState["WhereClause"] = "";
                if (radTreeGroup.CheckedNodes.Count == 0)
                    throw new ItcApplicationErrorManagerException("لطفا از درخت گروه کاربران  انتخاب نمایید");
                var groupItemSmsEntity = new Group_Item_SmsEntity()
                {
                    FromDate =ItcToDate.ShamsiToMiladi(txtFromDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    ItemId = Guid.Parse(cmbItem.SelectedValue),
                    MessageDraftId = (cmbMessageDraft.SelectedIndex>0?Guid.Parse(cmbMessageDraft.SelectedValue):(Guid?)null),
                    GroupIdStr = GetValueCheckinTree(),
                    MessageText = txtMessageText.Text,
                    SendTime = txtSendTime.TextWithLiterals, 
                    SmsServiceProviderId = Guid.Parse(cmbSmsServiceProvider.SelectedValue)
                };

                _groupItemSmsBL.Add(groupItemSmsEntity, out groupItemSmsId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس مناسبت فردیهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtMessageText.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageText Like N'%" +
                                               FarsiToArabic.ToArabic(txtMessageText.Text.Trim()) + "%'";
                if(txtFromDate.Text !="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FromDate ='" +
                                                   (txtFromDate.Text.Trim()) + "'";
                if (txtEndDate.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                                   (txtEndDate.Text.Trim()) + "'";
                if (cmbMessageDraft.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageDraftId ='" +
                                               Guid.Parse(cmbMessageDraft.SelectedValue) + "'";
                if (cmbOccasionType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayTypeId ='" +
                                               int.Parse(cmbOccasionType.SelectedValue) + "'";
                if (cmbOccasionTitle.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayId ='" +
                                               int.Parse(cmbOccasionTitle.SelectedValue) + "'";
                RadTreeView tree = (RadTreeView)cmbItemCategory.Items[0].FindControl("radtreeItemCategory");
                if(!string.IsNullOrEmpty(tree.SelectedValue))
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ItemCategoryId ='" +
                                       Guid.Parse(tree.SelectedValue) + "'";
                if(cmbItem.SelectedIndex>0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ItemId ='" +
                                               Guid.Parse(cmbItem.SelectedValue) + "'";
                if (cmbSmsServiceProvider.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SmsServiceProviderId ='" +
                                               Guid.Parse(cmbSmsServiceProvider.SelectedValue) + "'";
                if (txtSendTime.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SendTime = '" +
                                               txtSendTime.TextWithLiterals + "'";

                if(radTreeGroup.CheckedNodes.Count!=0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and GroupId in (select value from dbo.Fn_CommaStr2Table ('" +
                                  GetValueCheckinTree() + "'))";
                grdGroup_Item_Sms.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdGroup_Item_Sms.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(radTreeGroup.SelectedValue))
                    throw new ItcApplicationErrorManagerException("لطفا از درخت گروه کاربران انتخاب نمایید");
                var groupItemSmsEntity = new Group_Item_SmsEntity()
                {
                    Group_Item_SmsId = Guid.Parse(ViewState["Group_Item_SmsId"].ToString()),
                    FromDate = ItcToDate.ShamsiToMiladi(txtFromDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    ItemId = Guid.Parse(cmbItem.SelectedValue),
                    MessageDraftId = (cmbMessageDraft.SelectedIndex > 0 ? Guid.Parse(cmbMessageDraft.SelectedValue) : (Guid?)null),
                    GroupId = Guid.Parse(radTreeGroup.SelectedNode.Value),
                    MessageText = txtMessageText.Text,
                    SendTime = txtSendTime.TextWithLiterals,
                    SmsServiceProviderId = Guid.Parse(cmbSmsServiceProvider.SelectedValue)
                };
                _groupItemSmsBL.Update(groupItemSmsEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, 0, new Guid(ViewState["Group_Item_SmsId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdGroup_Item_Sms_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["Group_Item_SmsId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetPanelLast();
                    SetDataShow();
                    
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";
                    var groupItemSmsEntity = new Group_Item_SmsEntity()
                    {
                        Group_Item_SmsId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _groupItemSmsBL.Delete(groupItemSmsEntity);
                    var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdGroup_Item_Sms_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdGroup_Item_Sms_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdGroup_Item_Sms_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdGroup_Item_Sms.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
     

    

        protected void radtreeItemCategory_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            SetComboBoxItem(Guid.Parse(e.Node.Value));
        }
        #endregion



        protected void cmbOccasionType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value != "")
            {
                SetComboBoxOccasion(int.Parse(e.Value));
            }
            else
            {
                cmbOccasionTitle.Items.Clear();
                cmbOccasionTitle.Text = "";
                cmbMessageDraft.Items.Clear();
                cmbMessageDraft.Text = "";
            }
        }

        protected void cmbOccasionTitle_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value != "")
            {
                SetComboBoxMessageDraft(int.Parse(e.Value));
            }
            else
            {
                cmbMessageDraft.Items.Clear();
                cmbMessageDraft.Text = "";
            }
        }
        protected void btnRunJob_Click(object sender, EventArgs e)
        {
            try
            {
                Session["GroupSmsDS"] = _messagePackageHistoryBL.GenerateMessagePackageForSmswithItem();
                var navigateUrl = "../ModalForm/GroupSmsMessagePage.aspx";
                var newWindow = new RadWindow
                {
                    NavigateUrl = navigateUrl,
                    Top = Unit.Pixel(22),
                    VisibleOnPageLoad = true,
                    Left = Unit.Pixel(0),
                    DestroyOnClose = true,
                    Modal = true,
                    ReloadOnShow = true,
                };
                RadWindowItemValue.Windows.Add(newWindow);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        protected void radTreeGroup_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            try
            {

                var navigateUrl = "../ModalForm/GroupPersonPage.aspx?WhereClause='" + e.Node.Value + "'";
                var newWindow = new RadWindow
                {
                    NavigateUrl = navigateUrl,
                    Top = Unit.Pixel(22),
                    VisibleOnPageLoad = true,
                    Left = Unit.Pixel(0),
                    DestroyOnClose = true,
                    Modal = true,
                    ReloadOnShow = true,
                };
                RadWindowItemValue.Windows.Add(newWindow);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion


    }
}