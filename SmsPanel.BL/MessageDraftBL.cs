﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/21>
    /// Description: < پیغام پیش نویس>
    /// </summary>
    
//-------------------------------------------------------


	/// <summary>
	/// 
	/// </summary>
	public class MessageDraftBL 
	{	
	  	 private readonly MessageDraftDB _MessageDraftDB;					
			
		public MessageDraftBL()
		{
			_MessageDraftDB = new MessageDraftDB();
		}			
	
		public  void Add(MessageDraftEntity  MessageDraftEntityParam, out Guid MessageDraftId)
		{ 
			_MessageDraftDB.AddMessageDraftDB(MessageDraftEntityParam,out MessageDraftId);			
		}

		public  void Update(MessageDraftEntity  MessageDraftEntityParam)
		{
			_MessageDraftDB.UpdateMessageDraftDB(MessageDraftEntityParam);		
		}

		public  void Delete(MessageDraftEntity  MessageDraftEntityParam)
		{
			 _MessageDraftDB.DeleteMessageDraftDB(MessageDraftEntityParam);			
		}

		public  MessageDraftEntity GetSingleById(MessageDraftEntity  MessageDraftEntityParam)
		{
			MessageDraftEntity o = GetMessageDraftFromMessageDraftDB(
			_MessageDraftDB.GetSingleMessageDraftDB(MessageDraftEntityParam));
			
			return o;
		}

		public  List<MessageDraftEntity> GetAll()
		{
			List<MessageDraftEntity> lst = new List<MessageDraftEntity>();
			//string key = "MessageDraft_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MessageDraftEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetMessageDraftCollectionFromMessageDraftDBList(
				_MessageDraftDB.GetAllMessageDraftDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<MessageDraftEntity> GetAllIsActive()
        {
            List<MessageDraftEntity> lst = new List<MessageDraftEntity>();
            //string key = "MessageDraft_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MessageDraftEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMessageDraftCollectionFromMessageDraftDBList(
            _MessageDraftDB.GetAllMessageDraftIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<MessageDraftEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "MessageDraft_List_Page_" + currentPage ;
			//string countKey = "MessageDraft_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<MessageDraftEntity> lst = new List<MessageDraftEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MessageDraftEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetMessageDraftCollectionFromMessageDraftDBList(
				_MessageDraftDB.GetPageMessageDraftDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  MessageDraftEntity GetMessageDraftFromMessageDraftDB(MessageDraftEntity o)
		{
	if(o == null)
                return null;
			MessageDraftEntity ret = new MessageDraftEntity(o.MessageDraftId ,o.MessageDraftText  ,o.IsActive ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<MessageDraftEntity> GetMessageDraftCollectionFromMessageDraftDBList( List<MessageDraftEntity> lst)
		{
			List<MessageDraftEntity> RetLst = new List<MessageDraftEntity>();
			foreach(MessageDraftEntity o in lst)
			{
				RetLst.Add(GetMessageDraftFromMessageDraftDB(o));
			}
			return RetLst;
			
		}





}

}