﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/22>
    /// Description: <وضیعت پیامک>
    /// </summary>    
	public class MessageStatusBL
	{	
	  	 private readonly MessageStatusDB _MessageStatusDB;					
			
		public MessageStatusBL()
		{
			_MessageStatusDB = new MessageStatusDB();
		}			
	
		public  void Add(MessageStatusEntity  MessageStatusEntityParam,out int MessageStatusId)
		{ 
			_MessageStatusDB.AddMessageStatusDB(MessageStatusEntityParam,out MessageStatusId);			
		}

		public  void Update(MessageStatusEntity  MessageStatusEntityParam)
		{
			_MessageStatusDB.UpdateMessageStatusDB(MessageStatusEntityParam);		
		}

		public  void Delete(MessageStatusEntity  MessageStatusEntityParam)
		{
			 _MessageStatusDB.DeleteMessageStatusDB(MessageStatusEntityParam);			
		}

		public  MessageStatusEntity GetSingleById(MessageStatusEntity  MessageStatusEntityParam)
		{
			MessageStatusEntity o = GetMessageStatusFromMessageStatusDB(
			_MessageStatusDB.GetSingleMessageStatusDB(MessageStatusEntityParam));
			
			return o;
		}

		public  List<MessageStatusEntity> GetAll()
		{
			List<MessageStatusEntity> lst = new List<MessageStatusEntity>();
			//string key = "MessageStatus_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MessageStatusEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetMessageStatusCollectionFromMessageStatusDBList(
				_MessageStatusDB.GetAllMessageStatusDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<MessageStatusEntity> GetAllIsActive()
        {
            List<MessageStatusEntity> lst = new List<MessageStatusEntity>();
            //string key = "MessageStatus_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MessageStatusEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMessageStatusCollectionFromMessageStatusDBList(
            _MessageStatusDB.GetAllMessageStatusIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<MessageStatusEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "MessageStatus_List_Page_" + currentPage ;
			//string countKey = "MessageStatus_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<MessageStatusEntity> lst = new List<MessageStatusEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MessageStatusEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetMessageStatusCollectionFromMessageStatusDBList(
				_MessageStatusDB.GetPageMessageStatusDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  MessageStatusEntity GetMessageStatusFromMessageStatusDB(MessageStatusEntity o)
		{
	if(o == null)
                return null;
			MessageStatusEntity ret = new MessageStatusEntity(o.MessageStatusId,o.MessageStatusCode,o.SmsServiceProviderId,o.MessageStatusEnglishTitle ,o.MessageStatusPersianTitle,o.IsActive ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<MessageStatusEntity> GetMessageStatusCollectionFromMessageStatusDBList( List<MessageStatusEntity> lst)
		{
			List<MessageStatusEntity> RetLst = new List<MessageStatusEntity>();
			foreach(MessageStatusEntity o in lst)
			{
				RetLst.Add(GetMessageStatusFromMessageStatusDB(o));
			}
			return RetLst;
			
		}
        public List<MessageStatusEntity> GetMessageStatusDBCollectionBySmsServiceProvider(MessageStatusEntity MessageStatusEntityParam)
        {
            return GetMessageStatusCollectionFromMessageStatusDBList(_MessageStatusDB.GetMessageStatusDBCollectionBySmsServiceProviderDB(MessageStatusEntityParam));
        }	
		
	}

}



