﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <آیتم فرد>
    /// </summary>

	public class PersonItemBL 
	{	
	  	 private readonly PersonItemDB _PersonItemDB;					
			
		public PersonItemBL()
		{
			_PersonItemDB = new PersonItemDB();
		}			
	
		public  void Add(PersonItemEntity  PersonItemEntityParam, out Guid PersonItemId)
		{ 
			_PersonItemDB.AddPersonItemDB(PersonItemEntityParam,out PersonItemId);			
		}

		public  void Update(PersonItemEntity  PersonItemEntityParam)
		{
			_PersonItemDB.UpdatePersonItemDB(PersonItemEntityParam);		
		}

		public  void Delete(PersonItemEntity  PersonItemEntityParam)
		{
			 _PersonItemDB.DeletePersonItemDB(PersonItemEntityParam);			
		}

		public  PersonItemEntity GetSingleById(PersonItemEntity  PersonItemEntityParam)
		{
			PersonItemEntity o = GetPersonItemFromPersonItemDB(
			_PersonItemDB.GetSinglePersonItemDB(PersonItemEntityParam));
			
			return o;
		}

		public  List<PersonItemEntity> GetAll()
		{
			List<PersonItemEntity> lst = new List<PersonItemEntity>();
			//string key = "PersonItem_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<PersonItemEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetPersonItemCollectionFromPersonItemDBList(
				_PersonItemDB.GetAllPersonItemDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<PersonItemEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "PersonItem_List_Page_" + currentPage ;
			//string countKey = "PersonItem_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<PersonItemEntity> lst = new List<PersonItemEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<PersonItemEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetPersonItemCollectionFromPersonItemDBList(
				_PersonItemDB.GetPagePersonItemDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  PersonItemEntity GetPersonItemFromPersonItemDB(PersonItemEntity o)
		{
	if(o == null)
                return null;
			PersonItemEntity ret = new PersonItemEntity(o.PersonItemId ,o.PersonId ,o.ItemId ,o.Date ,o.MonthId ,o.DayNo ,o.IsActive ,o.CreationDate ,o.ModificationDate,o.ItemCategoryId,o.FullName );
			return ret;
		}
		
		private  List<PersonItemEntity> GetPersonItemCollectionFromPersonItemDBList( List<PersonItemEntity> lst)
		{
			List<PersonItemEntity> RetLst = new List<PersonItemEntity>();
			foreach(PersonItemEntity o in lst)
			{
				RetLst.Add(GetPersonItemFromPersonItemDB(o));
			}
			return RetLst;
			
		}


        public List<PersonItemEntity> GetPersonItemCollectionByPerson(PersonItemEntity personItemEntityParam)
{
    return GetPersonItemCollectionFromPersonItemDBList(_PersonItemDB.GetPersonItemDBCollectionByPersonDB(personItemEntityParam));
}


public List<PersonItemEntity> GetPersonItemCollectionByItem(PersonItemEntity personItemEntityParam)
{
    return GetPersonItemCollectionFromPersonItemDBList(
        _PersonItemDB.GetPersonItemDBCollectionByItemDB(personItemEntityParam));
}




}

}