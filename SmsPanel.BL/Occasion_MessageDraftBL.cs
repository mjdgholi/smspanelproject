﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/22>
    /// Description: < ارتباطی بین مناسبت و پیغام پیش نویس>
    /// </summary>

    public class Occasion_MessageDraftBL
    {
        private readonly Occasion_MessageDraftDB _Occasion_MessageDraftDB;

        public Occasion_MessageDraftBL()
        {
            _Occasion_MessageDraftDB = new Occasion_MessageDraftDB();
        }

        public void Add(Occasion_MessageDraftEntity Occasion_MessageDraftEntityParam, out Guid Occasion_MessageDraftId)
        {
            _Occasion_MessageDraftDB.AddOccasion_MessageDraftDB(Occasion_MessageDraftEntityParam,
                out Occasion_MessageDraftId);
        }

        public void Update(Occasion_MessageDraftEntity Occasion_MessageDraftEntityParam)
        {
            _Occasion_MessageDraftDB.UpdateOccasion_MessageDraftDB(Occasion_MessageDraftEntityParam);
        }

        public void Delete(Occasion_MessageDraftEntity Occasion_MessageDraftEntityParam)
        {
            _Occasion_MessageDraftDB.DeleteOccasion_MessageDraftDB(Occasion_MessageDraftEntityParam);
        }

        public Occasion_MessageDraftEntity GetSingleById(Occasion_MessageDraftEntity Occasion_MessageDraftEntityParam)
        {
            Occasion_MessageDraftEntity o = GetOccasion_MessageDraftFromOccasion_MessageDraftDB(
                _Occasion_MessageDraftDB.GetSingleOccasion_MessageDraftDB(Occasion_MessageDraftEntityParam));

            return o;
        }

        public List<Occasion_MessageDraftEntity> GetAll()
        {
            List<Occasion_MessageDraftEntity> lst = new List<Occasion_MessageDraftEntity>();
            //string key = "Occasion_MessageDraft_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<Occasion_MessageDraftEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOccasion_MessageDraftCollectionFromOccasion_MessageDraftDBList(
                _Occasion_MessageDraftDB.GetAllOccasion_MessageDraftDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<Occasion_MessageDraftEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "Occasion_MessageDraft_List_Page_" + currentPage ;
            //string countKey = "Occasion_MessageDraft_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<Occasion_MessageDraftEntity> lst = new List<Occasion_MessageDraftEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<Occasion_MessageDraftEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetOccasion_MessageDraftCollectionFromOccasion_MessageDraftDBList(
                _Occasion_MessageDraftDB.GetPageOccasion_MessageDraftDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private Occasion_MessageDraftEntity GetOccasion_MessageDraftFromOccasion_MessageDraftDB(
            Occasion_MessageDraftEntity o)
        {
            if (o == null)
                return null;
            Occasion_MessageDraftEntity ret = new Occasion_MessageDraftEntity(o.Occasion_MessageDraftId, o.OccasionDayId,
                o.MessageDraftId, o.IsActive, o.CreationDate, o.ModificationDate,o.MessageDraftText);
            return ret;
        }

        private List<Occasion_MessageDraftEntity> GetOccasion_MessageDraftCollectionFromOccasion_MessageDraftDBList(
            List<Occasion_MessageDraftEntity> lst)
        {
            List<Occasion_MessageDraftEntity> RetLst = new List<Occasion_MessageDraftEntity>();
            foreach (Occasion_MessageDraftEntity o in lst)
            {
                RetLst.Add(GetOccasion_MessageDraftFromOccasion_MessageDraftDB(o));
            }
            return RetLst;

        }


        public List<Occasion_MessageDraftEntity> GetOccasion_MessageDraftCollectionByMessageDraft(
            Occasion_MessageDraftEntity occasionMessageDraftEntityParam)
        {
            return
                GetOccasion_MessageDraftCollectionFromOccasion_MessageDraftDBList(
                    _Occasion_MessageDraftDB.GetOccasion_MessageDraftDBCollectionByMessageDraftDB(
                        occasionMessageDraftEntityParam));
        }

        public List<Occasion_MessageDraftEntity> GetOccasion_MessageDraftCollectionByOccasion(
            Occasion_MessageDraftEntity occasionMessageDraftEntityParam)
        {
            return
                GetOccasion_MessageDraftCollectionFromOccasion_MessageDraftDBList(
                    _Occasion_MessageDraftDB.GetOccasion_MessageDraftDBCollectionByOccasionDB(
                        occasionMessageDraftEntityParam));
        }



    }

}