﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/21>
    /// Description: <نوع مناسبت>
    /// </summary>

    public class OccasionTypeBL
    {
        private readonly OccasionTypeDB _OccasionTypeDB;

        public OccasionTypeBL()
        {
            _OccasionTypeDB = new OccasionTypeDB();
        }

        public void Add(OccasionTypeEntity OccasionTypeEntityParam, out Guid OccasionTypeId)
        {
            _OccasionTypeDB.AddOccasionTypeDB(OccasionTypeEntityParam, out OccasionTypeId);
        }

        public void Update(OccasionTypeEntity OccasionTypeEntityParam)
        {
            _OccasionTypeDB.UpdateOccasionTypeDB(OccasionTypeEntityParam);
        }

        public void Delete(OccasionTypeEntity OccasionTypeEntityParam)
        {
            _OccasionTypeDB.DeleteOccasionTypeDB(OccasionTypeEntityParam);
        }

        public OccasionTypeEntity GetSingleById(OccasionTypeEntity OccasionTypeEntityParam)
        {
            OccasionTypeEntity o = GetOccasionTypeFromOccasionTypeDB(
                _OccasionTypeDB.GetSingleOccasionTypeDB(OccasionTypeEntityParam));

            return o;
        }

        public List<OccasionTypeEntity> GetAll()
        {
            List<OccasionTypeEntity> lst = new List<OccasionTypeEntity>();
            //string key = "OccasionType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OccasionTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOccasionTypeCollectionFromOccasionTypeDBList(
                _OccasionTypeDB.GetAllOccasionTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<OccasionTypeEntity> GetAllIsActive()
        {
            List<OccasionTypeEntity> lst = new List<OccasionTypeEntity>();
            //string key = "OccasionType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OccasionTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOccasionTypeCollectionFromOccasionTypeDBList(
                _OccasionTypeDB.GetAllOccasionTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<OccasionTypeEntity> GetAllPaging(int currentPage, int pageSize,
            string sortExpression, out int count, string whereClause)
        {
            //string key = "OccasionType_List_Page_" + currentPage ;
            //string countKey = "OccasionType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<OccasionTypeEntity> lst = new List<OccasionTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OccasionTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetOccasionTypeCollectionFromOccasionTypeDBList(
                _OccasionTypeDB.GetPageOccasionTypeDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private OccasionTypeEntity GetOccasionTypeFromOccasionTypeDB(OccasionTypeEntity o)
        {
            if (o == null)
                return null;
            OccasionTypeEntity ret = new OccasionTypeEntity(o.OccasionTypeId, o.OccasionTypeTitle, o.IsActive,
                o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<OccasionTypeEntity> GetOccasionTypeCollectionFromOccasionTypeDBList(List<OccasionTypeEntity> lst)
        {
            List<OccasionTypeEntity> RetLst = new List<OccasionTypeEntity>();
            foreach (OccasionTypeEntity o in lst)
            {
                RetLst.Add(GetOccasionTypeFromOccasionTypeDB(o));
            }
            return RetLst;

        }


    }

}
