﻿using System;
using System.Collections;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    /// Description: < گروه بندی آیتم ها>
    /// </summary>

	public class ItemCategoryBL 
	{	
	  	 private readonly ItemCategoryDB _ItemCategoryDB;					
			
		public ItemCategoryBL()
		{
			_ItemCategoryDB = new ItemCategoryDB();
		}			
	
		public  void Add(ItemCategoryEntity  ItemCategoryEntityParam, out Guid ItemCategoryId)
		{ 
			_ItemCategoryDB.AddItemCategoryDB(ItemCategoryEntityParam,out ItemCategoryId);			
		}

		public  void Update(ItemCategoryEntity  ItemCategoryEntityParam)
		{
			_ItemCategoryDB.UpdateItemCategoryDB(ItemCategoryEntityParam);		
		}

		public  void Delete(ItemCategoryEntity  ItemCategoryEntityParam)
		{
			 _ItemCategoryDB.DeleteItemCategoryDB(ItemCategoryEntityParam);			
		}

		public  ItemCategoryEntity GetSingleById(ItemCategoryEntity  ItemCategoryEntityParam)
		{
			ItemCategoryEntity o = GetItemCategoryFromItemCategoryDB(
			_ItemCategoryDB.GetSingleItemCategoryDB(ItemCategoryEntityParam));
			
			return o;
		}

		public  List<ItemCategoryEntity> GetAll()
		{
			List<ItemCategoryEntity> lst = new List<ItemCategoryEntity>();
			//string key = "ItemCategory_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ItemCategoryEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetItemCategoryCollectionFromItemCategoryDBList(
				_ItemCategoryDB.GetAllItemCategoryDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<ItemCategoryEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "ItemCategory_List_Page_" + currentPage ;
			//string countKey = "ItemCategory_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ItemCategoryEntity> lst = new List<ItemCategoryEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ItemCategoryEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetItemCategoryCollectionFromItemCategoryDBList(
				_ItemCategoryDB.GetPageItemCategoryDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ItemCategoryEntity GetItemCategoryFromItemCategoryDB(ItemCategoryEntity o)
		{
	if(o == null)
                return null;
			ItemCategoryEntity ret = new ItemCategoryEntity(o.ItemCategoryId ,o.OwnerId ,o.ItemGroupTitle ,o.ShowOrder ,o.IsActive ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<ItemCategoryEntity> GetItemCategoryCollectionFromItemCategoryDBList( List<ItemCategoryEntity> lst)
		{
			List<ItemCategoryEntity> RetLst = new List<ItemCategoryEntity>();
			foreach(ItemCategoryEntity o in lst)
			{
				RetLst.Add(GetItemCategoryFromItemCategoryDB(o));
			}
			return RetLst;
			
		}


        public IList<ItemCategoryEntity> GetAllNodes(ItemCategoryEntity ItemCategoryEntityParam)
        {
          return  _ItemCategoryDB.GetAllNodesDB(ItemCategoryEntityParam);
        }
	}




}

