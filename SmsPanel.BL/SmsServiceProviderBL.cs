﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/19>
    /// Description: < مهیا کننده سرویس>
    /// </summary>


	public class SmsServiceProviderBL 
	{	
	  	 private readonly SmsServiceProviderDB _SmsServiceProviderDB;					
			
		public SmsServiceProviderBL()
		{
			_SmsServiceProviderDB = new SmsServiceProviderDB();
		}			
	
		public  void Add(SmsServiceProviderEntity  SmsServiceProviderEntityParam, out Guid SmsServiceProviderId)
		{ 
			_SmsServiceProviderDB.AddSmsServiceProviderDB(SmsServiceProviderEntityParam,out SmsServiceProviderId);			
		}

		public  void Update(SmsServiceProviderEntity  SmsServiceProviderEntityParam)
		{
			_SmsServiceProviderDB.UpdateSmsServiceProviderDB(SmsServiceProviderEntityParam);		
		}

		public  void Delete(SmsServiceProviderEntity  SmsServiceProviderEntityParam)
		{
			 _SmsServiceProviderDB.DeleteSmsServiceProviderDB(SmsServiceProviderEntityParam);			
		}

		public  SmsServiceProviderEntity GetSingleById(SmsServiceProviderEntity  SmsServiceProviderEntityParam)
		{
			SmsServiceProviderEntity o = GetSmsServiceProviderFromSmsServiceProviderDB(
			_SmsServiceProviderDB.GetSingleSmsServiceProviderDB(SmsServiceProviderEntityParam));
			
			return o;
		}

		public  List<SmsServiceProviderEntity> GetAll()
		{
			List<SmsServiceProviderEntity> lst = new List<SmsServiceProviderEntity>();
			//string key = "SmsServiceProvider_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SmsServiceProviderEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetSmsServiceProviderCollectionFromSmsServiceProviderDBList(
				_SmsServiceProviderDB.GetAllSmsServiceProviderDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<SmsServiceProviderEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "SmsServiceProvider_List_Page_" + currentPage ;
			//string countKey = "SmsServiceProvider_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<SmsServiceProviderEntity> lst = new List<SmsServiceProviderEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SmsServiceProviderEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetSmsServiceProviderCollectionFromSmsServiceProviderDBList(
				_SmsServiceProviderDB.GetPageSmsServiceProviderDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  SmsServiceProviderEntity GetSmsServiceProviderFromSmsServiceProviderDB(SmsServiceProviderEntity o)
		{
	if(o == null)
                return null;
			SmsServiceProviderEntity ret = new SmsServiceProviderEntity(o.SmsServiceProviderId ,o.SmsServiceProviderCode ,o.SmsServiceProviderName ,o.SmsServiceProviderPersianName ,o.IsDefault ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<SmsServiceProviderEntity> GetSmsServiceProviderCollectionFromSmsServiceProviderDBList( List<SmsServiceProviderEntity> lst)
		{
			List<SmsServiceProviderEntity> RetLst = new List<SmsServiceProviderEntity>();
			foreach(SmsServiceProviderEntity o in lst)
			{
				RetLst.Add(GetSmsServiceProviderFromSmsServiceProviderDB(o));
			}
			return RetLst;
			
		} 
				
		
	}


}

