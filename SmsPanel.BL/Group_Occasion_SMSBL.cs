﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/17>
    /// Description: <پیامک با مناسبت>
    /// </summary>

	public class Group_Occasion_SMSBL 
	{	
	  	 private readonly Group_Occasion_SMSDB _Group_Occasion_SMSDB;					
			
		public Group_Occasion_SMSBL()
		{
			_Group_Occasion_SMSDB = new Group_Occasion_SMSDB();
		}			
	
		public  void Add(Group_Occasion_SMSEnity  Group_Occasion_SMSEnityParam, out Guid Group_Occasion_SMSId)
		{ 
			_Group_Occasion_SMSDB.AddGroup_Occasion_SMSDB(Group_Occasion_SMSEnityParam,out Group_Occasion_SMSId);			
		}

		public  void Update(Group_Occasion_SMSEnity  Group_Occasion_SMSEnityParam)
		{
			_Group_Occasion_SMSDB.UpdateGroup_Occasion_SMSDB(Group_Occasion_SMSEnityParam);		
		}

		public  void Delete(Group_Occasion_SMSEnity  Group_Occasion_SMSEnityParam)
		{
			 _Group_Occasion_SMSDB.DeleteGroup_Occasion_SMSDB(Group_Occasion_SMSEnityParam);			
		}

		public  Group_Occasion_SMSEnity GetSingleById(Group_Occasion_SMSEnity  Group_Occasion_SMSEnityParam)
		{
			Group_Occasion_SMSEnity o = GetGroup_Occasion_SMSFromGroup_Occasion_SMSDB(
			_Group_Occasion_SMSDB.GetSingleGroup_Occasion_SMSDB(Group_Occasion_SMSEnityParam));
			
			return o;
		}

		public  List<Group_Occasion_SMSEnity> GetAll()
		{
			List<Group_Occasion_SMSEnity> lst = new List<Group_Occasion_SMSEnity>();
			//string key = "Group_Occasion_SMS_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<Group_Occasion_SMSEnity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetGroup_Occasion_SMSCollectionFromGroup_Occasion_SMSDBList(
				_Group_Occasion_SMSDB.GetAllGroup_Occasion_SMSDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<Group_Occasion_SMSEnity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Group_Occasion_SMS_List_Page_" + currentPage ;
			//string countKey = "Group_Occasion_SMS_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<Group_Occasion_SMSEnity> lst = new List<Group_Occasion_SMSEnity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<Group_Occasion_SMSEnity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetGroup_Occasion_SMSCollectionFromGroup_Occasion_SMSDBList(
				_Group_Occasion_SMSDB.GetPageGroup_Occasion_SMSDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  Group_Occasion_SMSEnity GetGroup_Occasion_SMSFromGroup_Occasion_SMSDB(Group_Occasion_SMSEnity o)
		{
	if(o == null)
                return null;
			Group_Occasion_SMSEnity ret = new Group_Occasion_SMSEnity(o.Group_Occasion_SMSId,o.SmsServiceProviderId ,o.GroupId ,o.OccasionDayId ,o.MessageDraftId ,o.MessageText ,o.SendTime,o.DifferenceDay ,o.FromDate ,o.EndDate ,o.CreationDate ,o.ModificationDate,o.OccasionDayTypeId );
			return ret;
		}
		
		private  List<Group_Occasion_SMSEnity> GetGroup_Occasion_SMSCollectionFromGroup_Occasion_SMSDBList( List<Group_Occasion_SMSEnity> lst)
		{
			List<Group_Occasion_SMSEnity> RetLst = new List<Group_Occasion_SMSEnity>();
			foreach(Group_Occasion_SMSEnity o in lst)
			{
				RetLst.Add(GetGroup_Occasion_SMSFromGroup_Occasion_SMSDB(o));
			}
			return RetLst;
			
		}



        public List<Group_Occasion_SMSEnity> GetGroup_Occasion_SMSCollectionByOccasionDay(Group_Occasion_SMSEnity Group_Occasion_SMSEnityParam)
{
    return GetGroup_Occasion_SMSCollectionFromGroup_Occasion_SMSDBList(_Group_Occasion_SMSDB.GetGroup_Occasion_SMSDBCollectionByOccasionDayDB(Group_Occasion_SMSEnityParam));
}

        public List<Group_Occasion_SMSEnity> GetGroup_Occasion_SMSCollectionByGroup(Group_Occasion_SMSEnity Group_Occasion_SMSEnityParam)
{
    return GetGroup_Occasion_SMSCollectionFromGroup_Occasion_SMSDBList(_Group_Occasion_SMSDB.GetGroup_Occasion_SMSDBCollectionByGroupDB(Group_Occasion_SMSEnityParam));
}

        public List<Group_Occasion_SMSEnity> GetGroup_Occasion_SMSCollectionByMessageDraft(Group_Occasion_SMSEnity Group_Occasion_SMSEnityParam)
{
    return GetGroup_Occasion_SMSCollectionFromGroup_Occasion_SMSDBList(_Group_Occasion_SMSDB.GetGroup_Occasion_SMSDBCollectionByMessageDraftDB(Group_Occasion_SMSEnityParam));
}





}

}