﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <آیتم ها>
    /// </summary>

    public class ItemBL
    {
        private readonly ItemDB _ItemDB;

        public ItemBL()
        {
            _ItemDB = new ItemDB();
        }

        public void Add(ItemEntity ItemEntityParam, out Guid ItemId)
        {
            _ItemDB.AddItemDB(ItemEntityParam, out ItemId);
        }

        public void Update(ItemEntity ItemEntityParam)
        {
            _ItemDB.UpdateItemDB(ItemEntityParam);
        }

        public void Delete(ItemEntity ItemEntityParam)
        {
            _ItemDB.DeleteItemDB(ItemEntityParam);
        }

        public ItemEntity GetSingleById(ItemEntity ItemEntityParam)
        {
            ItemEntity o = GetItemFromItemDB(
                _ItemDB.GetSingleItemDB(ItemEntityParam));

            return o;
        }

        public List<ItemEntity> GetAll()
        {
            List<ItemEntity> lst = new List<ItemEntity>();
            //string key = "Item_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ItemEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetItemCollectionFromItemDBList(
                _ItemDB.GetAllItemDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ItemEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "Item_List_Page_" + currentPage ;
            //string countKey = "Item_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ItemEntity> lst = new List<ItemEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ItemEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetItemCollectionFromItemDBList(
                _ItemDB.GetPageItemDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ItemEntity GetItemFromItemDB(ItemEntity o)
        {
            if (o == null)
                return null;
            ItemEntity ret = new ItemEntity(o.ItemId, o.ItemCategoryId, o.ItemTitle, o.Priority, o.IsActive,
                o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<ItemEntity> GetItemCollectionFromItemDBList(List<ItemEntity> lst)
        {
            List<ItemEntity> RetLst = new List<ItemEntity>();
            foreach (ItemEntity o in lst)
            {
                RetLst.Add(GetItemFromItemDB(o));
            }
            return RetLst;

        }




        public List<ItemEntity> GetItemCollectionByItemCategory(ItemEntity ItemEntityParam)
        {
            return GetItemCollectionFromItemDBList(_ItemDB.GetItemDBCollectionByItemCategoryDB(ItemEntityParam));
        }

    }

}