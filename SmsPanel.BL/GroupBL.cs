﻿using System;
using System.Collections.Generic;
using System.Data;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{

    //-----93-04-18
    //----- majid Gholibeygian
    /// <summary>
    /// 
    /// </summary>
    public class GroupBL
    {
        private readonly GroupDB _GroupDB;

        public GroupBL()
        {
            _GroupDB = new GroupDB();
        }

        public void Add(GroupEntity GroupEntityParam, out Guid GroupId)
        {
            _GroupDB.AddGroupDB(GroupEntityParam, out GroupId);
        }

        public void Update(GroupEntity GroupEntityParam)
        {
            _GroupDB.UpdateGroupDB(GroupEntityParam);
        }

        public void Delete(GroupEntity GroupEntityParam)
        {
            _GroupDB.DeleteGroupDB(GroupEntityParam);
        }

        public GroupEntity GetSingleById(GroupEntity GroupEntityParam)
        {
            GroupEntity o = GetGroupFromGroupDB(
                _GroupDB.GetSingleGroupDB(GroupEntityParam));

            return o;
        }

        public List<GroupEntity> GetAll()
        {
            List<GroupEntity> lst = new List<GroupEntity>();
            //string key = "Group_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GroupEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetGroupCollectionFromGroupDBList(
                _GroupDB.GetAllGroupDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<GroupEntity> GetAllPaging(int currentPage, int pageSize,string sortExpression, out int count, string whereClause)
        {
            //string key = "Group_List_Page_" + currentPage ;
            //string countKey = "Group_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<GroupEntity> lst = new List<GroupEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GroupEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetGroupCollectionFromGroupDBList(
                _GroupDB.GetPageGroupDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private GroupEntity GetGroupFromGroupDB(GroupEntity o)
        {
            if (o == null)
                return null;
            GroupEntity ret = new GroupEntity(o.GroupId, o.OwnerId, o.GroupTitle, o.ChildNo, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<GroupEntity> GetGroupCollectionFromGroupDBList(List<GroupEntity> lst)
        {
            List<GroupEntity> RetLst = new List<GroupEntity>();
            foreach (GroupEntity o in lst)
            {
                RetLst.Add(GetGroupFromGroupDB(o));
            }
            return RetLst;

        }

        public void DeleteWithAllChild(Guid groupId)
        {
            _GroupDB.DeleteWithAllChild(groupId);
            

        }

        public DataTable GetParentNodes(Guid groupId)
        {
          return  _GroupDB.GetParentNodes(groupId);
        }

        public DataTable GetAllNodes(string groupTitle)
        {
            return _GroupDB.GetAllNodes(groupTitle);
        }
    }

}
