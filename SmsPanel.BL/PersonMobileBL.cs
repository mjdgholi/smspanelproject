﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/22>
    /// Description: <شماره موبایل شخص>
    /// </summary>

    public class PersonMobileBL
    {
        private readonly PersonMobileDB _PersonMobileDB;

        public PersonMobileBL()
        {
            _PersonMobileDB = new PersonMobileDB();
        }

        public void Add(PersonMobileEntity PersonMobileEntityParam, out Guid PersonMoblieId)
        {
            _PersonMobileDB.AddPersonMobileDB(PersonMobileEntityParam, out PersonMoblieId);
        }

        public void Update(PersonMobileEntity PersonMobileEntityParam)
        {
            _PersonMobileDB.UpdatePersonMobileDB(PersonMobileEntityParam);
        }

        public void Delete(PersonMobileEntity PersonMobileEntityParam)
        {
            _PersonMobileDB.DeletePersonMobileDB(PersonMobileEntityParam);
        }

        public PersonMobileEntity GetSingleById(PersonMobileEntity PersonMobileEntityParam)
        {
            PersonMobileEntity o = GetPersonMobileFromPersonMobileDB(
                _PersonMobileDB.GetSinglePersonMobileDB(PersonMobileEntityParam));

            return o;
        }

        public List<PersonMobileEntity> GetAll()
        {
            List<PersonMobileEntity> lst = new List<PersonMobileEntity>();
            //string key = "PersonMobile_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<PersonMobileEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetPersonMobileCollectionFromPersonMobileDBList(
                _PersonMobileDB.GetAllPersonMobileDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<PersonMobileEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "PersonMobile_List_Page_" + currentPage ;
            //string countKey = "PersonMobile_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<PersonMobileEntity> lst = new List<PersonMobileEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<PersonMobileEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetPersonMobileCollectionFromPersonMobileDBList(
                _PersonMobileDB.GetPagePersonMobileDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private PersonMobileEntity GetPersonMobileFromPersonMobileDB(PersonMobileEntity o)
        {
            if (o == null)
                return null;
            PersonMobileEntity ret = new PersonMobileEntity(o.PersonMoblieId, o.PersonId, o.MobileNo, o.IsDefault,
                o.IsActive, o.CreationDate, o.ModificationDate,o.FullName);
            return ret;
        }

        private List<PersonMobileEntity> GetPersonMobileCollectionFromPersonMobileDBList(List<PersonMobileEntity> lst)
        {
            List<PersonMobileEntity> RetLst = new List<PersonMobileEntity>();
            foreach (PersonMobileEntity o in lst)
            {
                RetLst.Add(GetPersonMobileFromPersonMobileDB(o));
            }
            return RetLst;

        }


        public  List<PersonMobileEntity> GetPersonMobileCollectionByPerson(PersonMobileEntity personMobileEntityParam)
        {
            return
                GetPersonMobileCollectionFromPersonMobileDBList(
                    _PersonMobileDB.GetPersonMobileDBCollectionByPersonDB(personMobileEntityParam));
        }




    }

}
