﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{

    //-----93-04-18
    //----- majid Gholibeygian

    public class GroupPersonBL
    {
        private readonly GroupPersonDB _GroupPersonDB;

        public GroupPersonBL()
        {
            _GroupPersonDB = new GroupPersonDB();
        }

        public void Add(GroupPersonEntity GroupPersonEntityParam)
        {
            _GroupPersonDB.AddGroupPersonDB(GroupPersonEntityParam);
        }

        public void Update(GroupPersonEntity GroupPersonEntityParam)
        {
            _GroupPersonDB.UpdateGroupPersonDB(GroupPersonEntityParam);
        }

        public void Delete(GroupPersonEntity GroupPersonEntityParam)
        {
            _GroupPersonDB.DeleteGroupPersonDB(GroupPersonEntityParam);
        }

        public GroupPersonEntity GetSingleById(GroupPersonEntity GroupPersonEntityParam)
        {
            GroupPersonEntity o = GetGroupPersonFromGroupPersonDB(
                _GroupPersonDB.GetSingleGroupPersonDB(GroupPersonEntityParam));

            return o;
        }

        public List<GroupPersonEntity> GetAll()
        {
            List<GroupPersonEntity> lst = new List<GroupPersonEntity>();
            //string key = "GroupPerson_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GroupPersonEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetGroupPersonCollectionFromGroupPersonDBList(
                _GroupPersonDB.GetAllGroupPersonDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<GroupPersonEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "GroupPerson_List_Page_" + currentPage ;
            //string countKey = "GroupPerson_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<GroupPersonEntity> lst = new List<GroupPersonEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GroupPersonEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetGroupPersonCollectionFromGroupPersonDBList(
                _GroupPersonDB.GetPageGroupPersonDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private GroupPersonEntity GetGroupPersonFromGroupPersonDB(GroupPersonEntity o)
        {
            if (o == null)
                return null;
            GroupPersonEntity ret = new GroupPersonEntity(o.GroupPersonId, o.GroupId,o.PersonMoblieId, o.PersonId, o.CreationDate, o.ModificationDate,o.PersonIdStr,o.PersonMoblieIdStr,o.FullName);
            return ret;
        }

        private List<GroupPersonEntity> GetGroupPersonCollectionFromGroupPersonDBList(List<GroupPersonEntity> lst)
        {
            List<GroupPersonEntity> RetLst = new List<GroupPersonEntity>();
            foreach (GroupPersonEntity o in lst)
            {
                RetLst.Add(GetGroupPersonFromGroupPersonDB(o));
            }
            return RetLst;

        }

        public List<GroupPersonEntity> GetGroupPersonCollectionByPerson(Guid PersonId)
        {
            return GetGroupPersonCollectionFromGroupPersonDBList(_GroupPersonDB.GetGroupPersonDBCollectionByPersonDB(PersonId));
        }

        public List<GroupPersonEntity> GetGroupPersonCollectionByGroup(Guid GroupId)
        {
            return GetGroupPersonCollectionFromGroupPersonDBList(_GroupPersonDB.GetGroupPersonDBCollectionByGroupDB(GroupId));
        }

    }
}