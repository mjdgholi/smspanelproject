﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <پیامک با آیتم>
    /// </summary>

	public class Group_Item_SmsBL 
	{	
	  	 private readonly Group_Item_SmsDB _Group_Item_SmsDB;					
			
		public Group_Item_SmsBL()
		{
			_Group_Item_SmsDB = new Group_Item_SmsDB();
		}			
	
		public  void Add(Group_Item_SmsEntity  Group_Item_SmsEntityParam, out Guid Group_Item_SmsId)
		{ 
			_Group_Item_SmsDB.AddGroup_Item_SmsDB(Group_Item_SmsEntityParam,out Group_Item_SmsId);			
		}

		public  void Update(Group_Item_SmsEntity  Group_Item_SmsEntityParam)
		{
			_Group_Item_SmsDB.UpdateGroup_Item_SmsDB(Group_Item_SmsEntityParam);		
		}

		public  void Delete(Group_Item_SmsEntity  Group_Item_SmsEntityParam)
		{
			 _Group_Item_SmsDB.DeleteGroup_Item_SmsDB(Group_Item_SmsEntityParam);			
		}

		public  Group_Item_SmsEntity GetSingleById(Group_Item_SmsEntity  Group_Item_SmsEntityParam)
		{
			Group_Item_SmsEntity o = GetGroup_Item_SmsFromGroup_Item_SmsDB(
			_Group_Item_SmsDB.GetSingleGroup_Item_SmsDB(Group_Item_SmsEntityParam));
			
			return o;
		}

		public  List<Group_Item_SmsEntity> GetAll()
		{
			List<Group_Item_SmsEntity> lst = new List<Group_Item_SmsEntity>();
			//string key = "Group_Item_Sms_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<Group_Item_SmsEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetGroup_Item_SmsCollectionFromGroup_Item_SmsDBList(
				_Group_Item_SmsDB.GetAllGroup_Item_SmsDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<Group_Item_SmsEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Group_Item_Sms_List_Page_" + currentPage ;
			//string countKey = "Group_Item_Sms_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<Group_Item_SmsEntity> lst = new List<Group_Item_SmsEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<Group_Item_SmsEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetGroup_Item_SmsCollectionFromGroup_Item_SmsDBList(
				_Group_Item_SmsDB.GetPageGroup_Item_SmsDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  Group_Item_SmsEntity GetGroup_Item_SmsFromGroup_Item_SmsDB(Group_Item_SmsEntity o)
		{
	if(o == null)
                return null;
			Group_Item_SmsEntity ret = new Group_Item_SmsEntity(o.Group_Item_SmsId ,o.GroupId ,o.ItemId ,o.MessageDraftId ,o.MessageText ,o.SendTime ,o.FromDate ,o.EndDate ,o.CreationDate ,o.ModificationDate,o.ItemCategoryId,o.OccasionDayTypeId,o.OccasionDayId,o.SmsServiceProviderId );
			return ret;
		}
		
		private  List<Group_Item_SmsEntity> GetGroup_Item_SmsCollectionFromGroup_Item_SmsDBList( List<Group_Item_SmsEntity> lst)
		{
			List<Group_Item_SmsEntity> RetLst = new List<Group_Item_SmsEntity>();
			foreach(Group_Item_SmsEntity o in lst)
			{
				RetLst.Add(GetGroup_Item_SmsFromGroup_Item_SmsDB(o));
			}
			return RetLst;
			
		}



        public List<Group_Item_SmsEntity> GetGroup_Item_SmsCollectionByGroup(Group_Item_SmsEntity groupItemSmsEntityParam)
{
    return GetGroup_Item_SmsCollectionFromGroup_Item_SmsDBList(_Group_Item_SmsDB.GetGroup_Item_SmsDBCollectionByGroupDB(groupItemSmsEntityParam));
}

        public List<Group_Item_SmsEntity> GetGroup_Item_SmsCollectionByItem(Group_Item_SmsEntity groupItemSmsEntityParam)
{
    return GetGroup_Item_SmsCollectionFromGroup_Item_SmsDBList(_Group_Item_SmsDB.GetGroup_Item_SmsDBCollectionByItemDB(groupItemSmsEntityParam));
}

        public List<Group_Item_SmsEntity> GetGroup_Item_SmsCollectionByMessageDraft(Group_Item_SmsEntity groupItemSmsEntityParam)
{
    return GetGroup_Item_SmsCollectionFromGroup_Item_SmsDBList(_Group_Item_SmsDB.GetGroup_Item_SmsDBCollectionByMessageDraftDB(groupItemSmsEntityParam));
}





}

}