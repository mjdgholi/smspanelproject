﻿using System;
using System.Collections.Generic;
using System.Data;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.DAL;
using Intranet.DesktopModules.SmsPanelProject.SmsPanel.Entity;

namespace Intranet.DesktopModules.SmsPanelProject.SmsPanel.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/23>
    /// Description: < بسته پیامک>
    /// </summary>
    /// 

    public class MessagePackageHistoryBL
    {
        private readonly MessagePackageHistoryDB _MessagePackageHistoryDB;

        public MessagePackageHistoryBL()
        {
            _MessagePackageHistoryDB = new MessagePackageHistoryDB();
        }

        public void Add(MessagePackageHistoryEntity MessagePackageHistoryEntityParam, out Guid MessagePackageHistoryId)
        {
            _MessagePackageHistoryDB.AddMessagePackageHistoryDB(MessagePackageHistoryEntityParam,
                out MessagePackageHistoryId);
        }

        public void Update(MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            _MessagePackageHistoryDB.UpdateMessagePackageHistoryDB(MessagePackageHistoryEntityParam);
        }

        public void Delete(MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            _MessagePackageHistoryDB.DeleteMessagePackageHistoryDB(MessagePackageHistoryEntityParam);
        }

        public MessagePackageHistoryEntity GetSingleById(MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            MessagePackageHistoryEntity o = GetMessagePackageHistoryFromMessagePackageHistoryDB(
                _MessagePackageHistoryDB.GetSingleMessagePackageHistoryDB(MessagePackageHistoryEntityParam));

            return o;
        }

        public List<MessagePackageHistoryEntity> GetAll()
        {
            List<MessagePackageHistoryEntity> lst = new List<MessagePackageHistoryEntity>();
            //string key = "MessagePackageHistory_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MessagePackageHistoryEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMessagePackageHistoryCollectionFromMessagePackageHistoryDBList(
                _MessagePackageHistoryDB.GetAllMessagePackageHistoryDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<MessagePackageHistoryEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "MessagePackageHistory_List_Page_" + currentPage ;
            //string countKey = "MessagePackageHistory_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<MessagePackageHistoryEntity> lst = new List<MessagePackageHistoryEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MessagePackageHistoryEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetMessagePackageHistoryCollectionFromMessagePackageHistoryDBList(
                _MessagePackageHistoryDB.GetPageMessagePackageHistoryDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private MessagePackageHistoryEntity GetMessagePackageHistoryFromMessagePackageHistoryDB(
            MessagePackageHistoryEntity o)
        {
            if (o == null)
                return null;
            MessagePackageHistoryEntity ret = new MessagePackageHistoryEntity(o.MessagePackageHistoryId, o.AcctionTypeId,
                o.OccasionDayId, o.ItemId, o.PersonMoblieId, o.UserAndGroupId, o.MessageDraftId, o.MessageText,
                o.PersonMoblieIdStr, o.SendDate, o.SmsServiceProviderId, o.RelationShipId, o.CreationDate,
                o.ModificationDate, o.SendTime, o.GroupIdStr, o.PersonMoblieIdShowSelectControlStr,
                o.FullNameShowSelectControlStr);
            return ret;
        }

        private List<MessagePackageHistoryEntity> GetMessagePackageHistoryCollectionFromMessagePackageHistoryDBList(
            List<MessagePackageHistoryEntity> lst)
        {
            List<MessagePackageHistoryEntity> RetLst = new List<MessagePackageHistoryEntity>();
            foreach (MessagePackageHistoryEntity o in lst)
            {
                RetLst.Add(GetMessagePackageHistoryFromMessagePackageHistoryDB(o));
            }
            return RetLst;

        }




        public List<MessagePackageHistoryEntity> GetMessagePackageHistoryCollectionByOccasionDay(
            MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            return
                GetMessagePackageHistoryCollectionFromMessagePackageHistoryDBList(
                    _MessagePackageHistoryDB.GetMessagePackageHistoryDBCollectionByOccasionDayDB(
                        MessagePackageHistoryEntityParam));
        }

        public List<MessagePackageHistoryEntity> GetMessagePackageHistoryCollectionByAcctionType(
            MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            return
                GetMessagePackageHistoryCollectionFromMessagePackageHistoryDBList(
                    _MessagePackageHistoryDB.GetMessagePackageHistoryDBCollectionByAcctionTypeDB(
                        MessagePackageHistoryEntityParam));
        }

        public List<MessagePackageHistoryEntity> GetMessagePackageHistoryCollectionByMessageDraft(
            MessagePackageHistoryEntity MessagePackageHistoryEntityParam)
        {
            return
                GetMessagePackageHistoryCollectionFromMessagePackageHistoryDBList(
                    _MessagePackageHistoryDB.GetMessagePackageHistoryDBCollectionByMessageDraftDB(
                        MessagePackageHistoryEntityParam));
        }


        public DataSet GenerateMessagePackageForSmswithItem()
        {
            return _MessagePackageHistoryDB.GenerateMessagePackageForSmswithItemDB();
        }

        public DataSet GenerateMessagePackageForSmswithOccasion()
        {
            return _MessagePackageHistoryDB.GenerateMessagePackageForSmswithOccasionDB();
        }
    }


}

